input_labeled folder must be placed under data/labeled/ folder

Performance report:
SOURCE-PRIM
F1 score 0.971039182282794
Recall 0.9595959595959596
Precision 0.9827586206896551
SOURCE-SEC
F1 score 0.8920863309352518
Recall 0.8378378378378378
Precision 0.9538461538461539
