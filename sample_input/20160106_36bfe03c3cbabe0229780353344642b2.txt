François Hollande.
Crédits photo : Jean-Christophe MARMARA/Le Figaro
Le contre-point de Guillaume Tabard.
Plus les jours passent et plus le débat s'enlise.
Plus la question se complique.
Moins l'issue paraît évidente.
Et plus les arrière-pensées tactiques apparaissent au premier plan.
La déchéance de nationalité.
Au Congrès de Versailles, le 16 novembre, François Hollande avait réussi un bel effet de surprise.
Et un joli coup politique: en une mesure, il obligeait la droite à l'applaudir, contraignait la gauche à le soutenir et se construisait une image de fermeté dans l'opinion.
Mais il n'aura pas fallu deux mois pour que l'offensive éclair se transforme en bourbier.
Le débat parlementaire n'a pas encore commencé que les questions ne sont déjà plus: ...
Cet article a été publié dans l'édition du Figaro du
07/01/2016 .
