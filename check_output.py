import os

is_correct_output = True

output_folder = 'out/'
expected_output_folder = 'expected_output/'
for file_name in os.listdir(output_folder):
    with open(os.path.join(output_folder, file_name)) as f:
        out = f.read()
    with open(os.path.join(expected_output_folder, file_name)) as f:
        expected = f.read()
    if out != expected:
        print('DIFF', file_name)
        is_correct_output = False

if len(os.listdir(output_folder)) != len(os.listdir(expected_output_folder)):
    print('Different number of output files')
    is_correct_output = False

if is_correct_output:
    print('Correct output')
