# Limsi-SourceExtractor

## Abstract

This is a code refactoring of Limsi's source extractor program in order to expose source extraction as a web service.
This is a Spring Boot application deployed in a Docker image. 

## Project requirement
This is a Java/Spring boot project. All dependencies are setup in a Maven pom.
On Macos X or Windows workstation, Docker should be install [https://www.docker.com/docker-toolbox](https://www.docker.com/docker-toolbox)

+ Java Oracle JDK8 [http://www.oracle.com/technetwork/java/javase/downloads/index.html](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
+ Apache Maven [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)
+ Docker [https://www.docker.com/docker-toolbox](https://www.docker.com/docker-toolbox)



## Run and Build project
### Run localy
+ Change `application.yml` in `PROJECT_HOME/Limsi-SourceExtractor/src/test/configuration` with local lib and resources path (change PROJECT_HOME). Required absolute path.

    resource: 
     #Directory containing the librairies and models
     lib: PROJECT_HOME/Limsi-SourceExtractor/lib
     #Directory containing the language-dependent resources
     resources: PROJECT_HOME/Limsi-SourceExtractor/resources

+ `mvn clean install`
+ Goto to `target` directory
+ Run in a terminal `java -Dspring.config.location=../src/test/configuration/ -jar limsiSourceExtractor.jar` 

If running from IDE (eclipse), VM argument `java -Dspring.config.location=src/test/configuration/` must be added in the "Run configuration".

#### Troubleshooting
+ Application do not find `lib` and `resources` path at startup:
 
	=> check that application.yml set the correct path for lib and resources
	=> -Dspring.configuration path should end with /
+ Wapiti OS libraries may crash the webapp: see Wapiti build section below

#### Bug
+ Service stop on MacosX during extraction with wapiti's message `error: invalid model format` (This is not happening running inside eclipse ...)

### Run with Docker
####Use default resources
Models and resources are embedded by default in the docker image.
+ Build docker image `mvn clean package docker:build`
+ Start default image `docker run -p 8080:8080 -t limsi-source-extractor`
+ Start named default image `docker run --name limsi-extractor -p 8080:8080 -t limsi-source-extractor`

Note: Memory usage can be setup with JAVA_OPTS in the docker command line:
 `docker run --name limsi-extractor -p 8080:8080 -e JAVA_OPTS='-Xmx3g' -d limsi-source-extractor`
 
####Custom resources/lib
Build image can be used with new training set. In this case it is possible to overwrite default model configuration.

In the docker image volumes have been setup to mount new resources.
+ `lib` folder should be mount to `/configuration/lib`
+ `resources` folder should be mount to `/configuration/resources`
+ Configuration file from PROJECT_HOME/Limsi-SourceExtractor/configuration/ that define `application.yml`  with docker Volumes must be use.

+ Run docker image with mount resources:

    docker run -p 8080:8080 -v /PROJECT_HOME/Limsi-SourceExtractor/configuration:/configuration -v /PROJECT_HOME/Limsi-SourceExtractor/lib:/configuration/lib -v /PROJECT_HOME/Limsi-SourceExtractor/resources:/configuration/resources -t limsi-source-extractor

#### Docker cheat sheet

+ List images `docker images`
+ List containers `docker ps -a`
+ Stop container `docker stop <container_name>`
+ Start container `docker start <container_name>`
+ Show container logs `docker logs <container_name>`
+ Remove stop containers `docker rm $(docker ps -a -q)`
+ Remove image `docker rmi <image_name>`
+ Load image from file `docker load < image_file.tar`
+ Export image `docker export > image_file.tar`
+ Set environment variables  `docker run -e JAVA_OPTS=`


## Test
HTTP GET http://localhost:8080/sourceExtractor/

    =>HTTP 200 "Greetings from Spring Boot!"
    
HTTP POST http://localhost:8080/sourceExtractor/extractNewsML
Charset text/xml

    curl -XPOST -H "Content-Type: text/xml" -d @afp.com-20150123T153420Z-TX-PAR-SZS52.xml http://localhost:8080/sourceExtractor/extractNewsML


## Annex
### Third-party libraries

#### Wapiti build
Wapiti OS libraries may failed depending of your OS version.
You must then build the OS library from the Wapiti project: [https://github.com/kermitt2/Wapiti](https://github.com/kermitt2/Wapiti)


