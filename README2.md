# To build the Docker image (run this only once)
sudo docker build -t source-extractor .

# To run the source-extractor, you can take "sample_input" as <input_folder>
(sudo) ./run_docker.sh extract <input_folder>

# To evaluate the model's performance, the folder input_labeled contains 363 annotated documents and is provided upon request
(sudo) ./run_docker.sh evaluate input_labeled
