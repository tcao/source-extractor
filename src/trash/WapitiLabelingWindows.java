package fr.limsi.sourceExtractor.wapiti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import fr.limsi.wapiti.WapitiIO;
import fr.limsi.wapiti.WapitiModel;

public class WapitiLabelingWindows extends WapitiLabeling {
	
	private WapitiModel model;
	private MyWapitiIO ioA;
	
	public WapitiLabelingWindows(File modelsFile) throws UnsupportedEncodingException, FileNotFoundException {
		this.ioA = new MyWapitiIO(
				new FileInputStream(modelsFile),
				null);
		this.model = new WapitiModel(ioA);

	}

	@Override
	public void finalize() {
		this.ioA.close();		
	}
	
	@Override
	public void label(File input, File outputDir,
			String filename) throws UnsupportedEncodingException, FileNotFoundException {

		MyWapitiIO ioB = new MyWapitiIO(
				new FileInputStream(input),
				new FileOutputStream(outputDir+"/"+filename+".wapiti"));

		this.model.label(ioB);

		ioB.close();
	}
	
	private class MyWapitiIO extends WapitiIO {

//	    public int lines = 0;
//	    public int lines_out_count = 0;
	    private BufferedReader reader;
	    private OutputStream outputStream;
	    private InputStream inputStream;

	    public MyWapitiIO(
	        InputStream inputStream,
	        OutputStream outputStream) throws UnsupportedEncodingException
	        {
	            super();
	            this.reader = new BufferedReader(
	                new InputStreamReader(inputStream, "ISO-8859-1"));

	            this.outputStream = outputStream;
	            this.inputStream = inputStream;
	        }

	    @Override
	    public String readline() {
//	        this.lines++;
	        try {
	            return this.reader.readLine();
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    @Override 
	    public void append(String bytes) {
	        try {
	            byte[] b = bytes.getBytes("ISO-8859-1");
	            if (this.outputStream != null)
	                this.outputStream.write(b);
//	            this.lines_out_count++;
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    public void close() {
	        try {
	            if (this.outputStream != null)
	                this.outputStream.close();
	            if (this.inputStream != null)
	                this.inputStream.close();
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
	
}
