package fr.limsi.sourceExtractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.grobid.core.jni.WapitiModel;
import org.maltparser.concurrent.ConcurrentMaltParserModel;
import org.maltparser.concurrent.ConcurrentMaltParserService;
import org.maltparser.core.exception.MaltChainedException;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.CoreMapExpressionExtractor;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.ling.tokensregex.TokenSequencePattern;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

/**
 * Classe permettant de Tag, d'encoder en BIO, de convertir en Brat. Utilisant le Stanford CoreNLP.
 *
 */
public class SourceExtractor {
	// Default configuration file
	private static final String DEFAULT_CONFIG_FILE = "config.properties";

	// Configuration fields
	private static final String DATA_DIR_PROPERTY = "DATA_DIR";
	private static final String LIB_DIR_PROPERTY = "LIB_DIR";
	private static final String RESOURCES_DIR_PROPERTY = "RESOURCES_DIR";

	// Patterns
	private static final Pattern BRAT_ENTITY_PATTERN = Pattern.compile("^([^\\s]+)\t([^\\s]+) (.+)$");
	private static final Pattern PN_COMMA_PATTERN = Pattern.compile("^(\\p{javaUpperCase}[^, ]+( (de|du|\\p{javaUpperCase}[^, ]+))*) ?,.*");
	private static final Pattern COMMA_NP_AND_MORE_PATTERN = Pattern.compile("de [^()/,]+, (\\p{javaUpperCase}[^, ]+( (de|\\p{javaUpperCase}[^, ]+))+.*)$");
	private static final Pattern BRAT_ENTITY_DETAILED_PATTERN = Pattern.compile("^T(\\d+)\t([^\\s]+) (\\d+) (\\d+)\t(.+)$");
	private static final Pattern BRAT_COMMENT_DETAILED_PATTERN = Pattern.compile("#\\d+\tAnnotatorNotes T(\\d+)\t(.+)$");

	// Constants
	private final static String SOURCEPRIM = "SOURCE-PRIM";
	private final static String SOURCESEC = "SOURCE-SEC";
	private final static String SECONDARY = "secondary";
	private final static String PRIM = "-PRIM";
	private final static String SEC = "-SEC";
	private final static String OUT = "O";
	private final static String NULL = "NULL";
	private final static String SOURCE = "SOURCE";

	private final static String MODEL_PRIM = "model_primary";
	private final static String MODEL_SEC = "model_secondary";

	private static final String encodingI = "I";
	private static final String encodingB = "B";
	//	private static final String encodingIO = "io";
	//	private static final String encodingBIO = "bio";
	//	private static final String addAnnotation = "ann";

	private static final int numberFieldsPrim = 10;
	private static final int numberFieldsSec = 11;

	private int counterFiles = 0;

	//	HashSet<Integer> startOffsetsPrim;
	//	HashSet<Integer> inOffsetsPrim;
	//	HashSet<Integer> startOffsetsSec;
	//	HashSet<Integer> inOffsetsSec;

	//	HashMap<String, ArrayList<Integer>> sourceAndOffset;
	//	HashMap<Integer,Integer> bestSubjectForTheSources;
	//	HashMap<Integer, Pair<String, String>> wordsAndPosByOffset;

	// Data directories (all will depend on directories specified
	//   in configuration file)
	private static File DATA_DIR = null;
	private static File DIR_LIB = null;
	private static File DIR_RESOURCES = null;

	// If we're working on label data split into train/dev/test (evaluation purpose)
	private static boolean LABELED_MODE = false;
	// If we're working on production (already have a model, just apply it to unlabeled data)
	private static boolean PRODUCTION_MODE = false;
	// BIO mode (as opposed to IO mode only)
	//	private static boolean BIO;
	private static boolean modelBio = true;
	// Brat output (as opposed to JSON output)
	private static boolean outputBrat = false;
	// Media list is activated 
	private static boolean searchSecondary = true;

	private Tools tools;
	private Resources resources;
	private Paths paths;
	private Memory memory;
	//	private static File lastFinalFile;

	public SourceExtractor() throws IOException, MaltChainedException {
		this.memory = new Memory(new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
		this.paths = new Paths();

	}

	public void loadTools(String lemmatizerResourceDir, String gazetteersDir, File dirModelMaltParser) throws IOException, MaltChainedException {
		Set<String> citationVerbs = IOUtils.getFileContentAsSet(gazetteersDir + "/citation_verbs.lst");
		Set<String> professionWords = IOUtils.getFileContentAsSet(gazetteersDir + "/professions.lst");
		Set<String> countriesAndContinents = IOUtils.getFileContentAsSet(gazetteersDir + "/countries.lst");
		Set<String> stopWords = IOUtils.getFileContentAsSet(gazetteersDir + "/stopWords.lst");
		Set<String> persons = IOUtils.getFileContentAsSet(gazetteersDir + "/pers-org-triggers.lst");
		Set<String> capitals = IOUtils.getFileContentAsSet(gazetteersDir + "/capitals.lst");
		Set<String> personAbbr = IOUtils.getFileContentAsSet(gazetteersDir + "/person_abbr.lst");
		Set<String> pronouns = IOUtils.getFileContentAsSet(gazetteersDir + "/pronouns.lst");
		Set<String> temporalExpressions = IOUtils.getFileContentAsSet(gazetteersDir + "/temporal_expressions.lst");
		Set<String> nonDetArticles = IOUtils.getFileContentAsSet(gazetteersDir + "/undet-articles.lst");
		Map<String, String> coarseGrainedPOSMap = IOUtils.getFileContentAsMap(gazetteersDir + "/coarse_grained_category_mapping.lst", " ", false);

		this.resources = new Resources(citationVerbs, professionWords, countriesAndContinents, stopWords, persons, capitals, nonDetArticles, personAbbr, pronouns,
				temporalExpressions, coarseGrainedPOSMap);

		File stanfordExtractionRulesOthers = new File(DIR_RESOURCES + "/fr/citation-triggers.corenlp");
		File mediaListFile = new File(DIR_RESOURCES + "/fr/media-list.corenlp");
		File triggerSecondaryFile = new File(DIR_RESOURCES + "/fr/secondary-source-triggers.corenlp");

		StanfordCoreNLP coreNLPParser = getNewParser(Locale.FRENCH);
		FrenchLemmatizer lemmatizer = new FrenchLemmatizer(lemmatizerResourceDir);
		CoreMapExpressionExtractor<MatchedExpression> triggerExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(),
				stanfordExtractionRulesOthers.getAbsolutePath());

		CoreMapExpressionExtractor<MatchedExpression> mediaExtractor = null;
		if (searchSecondary) {
			mediaExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(), mediaListFile.getAbsolutePath());
		}

		CoreMapExpressionExtractor<MatchedExpression> triggerSecondaryExtractor = CoreMapExpressionExtractor.createExtractorFromFiles(TokenSequencePattern.getNewEnv(),
				triggerSecondaryFile.getAbsolutePath());

		System.err.print("Loading Malt parser... ");
		URL frMaltModelURL = new File(dirModelMaltParser, "fremalt-1.7.mco").toURI().toURL();
		ConcurrentMaltParserModel maltParserModel = ConcurrentMaltParserService.initializeParserModel(frMaltModelURL);
		this.tools = new Tools(triggerExtractor, mediaExtractor, triggerSecondaryExtractor, coreNLPParser, lemmatizer, maltParserModel);
		System.err.println("done");

	}

	private static StanfordCoreNLP getNewParser(Locale locale) {
		Properties props = new Properties();
		props.put("tokenize.options", "tokenizeNLs=true");
		// props.put("ssplit.eolonly", "true");
		props.put("ssplit.newlineIsSentenceBreak", "always");
		props.put("parse.maxlen", "100");

		if (locale.equals(Locale.ENGLISH)) {
			// creates a StanfordCoreNLP object, with POS tagging,
			// lemmatization, NER, parsing, and coreference resolution
			props.put("annotators", "tokenize, ssplit, pos, lemma");
			props.put("ner.useSUTime", "false");

		} else if (locale.equals(Locale.FRENCH)) {
			props.put("pos.model", "edu/stanford/nlp/models/pos-tagger/french/french.tagger");

			props.put("parse.model", "edu/stanford/nlp/models/lexparser/frenchFactored.ser.gz");
			props.put("depparse.model", "edu/stanford/nlp/models/parser/nndep/UD_French.gz");

			props.put("annotators", "tokenize, ssplit, pos");
			props.put("tokenize.language", "fr");
		} else {
			throw new RuntimeException("Locale " + locale + " is not supported");
		}
		return new StanfordCoreNLP(props);
	}

	/**
	 * Méthode utilisée pour parcourir les dossiers quand on a des fichiers de test déjà annotés.<br />
	 * On l'utilise avec un dossier dir et le comportement sera différent selon le dossier entrée.<br />
	 * Fonctionne avec seulement 2 dossiers :<br />
	 * &nbsp;&nbsp;- Celui de tous les fichiers .txt avec les fichiers .ann qui vont avec,<br />
	 * &nbsp;&nbsp;- Celui où se trouvent / trouveront les fichiers .tag annotés automatiquement avec Wapiti (voir
	 * script bio_wapiti_ann.sh).<br />
	 * <br />
	 * On parcourt les fichiers dans le dossier en entrée, si on est dans le dossier d'input, on appelle la fonction
	 * parseFileToTag sur chaque fichier .txt pour tag chaque <br />
	 * fichier (on "continue" quand fichier .ann), et si on est dans le fichier de sortie de Wapiti, on prend tous les
	 * fichiers .tag et pour chaque fichier on appelle <br />
	 * la fonction parseFileToAnn pour annoter automatiquement chaque fichier.<br />
	 * <br />
	 *
	 * @param input
	 *            Le dossier en entrée (input_labeled ou bioWapiti)
	 * @throws IOException
	 */
	public void parseDirForLabeled(File input) throws IOException {

		if (input.isDirectory()) {
			int nbFilesDir = input.list().length;
			int countFiles = 0;

			for (File inFile : input.listFiles()) {
				if (inFile.isFile()) {
					String name = inFile.getName();
					String fileId = name.substring(0, name.lastIndexOf("."));

					// first step : pos tagging + malt parser + (B)IO conversion
					if (name.endsWith(".txt") && input.getName().equals(this.paths.DIR_INPUT_LABELED.getName())) {

						countFiles++;
						System.out.println("PoS Tagging + malt parser + (B)IO conversion : Traitement du fichier : " + countFiles + "/" + (nbFilesDir / 2));
						System.out.println(inFile.getName());

						parseFileToTag(fileId, inFile, nbFilesDir / 2);
					}
					// second step : convert the wapiti output files to brat format
					else if (name.endsWith(".wapiti")) {
						countFiles++;
						// PRIM
						if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM.getName())) {
							System.out.println("BIOWAPITI LABELED PRIM: Traitement du fichier : " + countFiles + "/" + (nbFilesDir));

							parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, PRIM, tools, paths, resources, memory);
						}
						// SEC
						else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC.getName())) {

							System.out.println("BIOWAPITI LABELED SEC : Traitement du fichier : " + countFiles + "/" + (nbFilesDir));
							parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, SEC, tools, paths, resources, memory);

						} else {
							System.out.println("Erreur dossier bioWapiti");
							System.exit(0);
						}

					}
					// third step : add annotation to existing sources (coreference, named entity recognition..)
					else if (name.endsWith(".ann")) {
						if (input.getName().equals(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.getName())) {
							countFiles++;

							System.out.println("ADD ANNOTATION : Traitement du fichier : " + countFiles + "/" + (nbFilesDir));
							//							System.out.println(inFile.getAbsolutePath());
							File outputFile = parseFileToAddAnnotationToBrat(fileId, inFile);
							System.out.println("2. Output written in " + outputFile.getAbsolutePath());

						} else if (input.getName().equals(this.paths.DIR_INPUT_LABELED.getName())) {
							//							throw new RuntimeException(input.getName());
							continue;
						}
					}
					//					else if (name.endsWith(".json_temp")) {
					//						countFiles++;
					//						if (!outputBrat) {
					//							if (input.getName().equals(DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.getName())) {
					//								System.out.println("ADD ANNOTATION : Traitement du fichier : "+countFiles+"/"+(nbFilesDir));
					//								File outputFile = parseFileToAddAnnotationToBrat(fileId, inFile);
					//								System.out.println("3. Output written in " + outputFile.getAbsolutePath());
					//							}
					//						}
					//					}
					else {
						System.out.println("Format de fichier inconnu : " + name + " -> seulement .txt, .ann, .tag");
						System.exit(1);
					}
				}

			}
		} else if (input.isFile()) {
			String name = input.getName();
			//first step : pos tagging + malt parser + (B)IO conversion
			if (name.endsWith(".txt")) {
				String fileId = name.substring(0, name.lastIndexOf("."));
				System.out.println("PoS Tagging + malt parser + (B)IO conversion : Traitement du fichier " + name);
				parseFileToTag(fileId, input, 1);
			}
		}
	}

	/**
	 * Méthode utilisée pour parcourir les fichiers .ann dans le but d'y ajouter des annotations. </br>
	 * On commence par récupérer l'annotation BRAT automatiquement générée par la transformation du résultat de wapiti
	 * (bratAutoTransformed).</br>
	 * On prend ensuite le résultat de sortie de wapiti (wapitiResult).</br>
	 * On a besoin des offsets de chaque mots donc parse de nouveau le texte pour récupérer seulements les offsets
	 * (taggedTextOnlyOffset).</br>
	 * </br>
	 * Ensuite on appelle la fonction parseTaggedTextToKeepCoreferencesAndSubject pour trouver les sujets / correference
	 * et garder les meilleurs.</br>
	 * </br>
	 * On finit par ajouter les éléments trouvés au nouveau fichier BRAT avec addAnnotationCorreferenceToBrat.
	 * 
	 * @param inFile
	 * @throws IOException
	 */
	private File parseFileToAddAnnotationToBrat(String fileId, File inFile) throws IOException {
		String bratGeneratedWithWapitiResult = Files.toString(new File(inFile.getParentFile(), fileId + ".ann"), Charsets.UTF_8);
		String wapitiResult = "";

		String taggedTextOnlyOffset = "";
		File inputTxtFile;
		if (LABELED_MODE) {
			wapitiResult = Files.toString(new File(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM, fileId + ".wapiti"), Charsets.UTF_8);
			inputTxtFile = new File(this.paths.DIR_INPUT_LABELED, fileId + ".txt");
		} else {
			wapitiResult = Files.toString(new File(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, fileId + ".wapiti"), Charsets.UTF_8);
			inputTxtFile = new File(this.paths.DIR_INPUT_UNLABELED, fileId + ".txt");
		}
		taggedTextOnlyOffset = parseText(fileId, getDocText(fileId, inputTxtFile, memory), tools, resources, memory);

		HashMap<String, String> retrieveOffsetsOfSource = new HashMap<>();

		List<String[]> listOfBratAnnotation = new LinkedList<String[]>();
		String[] wordsBratAnnotation = bratGeneratedWithWapitiResult.split("\\r?\\n");

		for (int i = 0; i < wordsBratAnnotation.length; i++) {
			listOfBratAnnotation.add(wordsBratAnnotation[i].split("\\s+"));
		}

		// we keep only the SOURCE-PRIM, it is useful not to have the secondary ones to find the subjects
		for (int i = 0; i < wordsBratAnnotation.length; i++) {
			Matcher matcher = BRAT_ENTITY_PATTERN.matcher(wordsBratAnnotation[i]);
			if (matcher.find()) {
				String id = matcher.group(1);
				String source = matcher.group(2);
				String restOfLine = matcher.group(3);

				if (source.contains(PRIM)) {
					retrieveOffsetsOfSource.put(id, restOfLine.trim());
				}
			}
		}

		// this will contain the left and right offset of each source
		TreeMap<Integer, Integer> sourceOffsets = new TreeMap<>();
		// this will contain the left and right offset of each subject found by maltparser (column isSuj)
		TreeMap<Integer, Integer> subjectOffsets = new TreeMap<>();
		// this will contain the left and right offset of each professions word
		TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets = new TreeMap<>();
		// this will contain the left and right offset of each pronoun 
		TreeMap<Integer, Integer> pronounOffsets = new TreeMap<>();
		HashMap<Integer, Integer> bestSubjectForTheSources = new HashMap<>();

		HashSet<Integer> startOffsetsPrim = new HashSet<>();
		HashSet<Integer> inOffsetsPrim = new HashSet<>();
		HashSet<Integer> startOffsetsSec = new HashSet<>();
		HashSet<Integer> inOffsetsSec = new HashSet<>();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = new HashMap<>();

		// we look for what to write as annotation and we keep the best ones
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = parseTaggedTextToKeepCoreferencesAndSubject(wapitiResult, taggedTextOnlyOffset,
				retrieveOffsetsOfSource, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, bestSubjectForTheSources);

		fillMapsWithOffsets(listOfBratAnnotation, fileId, startOffsetsPrim, inOffsetsPrim, startOffsetsSec, inOffsetsSec, sourceAndOffset);

		// we write annotations that we found to be the best
		File outputFile = addAnnotationCoreferenceToBrat(fileId, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, wordsAndPosByOffset,
				bestSubjectForTheSources, sourceAndOffset);
		return outputFile;
	}

	private File addAnnotationCoreferenceToBrat(String fileId, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, HashMap<Integer, Integer> bestSubjectForTheSources,
			HashMap<String, ArrayList<Integer>> sourceAndOffset) throws IOException {

		// "left offset source" : "[subject, source]"
		HashMap<Integer, Pair<String, String>> coreference = new HashMap<>();

		// we loop through the best subject we found to be the best
		for (Entry<Integer, Integer> r : bestSubjectForTheSources.entrySet()) {
			StringBuilder source = new StringBuilder();

			int startSource = r.getKey();
			int startSubject = r.getValue();

			// we write the source in StringBuilder source
			appendSource(sourceOffsets, wordsAndPosByOffset, source, startSource);

			StringBuilder subject = new StringBuilder();
			// if the subject is actually a subject
			if (subjectOffsets.get(startSubject) != null) {
				appendSubject(subjectOffsets, sourceOffsets, wordsAndPosByOffset, coreference, source, startSource, startSubject, subject);
			}
			// if the subject is not a subject but a important word
			// in the text it is possible that "Le ministre E. Macron" exists without being a subject
			else if (profAndSecondarySourceTriggerOffsets.get(startSubject) != null) {
				appendSubject(profAndSecondarySourceTriggerOffsets, sourceOffsets, wordsAndPosByOffset, coreference, source, startSource, startSubject, subject);
			}
			// if anonymous source
			else if (startSubject == -1) {
				coreference.put(startSource, new Pair<>("anonymous", source.toString()));
			}
		}

		HashMap<String, Double> sourceHasAlreadyBeenAnnotated = new HashMap<String, Double>();

		int indexNotes = 0;

		HashMap<String, String> keepOnlyOneAnnotation = new HashMap<>();

		//		JSONObject object= null;
		//		JSONArray jsonArray = null;

		File annFile = null;
		File jsonFile = null;

		if (LABELED_MODE) {
			annFile = new File(this.paths.DIR_FINAL_RESULT_LABELED, fileId + ".ann");
			// JSON
			if (!outputBrat) {
				jsonFile = new File(this.paths.DIR_FINAL_RESULT_LABELED, fileId + ".json");

			}
		} else {
			annFile = new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".ann");

			if (!outputBrat) {
				jsonFile = new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".json");
			}
		}

		StringBuilder result = new StringBuilder();

		// sourceAndOffset : "T1" : [550, 551, 552, 553, 554, 555, ...]
		for (Entry<String, ArrayList<Integer>> entry : sourceAndOffset.entrySet()) {

			String idAnnotation = entry.getKey();
			ArrayList<Integer> eachOffsetsOfTheSource = entry.getValue();

			// if correference : "550" : ["le ministre", "E. Macron"] et sourceAndOffset : "T3" : [550, 551, 552, 553, 554]
			// 550 match -> true
			if (CollectionUtils.containsAny(eachOffsetsOfTheSource, coreference.keySet())) {

				// we keep 550
				Collection<Integer> sameValues = CollectionUtils.intersection(eachOffsetsOfTheSource, coreference.keySet());
				String annotation = "";

				// we loop through... 550
				for (int val : sameValues) {
					// if T3 has not already been marked
					if (!sourceHasAlreadyBeenAnnotated.containsKey(idAnnotation)) {
						// we get the subject of the source
						String coref = coreference.get(val).getFirst();
						// some corrections because we assemble the subjects from the word with a space between each word
						annotation = coref.replaceAll(" , ", ", ");
						annotation = annotation.replaceAll("l' ", "l'");

						if (annotation.endsWith(", ")) {
							annotation = annotation.substring(0, annotation.length() - 2);
						}

						indexNotes++;
						// we eventually write the coreference in Brat format
						keepOnlyOneAnnotation.put(idAnnotation, "#" + (indexNotes) + "\t" + "AnnotatorNotes " + idAnnotation + "\t" + annotation + "\t");

					}
				}
			}
		}

		//		if (outputBrat) {
		for (String s : keepOnlyOneAnnotation.values()) {
			//				System.out.println("APPEND " + s);
			result.append(s + "\n");
		}
		Files.append(result, annFile, Charsets.UTF_8);
		//		}
		// Convert to JSON
		if (outputBrat) {
			if (!LABELED_MODE) {
				// Also add the .txt file for visualization in Brat
				Files.copy(new File(this.paths.DIR_INPUT_UNLABELED, fileId + ".txt"), new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".txt"));
			}

			return annFile;
		} else {
			brat2JSON(fileId, annFile, jsonFile);
			//			Files.write("", outputFile, Charsets.UTF_8);
			//			try {
			//				Files.write(object.toString(2), outputFile, Charsets.UTF_8);
			//			} catch (JSONException e) {
			//				e.printStackTrace();
			//			}
			annFile.delete();
			return jsonFile;
		}
		//		if (jsonTempFile.exists())
		//			jsonTempFile.delete();
		//		return outputFile;
	}

	private static HashMap<String, SourceAnnotation> brat2Annotation(File annFile) throws IOException {
		HashMap<String, SourceAnnotation> result = new HashMap<>();
		String[] annLines = Files.toString(annFile, Charsets.UTF_8).split("\n");
		for (String annLine : annLines) {
			// Entity
			Matcher entityMatcher = BRAT_ENTITY_DETAILED_PATTERN.matcher(annLine);
			if (entityMatcher.find()) {
				String id = entityMatcher.group(1);
				String type = entityMatcher.group(2);
				int leftOffset = Integer.parseInt(entityMatcher.group(3));
				int rightOffset = Integer.parseInt(entityMatcher.group(4));
				String text = entityMatcher.group(5);
				SourceAnnotation annotation = new SourceAnnotation(id, type, leftOffset, rightOffset, text);
				result.put(id, annotation);
			} else {
				// Comment
				Matcher commentMatcher = BRAT_COMMENT_DETAILED_PATTERN.matcher(annLine);
				if (commentMatcher.find()) {
					String id = commentMatcher.group(1);
					String value = commentMatcher.group(2);
					SourceAnnotation annotation = result.get(id);
					assert annotation != null;
					if (value.equals("anonymous")) {
						annotation.setAnonymous(true);
					} else {
						annotation.setValue(value);
						String indexed = text2Index(value);
						if (indexed != null && indexed.length() > 0) {
							annotation.setIndexValue(indexed);
						}
					}
					result.put(id, annotation);
				}
			}
		}
		return result;
	}

	private void brat2JSON(String fileId, File annFile, File jsonFile) throws IOException {
		String docText = this.memory.docTexts.get(fileId);
		ArrayList<Integer> sentenceOffsets = this.memory.sentenceOffsetsByFile.get(fileId);
		//		System.out.println(sentenceOffsets);
		HashMap<String, SourceAnnotation> annotations = brat2Annotation(annFile);

		TreeSet<SourceAnnotation> orderedAnnotations = new TreeSet<>();
		orderedAnnotations.addAll(annotations.values());
		StringBuilder jsonResult = new StringBuilder();
		jsonResult.append("{\"source_sentences\":[\n");

		int annIndex = 0;

		ArrayList<SourceAnnotation> sentenceAnnotations = new ArrayList<>();
		int currentSentenceIndex = 0;
		int currentSentenceOffset = sentenceOffsets.get(currentSentenceIndex);
		int nextSentenceOffset;
		if (sentenceOffsets.size() > 1) {
			nextSentenceOffset = sentenceOffsets.get(currentSentenceIndex + 1);
		} else {
			nextSentenceOffset = docText.length();
		}

		for (SourceAnnotation annotation : orderedAnnotations) {
			int leftOffset = annotation.getLeftOffset();

			// Still same sentence
			if (leftOffset < nextSentenceOffset) {
				sentenceAnnotations.add(annotation);
			}
			// New sentence
			else {
				if (!sentenceAnnotations.isEmpty()) {
					annIndex = appendToJSON(docText, jsonResult, annIndex, sentenceAnnotations, currentSentenceIndex, currentSentenceOffset, nextSentenceOffset);
					sentenceAnnotations.clear();

				}

				// Go to sentence containing the annotation
				do {
					currentSentenceIndex++;
					currentSentenceOffset = sentenceOffsets.get(currentSentenceIndex);
					if (currentSentenceIndex < sentenceOffsets.size() - 1) {
						nextSentenceOffset = sentenceOffsets.get(currentSentenceIndex + 1);
					} else {
						nextSentenceOffset = docText.length();
					}
				} while (leftOffset > nextSentenceOffset);
				sentenceAnnotations.add(annotation);

			}
		}
		//		System.out.println("--" + currentSentenceOffset + " " + nextSentenceOffset);
		// Last annotations
		if (!sentenceAnnotations.isEmpty()) {
			annIndex = appendToJSON(docText, jsonResult, annIndex, sentenceAnnotations, currentSentenceIndex, currentSentenceOffset, nextSentenceOffset);
		}

		jsonResult.append("]}\n");
		Files.write(jsonResult.toString(), jsonFile, Charsets.UTF_8);
	}

	private int appendToJSON(String docText, StringBuilder jsonResult, int annIndex, ArrayList<SourceAnnotation> sentenceAnnotations, int currentSentenceIndex,
			int currentSentenceOffset, int nextSentenceOffset) {
		if (annIndex > 0) {
			jsonResult.append(",");
		}
		//		System.out.println("substring " + currentSentenceOffset + " -> " + (nextSentenceOffset - currentSentenceOffset));
		jsonResult.append("  {\"text\":\"" + docText.substring(currentSentenceOffset, nextSentenceOffset).replaceAll("\"", "\\\\\"").trim() + "\",");
		jsonResult.append("\n   \"sources\":[");
		int senAnnIndex = 0;
		for (SourceAnnotation sentenceAnnotation : sentenceAnnotations) {
			if (senAnnIndex > 0) {
				jsonResult.append(",");
			}
			jsonResult.append("\n" + sentenceAnnotation.toJSON());
			senAnnIndex++;
		}
		jsonResult.append("   ]}\n");
		annIndex++;
		return annIndex;
	}

	private void appendSubject(TreeMap<Integer, Integer> mapOffset, TreeMap<Integer, Integer> sourceOffsets, HashMap<Integer, Pair<String, String>> wordsAndPosByOffset,
			HashMap<Integer, Pair<String, String>> coreference, StringBuilder source, int startSource, int startSubject, StringBuilder subject) {

		Integer endSubject = mapOffset.get(startSubject);

		for (Entry<Integer, Integer> entry : sourceOffsets.entrySet()) {
			Integer oneStartSource = entry.getKey();
			Integer oneEndSource = entry.getValue();

			// Wapiti is better to delimit the start and the end of a source
			// so if a subject has the same start offset as a source (or if the subject is in the middle of the source),
			// we get the text of source instead of the subject text
			if (oneStartSource == startSubject || (oneStartSource < startSubject && endSubject < oneEndSource)) {
				startSubject = oneStartSource;
				endSubject = oneEndSource;
				break;
			}

		}

		// we build the subject from the index 
		for (int j = startSubject; j < endSubject; j++) {
			Pair<String, String> wordAndPos = wordsAndPosByOffset.get(j);
			if (wordAndPos != null)
				subject.append(wordAndPos.getFirst() + " ");

		}

		coreference.put(startSource, new Pair<>(subject.toString(), source.toString()));
	}

	/**
	 * @param mapOffsets
	 * @param source
	 * @param startSource
	 */
	private void appendSource(TreeMap<Integer, Integer> sourceOffsets, HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, StringBuilder source,
			int startSource) {

		Integer offsetEndSource = null;

		offsetEndSource = sourceOffsets.get(startSource);

		for (int offsetSubject = startSource; offsetSubject < offsetEndSource; offsetSubject++) {
			if (wordsAndPosByOffset.get(offsetSubject) != null)
				source.append(wordsAndPosByOffset.get(offsetSubject).getFirst() + " ");
		}
	}

	/**
	 * Cette méthode sert à garder les sujets importants et les correferences trouvés dans le texte. </br>
	 * On appelle d'abord la méthode findSubjects qui va récupérer tous les sujets annotés dans wapitiResult et ensuite
	 * on va trouver les meilleurs sujets</br>
	 * correspondant à chaque source.
	 * 
	 * @param wapitiResult
	 *            Le fichier en sortie de wapiti
	 * @param textWithOffset
	 *            On parse de nouveau le texte pour simplement récupérer les offsets des mots
	 * @param retrieveOffsetsOfSource
	 *            HashMap qui contient ID_SOURCE=OFFSETL OFFSETR TEXT
	 * @param sourceOffsets
	 *            TreeMap qui contient OFFSETL=OFFSETR
	 * @param subjectOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les sujets de la forme A=B
	 * @param profAndSecondarySourceTriggerOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les professions de la forme A=B
	 * @param pronounOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les pronoms de la forme A=B
	 * @return
	 * @return La liste de tous les sujets
	 * @throws IOException
	 */
	private HashMap<Integer, Pair<String, String>> parseTaggedTextToKeepCoreferencesAndSubject(String wapitiResult, String textWithOffset,
			HashMap<String, String> retrieveOffsetsOfSource, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets, HashMap<Integer, Integer> bestSubjectForTheSources)
			throws IOException {

		ArrayList<Integer> sourceOffsetLeft = new ArrayList<Integer>();

		// retrieveOffsetsOfSource contains OFFSET OFFSET TEXT, we want to keep only the first two element (offset left and right)
		for (String s : retrieveOffsetsOfSource.values()) {

			String[] fields = s.split("\\s+");
			String offsetLeft = fields[0];
			String offsetRight = fields[1];

			sourceOffsetLeft.add(Integer.parseInt(offsetLeft));
			sourceOffsets.put(Integer.parseInt(offsetLeft), Integer.parseInt(offsetRight));
		}

		// we start by gathering the subjects together 
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = findSubjects(wapitiResult, textWithOffset, sourceOffsets, subjectOffsets,
				profAndSecondarySourceTriggerOffsets, pronounOffsets, resources, memory);

		Collections.sort(sourceOffsetLeft);

		// we continue by looking for the best annotation among the ones we have found
		findBestSubjectsForSources(bestSubjectForTheSources, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, wordsAndPosByOffset);

		return wordsAndPosByOffset;

	}

	/**
	 * Cette méthode permet de lancer la méthode bestAnnotation sur les 3 listes que nous avons (sujets, professions,
	 * pronoms).
	 * 
	 * @param sourceOffsets
	 * @param subjectOffsets
	 * @param profAndSecondarySourceTriggerOffsets
	 * @param pronounOffsets
	 * @param wordsAndPosByOffset
	 */
	private HashMap<Integer, Integer> findBestSubjectsForSources(HashMap<Integer, Integer> bestSubjectForTheSources, TreeMap<Integer, Integer> sourceOffsets,
			TreeMap<Integer, Integer> subjectOffsets, TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset) {

		HashSet<Integer> alreadyAnnotated = new HashSet<>();
		HashMap<ArrayList<String>, Integer> profCorefAlreadyAnnoted = new HashMap<>();
		int lastIndexSujet = -1;

		// we loop through all source offset and we look for the left offset of each one.
		for (int sourceLeftOffset : sourceOffsets.keySet()) {

			lastIndexSujet = bestAnnotation(sourceOffsets, subjectOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources,
					sourceLeftOffset, lastIndexSujet, "subject");

			bestAnnotation(sourceOffsets, profAndSecondarySourceTriggerOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources,
					sourceLeftOffset, lastIndexSujet, "prof");

			bestAnnotation(sourceOffsets, pronounOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources, sourceLeftOffset,
					lastIndexSujet, "pronoun");

		}
		return bestSubjectForTheSources;
	}

	/**
	 * Méthode qui permet de garder le meilleur sujet pertinent selon la treemap que l'on parcourt.<br>
	 * 
	 * ALGO : <br>
	 * <br>
	 * Parcourir la liste des offsetsLeft de la treeMap voulue (item)<br>
	 * &nbsp;Si offsetLeftItem < sourceRightOffset (si le sujet apparaît avant la fin d'une source)<br>
	 * &nbsp;&nbsp;Si cette source n<br>
	 * &nbsp;&nbsp;&nbsp;Pour chaque tokenSOURCE != stopWords<br>
	 * 
	 * 
	 * 
	 * @param sourceOffsets
	 * @param mapOffsets
	 * @param wordsAndPosByOffset
	 * @param scores
	 * @param bestSujIndex
	 * @param alreadyAnnoted
	 * @param profCorefAlreadyAnnoted
	 * @param sourceLeftOffset
	 * @param lastIndexSujet
	 * @return
	 */
	private int bestAnnotation(TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> mapOffsets, HashSet<Integer> alreadyAnnoted,
			HashMap<ArrayList<String>, Integer> profCorefAlreadyAnnoted, HashMap<Integer, Pair<String, String>> wordsAndPosByOffset,
			HashMap<Integer, Integer> bestSubjectForTheSources, int sourceLeftOffset, int lastIndexSujet, String subjectMap) {

		Integer sourceRightOffset = sourceOffsets.get(sourceLeftOffset);
		ArrayList<String> sourceWords = new ArrayList<>();
		ArrayList<String> sourcePOSs = new ArrayList<>();

		// we rebuild the source from the offsets
		putWordsAndPos(wordsAndPosByOffset, sourceLeftOffset, sourceRightOffset, sourceWords, sourcePOSs);

		// based on the fact a term-profession which designs someone is used only for that person
		// so if a term-profession is a source and it has already been marked by a particular subject
		// so if we meet again this term-profession-source we can use the same subject used for the last same term-source
		//
		// let's make a example :
		// if we encounter these subjects in this order "Le réalisateur Antoine Viktine", "le réalisateur", "Le président de la République", "le réalisateur"
		// our way to annotate is always "look at the previous subject" and the last "le réalisateur" could have been annotated into "Le président de la République"
		// but with profCorefAlreadyAnnoted we can directly use the same mark which we have used on previous same term-profession-source.
		//
		// "Le réalisateur Antoine Viktine" -> Antoine Viktine
		// "le réalisateur" -> Antoine Viktine
		// "Le président de la République" 
		// "le réalisateur" -> Antoine Viktine 

		if (profCorefAlreadyAnnoted.containsKey(sourceWords)) {
			Integer sameProfWordIndex = profCorefAlreadyAnnoted.get(sourceWords);
			if (bestSubjectForTheSources.containsKey(sameProfWordIndex)) {
				Integer transitive = bestSubjectForTheSources.get(sameProfWordIndex);
				bestSubjectForTheSources.put(sourceLeftOffset, transitive);
			}
		} else {
			// we are looking for anonymous sources and they start with a non determinate article ("des","une","un")
			String firstWord;
			try {
				firstWord = wordsAndPosByOffset.get(sourceLeftOffset).getFirst();
			} catch (NullPointerException e) {
				TreeMap<Integer, Pair<String, String>> ordered = new TreeMap<>();
				ordered.putAll(wordsAndPosByOffset);
				System.out.println(ordered);
				System.out.println(sourceLeftOffset);
				System.out.println(wordsAndPosByOffset.get(sourceLeftOffset));
				System.out.println(sourceWords);
				System.out.println(sourcePOSs);
				throw e;
			}

			// if not anonymous
			if (!this.resources.NON_DET_ARTICLES.contains(firstWord)) {

				// this variable is THE variable to follow, it gives the best subject index ;)
				int bestSujIndex = -1;

				for (int offsetLeftItem : mapOffsets.keySet()) {

					// the best subject for a source appears ALWAYS before the source itself
					if (offsetLeftItem < sourceRightOffset) {

						Integer itemRightOffset = mapOffsets.get(offsetLeftItem);

						ArrayList<String> subjectWords = new ArrayList<>();
						ArrayList<String> subjectPOSs = new ArrayList<>();

						// we rebuild the subject from its offsets
						putWordsAndPos(wordsAndPosByOffset, offsetLeftItem, itemRightOffset, subjectWords, subjectPOSs);

						// we will mark the source if we are sure that the source is contained into the subject
						// SOURCE : M. Dupont
						// SUBJET : La ferme de Paul Dupont => the source is contained into the source (we stop the PERSON_ABBR)
						// but
						// SOURCE : Paul Dupont
						// SUBJECT : Henri Dupont => only Dupont is contained into the source therefore contain = false 
						// thus we will not have a annotation that "belongs to" someone to ... his brother !

						int countNumberOfSharedWords = 0;
						Boolean contain = true;
						for (String s : sourceWords) {
							if (!this.resources.STOP_WORDS.contains(s.toLowerCase()) && !this.resources.PERSON_ABBR.contains(s))
								if (!subjectWords.contains(s)) {
									contain = false;
								} else {
									countNumberOfSharedWords++;
								}
						}
						// we loop through all words in the subject
						for (int subjectTokenIndex = 0; subjectTokenIndex < subjectWords.size(); subjectTokenIndex++) {
							int lastIndexProf = 0;
							String tokenSuj = subjectWords.get(subjectTokenIndex);

							if (!this.resources.STOP_WORDS.contains(tokenSuj.toLowerCase())) {

								// we loop through all words in the source
								for (int wordIndex = 0; wordIndex < sourceWords.size(); wordIndex++) {

									String tokenSource = sourceWords.get(wordIndex);
									String posTokenSource = sourcePOSs.get(wordIndex);

									// we saves execution time
									if (this.resources.PERSON_ABBR.contains(tokenSource)) {
										continue;
									}

									if (!this.resources.STOP_WORDS.contains(tokenSource.toLowerCase())) {
										// we verify if a token from the source is equal to a token from the subject
										// "Le réalisateur Antoine Viktine" -> "le réalisateur" => réalisateur
										if (tokenSuj.equals(tokenSource) && !this.resources.PERSON_ABBR.contains(tokenSuj)) {

											// we get the token in common
											Pair<String, String> wordAndPosByOffset = wordsAndPosByOffset.get(offsetLeftItem);
											String wordByOffset = wordAndPosByOffset.getFirst();

											// if the token is a NPP (or ET because there are some bad annotations in Stanford)
											// and if the subject contains a NPP (we do not want "le réalisateur" to be marked as "le réalisateur")
											if ((posTokenSource.equals("NPP") || posTokenSource.equals("ET"))
													&& (subjectPOSs.contains("NPP") || subjectPOSs.contains("ET"))) {
												// contain because of the "brother's thing"
												if (!this.resources.PERSON_ABBR.contains(wordByOffset) && contain)
													bestSujIndex = offsetLeftItem;

											}
											// this token is not a NPP
											// if it is a prof word
											else if (this.resources.PROFESSION_WORDS.contains(tokenSource.toLowerCase())) {
												// example : SOURCE : "le roi d'Arabie saoudite"
												//			 SUBJECT : "le roi Salmane d'Arabie saoudite"
												if (contain && countNumberOfSharedWords > 1) {
													// if last subject is not the subject itself because we do not annotate a subject without NPP with itself
													if (lastIndexSujet != offsetLeftItem) {
														bestSujIndex = offsetLeftItem;
													}
													//we verify if the source is also a subject and if the source contains a NPP we annotate on this source the text of this source.
													else if (sourceOffsets.containsKey(offsetLeftItem) && (sourcePOSs.contains("NPP") || sourcePOSs.contains("ET"))) {
														bestSujIndex = offsetLeftItem;
													}
													//otherwise if the source does not contain a NPP, we take the last subject before the source.
													else {
														Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
														if (lastSubject != null)
															bestSujIndex = lastSubject;
													}
												}
												// if there is only one common word (and not NPP word)
												else if (contain && countNumberOfSharedWords == 1) {
													if (lastIndexSujet != -1) {
														// if we are looking for profession (not subject)
														if (subjectMap.equals("prof")) {
															// we put the last profession found before this source
															if (mapOffsets.lowerKey(sourceLeftOffset) != null) {
																lastIndexProf = mapOffsets.lowerKey(sourceLeftOffset);
																bestSujIndex = lastIndexProf;
															}
															// if it is the first profession of the text, we mark it as annotation
															else {
																lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
																bestSujIndex = lastIndexProf;
															}

														}
														// if it is a subject
														else {
															// we always want to put the last subject before the source as annotation
															if (lastIndexSujet != sourceLeftOffset)
																// if subject is not the source itself
																bestSujIndex = lastIndexSujet;
															else {
																// otherwise we look for the last subject before the source
																Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
																if (lastSubject != null) {
																	bestSujIndex = lastSubject;
																	// if we encounter a word that is subject, we keep it to maybe use it again
																	profCorefAlreadyAnnoted.put(sourceWords, lastSubject);
																} else
																	bestSujIndex = lastIndexSujet;
															}
														}
													}
												}
											}
											// if the source is a pronoun (not a NPP word or a profession)
											else if (this.resources.PRONOUNS.contains(tokenSource.toLowerCase())) {
												String lastWord = "";

												if (lastIndexSujet != -1) {
													String wordAtThisIndex = wordsAndPosByOffset.get(lastIndexSujet).getFirst();
													lastWord = wordAtThisIndex;
												}
												// if the last subject is a prof word
												if (lastIndexSujet != -1 && this.resources.PROFESSION_WORDS.contains(lastWord)) {
													bestSujIndex = lastIndexSujet;
												}
												// other we look for the last prof word
												else {
													lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
													if (lastIndexProf != offsetLeftItem) {
														bestSujIndex = lastIndexProf;
													} else {
														bestSujIndex = lastIndexSujet;
													}
												}

											}
											// if not a NPP, profword or pronoun, we look for a person word (femme, homme, jeune, oncle...)
											else if (this.resources.PERSON.contains(tokenSource.toLowerCase())) {
												if (lastIndexSujet != -1) {
													if (lastIndexSujet != sourceLeftOffset)
														bestSujIndex = lastIndexSujet;
													else {
														Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
														if (lastSubject != null)
															bestSujIndex = lastSubject;
														else
															bestSujIndex = lastIndexSujet;
													}
												}
											}
										}
										// if there is not any token in subject equal to a token in the source
										// il -> le ministre
										else if (this.resources.PRONOUNS.contains(tokenSource.toLowerCase())) {
											// if there is a last subject
											if (lastIndexSujet != -1) {
												bestSujIndex = lastIndexSujet;
											}
											// otherwise we look for a profession word
											else if (subjectMap.equals("prof")) {
												lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
												if (lastIndexProf != offsetLeftItem)
													bestSujIndex = lastIndexProf;
											}

										}
									}
								}
							}
							if (subjectMap.equals("subject"))
								lastIndexSujet = mapOffsets.lowerKey(sourceRightOffset);
						}

					}
					// if there is subject found
					if (bestSujIndex != -1) {
						// if this subject has already been marked for another source, we do a transitive relation
						if (bestSubjectForTheSources.containsKey(bestSujIndex)) {
							Integer transitive = bestSubjectForTheSources.get(bestSujIndex);
							bestSubjectForTheSources.put(sourceLeftOffset, transitive);
						}
						// otherwise we do mark the new subject for the source
						else {
							bestSubjectForTheSources.put(sourceLeftOffset, bestSujIndex);
						}
					}
				}

			}
			// if anonymous source
			else if (this.resources.NON_DET_ARTICLES.contains(firstWord)) {
				bestSubjectForTheSources.put(sourceLeftOffset, -1);
			}
		}

		return lastIndexSujet;

	}

	private void putWordsAndPos(HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, int offsetLeftItem, Integer itemRightOffset, ArrayList<String> sWords,
			ArrayList<String> sPOSs) {
		for (int offsetInSubject = offsetLeftItem; offsetInSubject < itemRightOffset; offsetInSubject++) {
			Pair<String, String> wordAndPOS = wordsAndPosByOffset.get(offsetInSubject);
			if (wordAndPOS != null) {
				sWords.add(wordAndPOS.getFirst());
				sPOSs.add(wordAndPOS.getSecond());
			}
		}
	}

	/**
	 * Cette méthode permet de garder 4 TreeMap : <br>
	 * - Une pour les sujets et leurs offsets, <br>
	 * - Une pour les noms des professions et leurs offsets (ministre Bernard Cazeneuve, l'imprimeur Michel
	 * Catalano...), <br>
	 * - Une pour les pronoms et leurs offsets. <br>
	 * - Une pour les sources et leurs offsets. <br>
	 * 
	 * @param wapitiResult
	 *            Le fichier en sortie de wapiti
	 * @param textWithOffset
	 *            On parse de nouveau le texte pour simplement récupérer les offsets des mots
	 * @param sourceOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les sources de la forme A=B
	 * @param subjectOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les sujets de la forme A=B
	 * @param profAndSecondarySourceTriggerOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les professions de la forme A=B
	 * @param pronounOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les pronoms de la forme A=B
	 * 
	 */
	private static HashMap<Integer, Pair<String, String>> findSubjects(String wapitiResult, String textWithOffset, TreeMap<Integer, Integer> sourceOffsets,
			TreeMap<Integer, Integer> subjectOffsets, TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			Resources resources, Memory memory) {

		Scanner parseWapitiResultScanner = new Scanner(wapitiResult);
		Scanner parseTextOffsetScanner = new Scanner(textWithOffset);

		String wapitiResultLine;
		String textOffsetLine;

		// this HashMap will contain all leftOffset of each word and for each offset it will contain a Pair of the word and the PoS at this offset
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = new HashMap<>();

		// this counter prevents to keep a subject in a sentence if one source is also in this same sentence 
		// sources have priority over the subjects 
		int numberOfSourcesInThatSentence = 0;

		// we need to loop though because parseWapitiResultScanner contains the tags (SUJ)
		// and parseTextOffsetScanner contains the offsets
		while (parseWapitiResultScanner.hasNextLine() && parseTextOffsetScanner.hasNextLine()) {

			wapitiResultLine = parseWapitiResultScanner.nextLine();
			textOffsetLine = parseTextOffsetScanner.nextLine();

			// we reset the counter when it is a new line (new line symbolizes beginning and end of sentence)
			if (wapitiResultLine.length() == 1 && textOffsetLine.length() == 1) {
				numberOfSourcesInThatSentence = 0;
				continue;
			}

			if (wapitiResultLine.length() > 1 && textOffsetLine.length() > 1) {
				String[] wapitiResultToken = wapitiResultLine.split("\\s+");

				String currentWord = wapitiResultToken[0];
				String currentPosTag = wapitiResultToken[1];
				String currentLemma = wapitiResultToken[2];
				String sourceSecTrigger = wapitiResultToken[7];
				String currentSubject = wapitiResultToken[8];

				String[] offsetTextToken = textOffsetLine.split("\\s+");
				String offset = "";
				//				if (onlyOffset)
				//					offset = offsetTextToken[4];
				//				else 
				offset = offsetTextToken[numberFieldsPrim];

				String[] offsetsStr = offset.split(":");
				assert offsetsStr.length == 2 : "offsetsStr.length" + "must be equal to 2";

				int offsetLeft = Integer.parseInt(offsetsStr[0]);
				int lastOffsetRight = Integer.parseInt(offsetsStr[1]);
				int tempLastOffset = lastOffsetRight;

				boolean isSubject = currentSubject.equals("SUJ");
				boolean isProf = resources.PROFESSION_WORDS.contains(currentWord.toLowerCase());
				boolean isPronoun = resources.PRONOUNS.contains(currentLemma);
				boolean isSourceSecTrigger = false;

				if (sourceSecTrigger.equals("SOURCE_SEC_TRIGGER")) {
					isSourceSecTrigger = true;
				}
				// Stanford has marked as word "-t-il" or "-il", we need to fix it and keep only "il"
				// we keep all words in wordsAndPosByOffset
				//				if (isPronoun) {
				//					System.out.println();
				//				}
				if (isPronoun && currentWord.startsWith("-t-")) {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(3), currentPosTag));
					offsetLeft += 3;
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(3), currentPosTag));
				} else if (isPronoun && currentWord.startsWith("-")) {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(1), currentPosTag));
					offsetLeft += 1;
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(1), currentPosTag));
				} else {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord, currentPosTag));
				}

				boolean isSource = sourceOffsets.containsKey(offsetLeft);

				// we keep only the subjects and the names of profession. We ignore subjects that could be a country or a capital
				// we also keep all primary sources and the trigger which triggers a secondary source
				if ((isSubject || isProf || isSource || isSourceSecTrigger) && !isGeographic(currentWord, resources)) {
					String newPosTag = currentPosTag;

					//we ignore some lemma to keep only important subject (c'est -> subject = c')
					if (!currentLemma.equals("c'") && !currentLemma.equals("cela") && !currentLemma.equals("nous") && !currentLemma.equals("tu")
							&& !currentLemma.equals("où")) {

						boolean keepIt = false;
						boolean foundNPP = false;

						// person contains word like "femme","homme"
						if (resources.PERSON.contains(currentLemma))
							keepIt = true;

						if (isSourceSecTrigger)
							keepIt = true;

						if (isSource) {
							numberOfSourcesInThatSentence++;
							keepIt = true;
						}

						if (isSubject && isPronoun)
							keepIt = true;

						if (isNPP(currentPosTag)) {
							keepIt = true;
							foundNPP = true;
						} else if (resources.PROFESSION_WORDS.contains(currentLemma)) {
							keepIt = true;
						}

						// we read the next word because a subject can start with "le" and we need to know "le what"
						String newWapitiResultLine = parseWapitiResultScanner.nextLine();
						String newTextOffsetLine = parseTextOffsetScanner.nextLine();

						String newLemma = "";
						String newWord = "";
						String newSourceSecTrigger = "";

						if (newWapitiResultLine.length() > 1 && newTextOffsetLine.length() > 1) {

							String[] newWapitiResultToken = newWapitiResultLine.split("\\s+");

							String[] newOffsetTextToken = newTextOffsetLine.split("\\s+");
							String newOffset = "";
							//							if (onlyOffset)
							//								newOffset = newOffsetTextToken[4];	
							//							else 
							newOffset = newOffsetTextToken[numberFieldsPrim];

							String[] newOffsetsStr = newOffset.split(":");
							assert offsetsStr.length == 2 : "offsetsStr.length" + " must be equal to 2 (" + offsetsStr.length + ")";

							// get left offset of the new word
							int newOffsetLeft = Integer.parseInt(newOffsetsStr[0]);

							newWord = newWapitiResultToken[0];
							newPosTag = newWapitiResultToken[1];
							newLemma = newWapitiResultToken[2];
							newSourceSecTrigger = newWapitiResultToken[7];

							if (resources.PERSON.contains(newLemma))
								keepIt = true;

							if (isNPP(newPosTag)) {

								keepIt = true;
								foundNPP = true;

							} else if (resources.PROFESSION_WORDS.contains(newLemma)) {
								keepIt = true;
							}

							if (newSourceSecTrigger.equals("SOURCE_SEC_TRIGGER"))
								keepIt = true;

							// we do not forget to add this new word into wordsAndPosByOffset 
							wordsAndPosByOffset.put(newOffsetLeft, new Pair<String, String>(newWord, newPosTag));

							// basicCategory is used with malt parser, it contains simplified PoS tagging
							String cgPos = resources.COARSE_GRAINED_POS_MAP.get(newPosTag);

							// we continue if we have a "N", "A", "D" or "P" word as well as PERSON_ABBR word (la ministre de l'Environnement Mme Royal)
							if (isSuperSensePos(cgPos) || resources.PERSON_ABBR.contains(newWord) || cgPos.equals("P")) {
								// we loop through the next words and we are more selective about which word we keep
								while (((newPosTag.equals("ET") || newWord.equals(",") || isSuperSensePos(cgPos) || (cgPos.equals("P") && !foundNPP)))
										&& !resources.TEMPORAL_EXPRESSIONS.contains(newLemma) && !newWord.equals("à") && !resources.PERSON_ABBR.contains(newWord)
										&& parseWapitiResultScanner.hasNextLine() && parseTextOffsetScanner.hasNextLine()) {

									if (isNPP(newPosTag)) {
										keepIt = true;
										foundNPP = true;
									} else if (resources.PROFESSION_WORDS.contains(newLemma)) {
										keepIt = true;
									} else if (resources.PERSON.contains(newLemma)) {
										keepIt = true;
									}

									if (newSourceSecTrigger.equals("SOURCE_SEC_TRIGGER"))
										keepIt = true;

									// next word
									newWapitiResultLine = parseWapitiResultScanner.nextLine();
									newTextOffsetLine = parseTextOffsetScanner.nextLine();

									if (newWapitiResultLine.length() > 1 && newTextOffsetLine.length() > 1) {
										newWapitiResultToken = newWapitiResultLine.split("\\s+");
										newWord = newWapitiResultToken[0];
										newPosTag = newWapitiResultToken[1];
										newLemma = newWapitiResultToken[2];
										newSourceSecTrigger = newWapitiResultToken[7];

										// Get left offset of the new word
										newOffsetTextToken = newTextOffsetLine.split("\\s+");
										//										if (onlyOffset)
										//											newOffset = newOffsetTextToken[4];		
										//										else 
										newOffset = newOffsetTextToken[numberFieldsPrim];

										newOffsetsStr = newOffset.split(":");
										if (offsetsStr.length != 2) {
											System.out.println(offsetsStr.length + "must be equal to 2");
											System.exit(1);
										}

										lastOffsetRight = Integer.parseInt(newOffsetsStr[1]);
										newOffsetLeft = Integer.parseInt(newOffsetsStr[0]);

										tempLastOffset = newOffsetLeft;

										wordsAndPosByOffset.put(newOffsetLeft, new Pair<String, String>(newWord, newPosTag));
										cgPos = resources.COARSE_GRAINED_POS_MAP.get(newPosTag);
									}
								}
							}

						}

						// if it is a important word we go through this
						if (keepIt) {
							// we do not keep sources which are pronoun and we add each source in the subject list
							if (isSource && !isPronoun) {
								subjectOffsets.put(offsetLeft, sourceOffsets.get(offsetLeft));
							}
							// we do not keep subjects which are pronoun and the most importantly thing is that we do not keep 
							// subject if we have kept a source in this sentence
							else if (isSubject && !isPronoun && numberOfSourcesInThatSentence == 0) {
								subjectOffsets.put(offsetLeft, tempLastOffset);
							}
							// we keep in another Map all profession word like "le ministre des sports", "le policier Ferdinand"...
							else if (isProf) {
								profAndSecondarySourceTriggerOffsets.put(offsetLeft, tempLastOffset);
							}
							// we keep all pronoun in another list
							else if (isPronoun) {
								pronounOffsets.put(offsetLeft, tempLastOffset);
							}
							// we also keep all word in relation with the secondary sources
							else if (isSourceSecTrigger) {
								profAndSecondarySourceTriggerOffsets.put(offsetLeft, tempLastOffset);
							}

						}

					}
				}
			}

		}

		parseWapitiResultScanner.close();
		parseTextOffsetScanner.close();
		return wordsAndPosByOffset;
	}

	private static boolean isGeographic(String currentWord, Resources resources) {
		boolean isGeographic = resources.COUNTRIES_AND_CONTINENTS.contains(currentWord) || resources.CAPITALS.contains(currentWord);
		return isGeographic;
	}

	private static boolean isSuperSensePos(String cgPos) {
		boolean isCommonPos = cgPos.equals("N") || cgPos.equals("A") || cgPos.equals("D");
		return isCommonPos;
	}

	private static boolean isNPP(String posTag) {
		boolean isNPP = posTag.equals("NPP") || posTag.equals("ET");
		return isNPP;
	}

	/**
	 * Extracts content within
	 * <p>
	 * element
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	protected static String extractTextFromXML(String fileId, File inFile, File outFile, Memory memory) throws IOException {
		// Let's do it simply, so that even ill-formed XML will make it
		Pattern pattern = Pattern.compile("<p>([^<]+)</p>");
		String xmlText = Files.toString(inFile, Charsets.UTF_8);
		Matcher matcher = pattern.matcher(xmlText);
		StringBuilder result = new StringBuilder();
		while (matcher.find()) {
			result.append(matcher.group(1) + " ");
		}
		String resultStr = result.toString();
		if (outFile.exists()) {
			outFile.delete();
		}
		resultStr.replaceAll("\\r", "");
		Files.append(resultStr, outFile, Charsets.UTF_8);
		memory.docTexts.put(fileId, resultStr);
		return resultStr;
	}

	/**
	 * Méthode utilisée pour parcourir les dossiers quand on a des fichiers de test non annotés.<br />
	 * On l'utilise avec un dossier dir et le comportement sera différent selon le dossier entrée.<br />
	 * Fonctionne avec seulement 2 dossiers :<br />
	 * &nbsp;&nbsp; - Celui de tous les fichiers .txt DEJA ANNOTES POUR LES PHASES DE TRAIN ET DE DEV avec les fichiers
	 * .ann qui vont avec,<br />
	 * &nbsp;&nbsp; - Celui où se trouvent / trouveront les fichiers .tag annotés automatiquement avec Wapiti (voir
	 * script bio_wapiti_ann.sh).\n<br />
	 * <br />
	 * On parcourt les fichiers dans le dossier en entrée.<br />
	 * &nbsp;&nbsp;- si on est dans le dossier d'input, on appelle la fonction parseFileToTag sur chaque fichier .txt
	 * pour tag chaque fichier (on "continue" quand fichier .ann).<br />
	 * Ensuite on va lire tous les fichiers dans le dossier des fichiers non annotés pour les transformer en format BIO
	 * (toutes les lignes auront comme tag un O).<br />
	 * <br />
	 * &nbsp;&nbsp;- si on est dans le fichier de sortie de Wapiti, on prend tous les fichiers .tag et pour chaque
	 * fichier on appelle la fonction parseFileToAnn pour annoter automatiquement chaque fichier.<br />
	 * <br />
	 * 
	 * @param input
	 *            Le dossier en entrée (input_labeled ou bioWapiti)
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void parseDirForUnlabeled(File input, boolean xmlInput, int jobNumber) throws IOException, InterruptedException {

		if (input.isDirectory()) {

			int nbFilesDir = input.list().length;
			int countTxt = 0;

			// for the training phase we need to take all LABELED input_files to test on unlabeled files
			if (input.getName().equals(this.paths.DIR_INPUT_LABELED.getName()) || (input.equals(this.paths.DIR_INPUT_UNLABELED) && PRODUCTION_MODE)) {
				// If input = DIR_INPUT_FILE, we are in production mode, and we apply
				// the same procedure as for the UNLABELED mode, except that there is no training step.
				boolean skipTraining = false;
				if (PRODUCTION_MODE) {
					skipTraining = true;
				}
				int countUnlabeled = 0;
				int nbFileUnlabeled = this.paths.DIR_INPUT_UNLABELED.list().length;

				// this loop is for the training phase files
				if (!skipTraining) {
					for (File inFile : input.listFiles()) {
						if (inFile.isFile()) {
							String name = inFile.getName();
							String fileId = name.substring(0, name.lastIndexOf("."));

							if (name.endsWith(".txt")) {
								countTxt++;
								System.out.println("TRAIN & DEV LABELED FOR UNLABELED: Traitement du fichier : " + countTxt + "/" + (nbFilesDir / 2));
								parseFileToTag(fileId, inFile, nbFilesDir / 2);
							} else if (name.endsWith(".ann")) {
								continue;
							} else {
								System.out.println("Format de fichier inconnu : " + name + " -> seulement .txt ou .ann");
								System.exit(1);
							}

						}
					}
				}
				// this loop is for the labeling phase files
				ProcessingThreadFactory threadFactory = new ProcessingThreadFactory();
				ForkJoinPool forkJoinPool = new ForkJoinPool(jobNumber, threadFactory, null, false);
				ArrayBlockingQueue<PreprocessingTask> forks = new ArrayBlockingQueue<>(1000000);

				for (File inFile : this.paths.DIR_INPUT_UNLABELED.listFiles()) {
					if (inFile.isFile() && !inFile.getName().endsWith(".ann")) {
						countUnlabeled++;
						PreprocessingTask task = new PreprocessingTask(inFile, xmlInput, countUnlabeled, nbFileUnlabeled, tools, resources, paths, memory);
						forks.put(task);
						forkJoinPool.submit(task);
						//						parseUnlabeledFile(inFile, xmlInput, countUnlabeled, nbFileUnlabeled, 
						//								this.tools, this.resources, this.paths, memory);
						//						deleteSentenceWithoutPrimForSec	(outFileSEC);

					}
				}
				while (forks.size() > 0) {
					PreprocessingTask task = forks.take();
					task.join();
				}

			}
			// convert the wapiti SOURCE-PRIM output files into brat format files
			else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM.getName())) {
				int countUnlabeled = 0;
				for (File inFile : input.listFiles()) {
					if (inFile.isFile()) {
						countUnlabeled++;
						String filename = inFile.getName();
						String fileId = filename.substring(0, filename.lastIndexOf("."));

						if (!filename.endsWith(".wapiti")) {
							System.err.println("Format de fichier inconnu : " + filename + " -> seulement .wapiti");
							System.exit(1);
						}
						System.err.println("  conversion Wapiti PRIM -> Brat " + countUnlabeled + "/" + (nbFilesDir) + " " + filename);

						parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, PRIM, this.tools, this.paths, this.resources, this.memory);
					}
				}
			}
			// convert the wapiti SOURCE-SEC output files into brat format files
			else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC.getName())) {
				int countUnlabeled = 0;
				for (File inFile : input.listFiles()) {
					if (inFile.isFile()) {
						countUnlabeled++;
						String filename = inFile.getName();
						String fileId = filename.substring(0, filename.lastIndexOf("."));

						if (!filename.endsWith(".wapiti")) {
							System.err.println("Format de fichier inconnu : " + filename + " -> seulement .wapiti");
							System.exit(1);
						}
						System.err.println("  conversion Wapiti SEC -> Brat " + countUnlabeled + "/" + (nbFilesDir) + " " + filename);

						parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, SEC, this.tools, this.paths, this.resources, this.memory);
					}
				}
			}
			// add annotation JSON and BRAT
			else if (input.getName().equals(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.getName())) {
				int countUnlabeled = 0;
				for (File inFile : input.listFiles()) {
					if (inFile.isFile()) {
						countUnlabeled++;
						String filename = inFile.getName();
						String fileId = filename.substring(0, filename.lastIndexOf("."));

						assert filename.endsWith(".ann") : "Unknown file format: " + filename + " -> should be .ann";

						System.err.println("ADD ANNOTATION : Traitement du fichier : " + countUnlabeled + "/" + (nbFilesDir));
						//						System.out.println(inFile.getName());
						File outputFile = parseFileToAddAnnotationToBrat(fileId, inFile);

						System.err.println("1. Output written in " + outputFile.getAbsolutePath());

						// If XML input, then we can delete the TXT file in the input directory
						if (xmlInput) {
							(new File(this.paths.DIR_INPUT_UNLABELED, filename.replaceAll("[.][^.]+$", ".txt"))).delete();
						}

					}
				}

			} else {
				System.out.println("Le dossier " + input.getName() + " n'est pas un dossier possible.");
			}
		} else if (input.isFile()) {
			throw new RuntimeException();
		}
	}

	//	private static void parseUnlabeledFile(File inFile, boolean xmlInput, 
	//			int countUnlabeled, int nbFileUnlabeled, Tools tools, Resources resources, Paths paths,
	//			Memory memory)
	//			throws IOException {
	//		String filename = inFile.getName();
	//		String fileId = inFile.getName().substring(0, filename.lastIndexOf("."));
	//
	//		System.err.println("Start labeling file : "+ countUnlabeled+"/"+(nbFileUnlabeled) + " " + fileId);
	//
	//		File outFilePRIM = null;
	//		File outFileSEC = null;
	//		String text;
	//		File txtFile = new File(paths.DIR_INPUT_UNLABELED, fileId + ".txt");
	//		if (xmlInput) {
	//			text = extractTextFromXML(fileId, new File(paths.DIR_INPUT_UNLABELED, fileId+".xml"), txtFile, memory);
	//		}
	//		else {
	//			text = getDocText(fileId, txtFile, memory);
	//		}
	//
	//		text = text.replaceAll("\\r", "");
	//
	//		// we create the files in which we will write the results
	//		outFilePRIM = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_PRIM, fileId, ".tag");
	//		outFileSEC = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_SEC, fileId, ".tag");
	//		File outFileMP = DIRUtils.createDirAndFilesWithExt(paths.DIR_INPUT_UNLABELED_MP, fileId, ".conll");
	//
	//		// parseText + maltParser + (b)io conversion
	//		tagAndConvert(fileId, inFile, text, outFileSEC, outFileMP, "", tools, resources, memory);
	//
	//		// we delete the last column of (B)io files to find the SOURCE-PRIM
	//		deleteLastColumnForPrim(outFilePRIM, outFileSEC, true);
	//		outFileSEC.delete();
	//	}

	/**
	 * Méthode qui permet de tag le fichier inFile en entrée avec la fonction parseText et de trouver les sujets avec
	 * parseTextForMaltParser.<br />
	 * On définit 3 dossiers (train, dev, test) utiles pour le CRF. Ces dossiers changent selon si on est dans le cas de
	 * fichiers de test déjà annotés ou non.<br />
	 * <br />
	 * On va ensuite rassembler les résultats des deux parseText pour transformer le résultat final en un fichier
	 * automatiquement transformé en fichier .tag (écrit avec un encoding (B)IO) par la méthode bioConversion qui sera
	 * mis dans un des 3 dossiers.<br />
	 * <br />
	 * Si labeled : on met les 75% premiers fichiers dans le dossier train, puis les 10 % suivant dans un dossier dev et
	 * les 15% restant dans un dossier de test. (on crée (ou efface puis crée de nouveau) les dossiers)<br />
	 * <br />
	 * Si !labeled : on met les 90% premiers fichiers du dossier input_labeled dans un dossier train et les 10% suivant
	 * dans un dossier dev (on a besoin d'utiliser les fichiers déjà annotés pour les phases d'entrainement et de
	 * développement). Le dossier de test est créé automatiquement avec la fonction parseDirUnlabeled.
	 * 
	 * 
	 * @param inFile
	 *            Le fichier qu'il faut tag.
	 * @param nbFiles
	 *            Le nombre de fichiers dans le dossier pour pouvoir mettre les x% dans le dossier spécifique
	 * @return
	 * @throws IOException
	 */
	public String parseFileToTag(String fileId, File inFile, int nbFiles) throws IOException {

		String text = getDocText(fileId, inFile, memory);

		text = text.replaceAll("\\r", "");

		File trainDirPRIM = null;
		File testDirPRIM = null;
		File devDirPRIM = null;

		File trainDirSEC = null;
		File testDirSEC = null;
		File devDirSEC = null;

		File outFileSEC = null;
		File outFilePRIM = null;
		File outFileMP = null;

		if (LABELED_MODE) {
			trainDirPRIM = this.paths.DIR_TRAIN_FILES_PRIM;
			testDirPRIM = this.paths.DIR_TEST_FILES_PRIM;
			devDirPRIM = this.paths.DIR_DEV_FILES_PRIM;
			trainDirSEC = this.paths.DIR_TRAIN_FILES_SEC;
			testDirSEC = this.paths.DIR_TEST_FILES_SEC;
			devDirSEC = this.paths.DIR_DEV_FILES_SEC;
		} else {
			trainDirPRIM = this.paths.DIR_TRAIN_FILES_UNLABELED_PRIM;
			testDirPRIM = this.paths.DIR_TEST_FILES_UNLABELED_PRIM;
			devDirPRIM = this.paths.DIR_DEV_FILES_UNLABELED_PRIM;
			trainDirSEC = this.paths.DIR_TRAIN_FILES_UNLABELED_SEC;
			testDirSEC = this.paths.DIR_TEST_FILES_UNLABELED_SEC;
			devDirSEC = this.paths.DIR_DEV_FILES_UNLABELED_SEC;
		}

		counterFiles += 1;

		boolean test = false;

		if (LABELED_MODE) {
			// Files distribution : 
			// 75% train, 10% dev, 15% test.
			if (counterFiles < 0.75 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(trainDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(trainDirPRIM, fileId, ".tag");
			} else if (counterFiles > 0.75 * nbFiles && counterFiles < 0.85 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(devDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(devDirPRIM, fileId, ".tag");
			} else {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(testDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(testDirPRIM, fileId, ".tag");
				test = true;
			}
		} else {
			// Files distribution : 
			// 90% train, 10% dev
			// all files unlabeled in test.
			if (counterFiles < 0.90 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(trainDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(trainDirPRIM, fileId, ".tag");
			} else {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(devDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(devDirPRIM, fileId, ".tag");
			}
		}

		String ann = Files.toString(new File(this.paths.DIR_INPUT_LABELED_CONVERTED, fileId + ".ann"), Charsets.UTF_8);

		outFileMP = DIRUtils.createDirAndFilesWithExt(this.paths.DIR_INPUT_LABELED_MP, fileId, ".conll");

		String parsedText = tagAndConvert(fileId, inFile, text, outFileSEC, outFileMP, ann, this.tools, this.resources, memory);

		deleteLastColumnForPrim(outFilePRIM, outFileSEC, test);
		if (test) {
			outFileSEC.delete();
		} else {
			deleteSentenceWithoutPrimForSec(outFileSEC, outFileSEC, false);
		}

		return parsedText;
	}

	/**
	 * inSec and outSEC can be the same
	 * 
	 * @param inSEC
	 * @param outSEC
	 * @param testMode
	 * @throws IOException
	 */
	private void deleteSentenceWithoutPrimForSec(File inSEC, File outSEC, boolean testMode) throws IOException {
		if (inSEC.isDirectory()) {
			for (File file : inSEC.listFiles()) {
				deleteSentenceWithoutPrimForSec(file, new File(outSEC, file.getName()), testMode);
			}
		} else if (inSEC.isFile()) {
			Scanner scan = new Scanner(inSEC);
			StringBuilder result = new StringBuilder();

			StringBuilder sentenceBuffer = new StringBuilder();
			boolean foundPrim = false;

			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.length() > 1) {
					String[] fields = line.split("\\s+");
					String primClassName;
					if (testMode) {
						primClassName = fields[fields.length - 1];
					} else {
						primClassName = fields[numberFieldsPrim];
					}

					assert primClassName.endsWith(PRIM) || primClassName.equals(OUT) : "You got the wrong column!";

					if (!primClassName.equals(OUT)) {
						foundPrim = true;
					}
					sentenceBuffer.append(line + "\n");
				} else {
					if (foundPrim) {
						result.append(sentenceBuffer.toString());
						result.append("\n");
						foundPrim = false;
					}
					//				else {
					//					// add a newline so that we know that a sentence
					//					// was skipped
					//					output.write("\n");
					//				}
					sentenceBuffer = new StringBuilder();
				}

			}
			scan.close();
			Files.write(result.toString(), outSEC, Charsets.UTF_8);
		} else {
			throw new RuntimeException();
		}
	}

	protected static void deleteLastColumnForPrim(File outFilePRIM, File inFileSec, boolean testMode) throws IOException {
		Scanner scan = new Scanner(inFileSec);
		FileWriter fw = new FileWriter(outFilePRIM, true);
		BufferedWriter output = new BufferedWriter(fw);
		output.write("");

		StringBuilder sb = new StringBuilder();

		// We keep all fields to train secondary sources but for the primary ones we need to remove the last column
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			if (line.length() > 1) {
				String[] fields = line.split("\\s+");
				int size = fields.length;
				// we write each field but the last one.
				int limit = size - 1;
				// if in test mode, also remove the reference SOURCE-PRIM field
				// so that it will not be used by the SOURCE-SEC test
				if (testMode) {
					limit--;
				}
				for (int fieldIndex = 0; fieldIndex < limit; fieldIndex++) {
					sb.append(fields[fieldIndex] + " ");

				}
				sb.append("\n");
			} else {
				sb.append("\n");
			}
		}

		output.append(sb);
		output.close();
		scan.close();
	}

	protected static String tagAndConvert(String fileId, File inFile, String text, File outFileSEC, File outFileMP, String ann, Tools tools, Resources resources,
			Memory memory) throws IOException {
		// we use malt parser to keep the subjects found
		String maltParsertext = parseTextForMaltParser(text, inFile, outFileMP, tools, resources);

		// we parse the text to tag each word of this text.
		String parsedText = parseText(fileId, text, tools, resources, memory);

		// we join both parse text
		String taggedText = tagFromMaltParserAndParseText(parsedText, maltParsertext, resources);

		// we convert this text in (B)IO and we write the result into the file outFileSEC
		bioConversion(taggedText, ann, outFileSEC);
		return parsedText;
	}

	/**
	 * Méthode permettant de rassembler les informations trouvées avec malt parser et avec le parse text.
	 * 
	 * @param parsedText
	 *            Texte tag provenant de la fonction parseText
	 * @param maltResult
	 *            Text provenant de la fonction parseTextForMaltParser
	 * @return Une jointure des deux textes
	 */
	private static String tagFromMaltParserAndParseText(String parsedText, String maltResult, Resources resources) {
		StringBuilder sb = new StringBuilder();

		FeatureSet myText = null;

		Scanner parsedTextScanner = new Scanner(parsedText);
		String parsedTextLine;

		Scanner maltResultScanner = new Scanner(maltResult);
		String maltResultLine;

		HashMap<String, String> sameIndexForSubject = new HashMap<>();
		HashSet<String> triggerVerbIndices = new HashSet<>();

		// at first we loop through the result of malt parser
		while (maltResultScanner.hasNextLine()) {
			maltResultLine = maltResultScanner.nextLine();
			if (maltResultLine.length() > 1) {

				String[] malt = maltResultLine.split("\\s+");
				// this is the id of the current line
				String currentLine = malt[0];
				String lemma = malt[2];
				String pos = malt[3];
				// this is the syntactic relationship between HEAD and this word
				String deprel = malt[6];
				// this is the syntactic annotation of this word found by malt parser
				String syntacticAnnotation = malt[7];

				String indexForDeprel = "";

				int indexOfCurrentLine = currentLine.lastIndexOf("-");

				if (indexOfCurrentLine > 0) {
					indexForDeprel = currentLine.substring(0, indexOfCurrentLine);
				}

				// if malt parser marked this word as a subject so we keep it
				if (syntacticAnnotation.equals("suj")) {
					sameIndexForSubject.put(currentLine, indexForDeprel + "-" + deprel);
				}

				// we keep each verbs which are contained in citVerbs
				if (pos.equals("V") && resources.CITATION_VERBS.contains(lemma)) {
					triggerVerbIndices.add(currentLine);
				}
			}
		}

		// Then we loop through the parsed text
		while (parsedTextScanner.hasNextLine()) {
			parsedTextLine = parsedTextScanner.nextLine();
			if (parsedTextLine.length() > 1) {

				String[] parsed = parsedTextLine.split("\\s+");

				String idLine = parsed[0];
				String word = parsed[1];
				String pos = parsed[2];
				String lemma = parsed[3];
				String isTrigger = parsed[4];
				String isProf = parsed[5];
				String QM = parsed[6];
				String isMedia = parsed[7];
				String mix_col = parsed[8];
				String offsetPrim = parsed[9];

				myText = new FeatureSet(word, pos, lemma, isTrigger, isProf, QM, isMedia, mix_col, offsetPrim, offsetPrim);

				// we check if malt parser marked this current line as a subject
				if (sameIndexForSubject.containsKey(idLine)) {
					myText.setIsSuj();
					// we also check if this subject is the subject of a quotation verb
					if (triggerVerbIndices.contains(sameIndexForSubject.get(idLine))) {
						myText.setIsSujForTrigger();
					}
				}
				sb.append(myText + "\n");

			} else {
				sb.append("\n");
			}

		}

		maltResultScanner.close();
		parsedTextScanner.close();
		return sb.toString();
	}

	private static String runMaltParser(String maltParsertext, File inFile, File outFileMP, Tools tools) throws IOException {

		FileWriter fw = new FileWriter(outFileMP, true);
		BufferedWriter output = new BufferedWriter(fw);
		output.write("");

		String[] tokens = maltParsertext.split("\\r?\\n");
		StringBuilder sb = new StringBuilder();
		try {

			String[] outputTokens = tools.getMaltParserModel().parseTokens(tokens);

			for (int i = 0; i < outputTokens.length; i++) {
				sb.append(outputTokens[i] + "\t\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		output.append(sb);
		output.close();

		return sb.toString();
	}

	/**
	 * Méthode qui permet de démarrer la conversion d'un fichier BIO en un fichier BRAT.<br />
	 * <br />
	 * On commence par récupérer le .txt du fichier BIO à convertir puis on le tag avec la méthode parseText pour avoir
	 * les offsets des mots.<br />
	 * <br />
	 * Ensuite on sauvegarde le contenu du fichier BIO généré par wapiti en entrée dans une variable annAuto puis on
	 * appelle la fonction annConversion<br />
	 * qui va convertir le fichier.<br />
	 * <br />
	 * 
	 * @param inFile
	 *            Le fichier de prédiction écrit par la phase de test de Wapiti.
	 * @param dirAuto
	 *            Le dossier où enregistrer le résultat de la conversion.
	 * @throws IOException
	 */
	public static void parseFileToAnn(String fileId, File inFile, File dirAuto, String sourceExt, Tools tools, Paths paths, Resources resources, Memory memory)
			throws IOException {

		String text = "";

		//		if (!PRODUCTION_MODE) {
		if (LABELED_MODE) {
			text = getDocText(fileId, new File(paths.DIR_INPUT_LABELED, fileId + ".txt"), memory);
		} else {
			text = getDocText(fileId, new File(paths.DIR_INPUT_UNLABELED, fileId + ".txt"), memory);
		}
		//		}
		//		else {
		//			text = Files.toString(new File(DIR_INPUT_FINAL, filename+".txt"), Charsets.UTF_8);
		//			pos = filename.lastIndexOf("_");
		//			if (pos > 0) {
		//				filename = filename.substring(0, pos);
		//			}
		//		}

		String tagWithOffset = parseText(fileId, text, tools, resources, memory);

		String resultWapiti = "";
		String line;

		// Read Wapiti output file
		BufferedReader br = new BufferedReader(new FileReader(inFile));

		while ((line = br.readLine()) != null) {
			// If we are annotating secondary sources,
			// we want to make sure that they won't overlap
			// with previously annotated primary sources
			if (sourceExt.equals(SEC)) {
				String[] fields = line.split("\\s+");
				String primInfo;

				if (fields.length == 1) {
					resultWapiti += "\n";
					continue;
				}

				// the last fields before class is the primary source BIO information
				// If the data is labeled, this field is n-2, otherwise n-1
				//				if (LABELED_MODE) {
				primInfo = fields[fields.length - 2];
				//				}
				//				else {
				//					primInfo = fields[fields.length - 1];					
				//				}
				if (!(primInfo.endsWith(PRIM) || primInfo.equals(OUT))) {
					System.out.println();
				}
				assert primInfo.endsWith(PRIM) || primInfo.equals(OUT) : primInfo;
				// if the primary source information is not "O",
				// then the secondary source information (class) must be set to "O"
				if (!primInfo.equals(OUT)) {
					fields[fields.length - 1] = OUT;
				}
				resultWapiti += String.join(" ", fields) + "\n";
			}
			// If we are annotating primary sources,
			// we just take the BIO output as it is
			else {
				resultWapiti += line + "\n";
			}
		}

		annConversion(tagWithOffset, resultWapiti, fileId, text, dirAuto, sourceExt);

		br.close();
	}

	protected static String getDocText(String fileId, File file, Memory memory) throws IOException {
		String text = memory.docTexts.get(fileId);
		if (text == null) {
			text = Files.toString(file, Charsets.UTF_8);
			text = text.replaceAll("\\r", "");
			memory.docTexts.put(fileId, text);
		}
		return text;
	}

	/**
	 * Méthode qui va tag le texte passé en paramètre. Utilisée dans la fonction parseFileToTag qui récupère le résultat
	 * du tagging avec l'aide d'un StringBuilder.<br />
	 * <br />
	 * Nous utilisons le parser de Stanford CoreNLP pour donner le tag PoS à chaque mot du texte et pour donner le lemme
	 * qui va avec chaque mot.<br />
	 * <br />
	 * Nous utilisons ensuite une liste de verbes qui traduisent une action de citation (citVerbs) et pour chaque mot du
	 * texte nous regardons si c'est un mot<br />
	 * déclencheur (isTrigger). De plus nous ne gardons dans le StringBuilder que les phrases contenant un mot
	 * déclencheur (pour que le CRF ne s'entraine que<br />
	 * sur les phrases contenant une citation).<br />
	 * <br />
	 * Nous ajoutons à ce StringBuilder une fenêtre de mots par rapport aux guillemets trouvés dans les phrases. D'abord
	 * nous parcourons tout le texte en mettant<br />
	 * la position de chaque guillemet dans un HashSet quotePositions puis pour chaque w index et 2 * w index suivants
	 * (précédents), nous mettons un tag QM+ (QM- resp.) <br />
	 * à chacun dans un HashMap quoteTags. En parcourant, ensuite, le text quand on contruit le StringBuilder, si une
	 * clé de quoteTags contient l'index en cours<br />
	 * on écrit la valeur de cette clé dans le StringBuilder.<br />
	 * 
	 * Nous gérons une liste de professions ainsi qu'une liste de médias qui permettent d'ajouter 2 colonnes pour aider
	 * le CRF.<br />
	 * 
	 * Finalement le StringBuilder sera de la forme suivante : word PoS lemma isProf isTrigger quoteMark isMedia mix_col
	 * token.beginPosition():token.endPosition() token.beginPosition():token.endPosition()
	 * 
	 * 
	 * @param text
	 *            Le text à tag.
	 * @return Le StringBuilder créé avec toutes les informations nécessaires. (6 champs)
	 */
	public static String parseText(String fileId, String text, Tools tools, Resources resources, Memory memory) {
		String parsedText = memory.parsedTexts.get(fileId);

		if (parsedText != null) {
			return parsedText;
		}
		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		tools.getCoreNLPParser().annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		StringBuilder result = new StringBuilder();

		HashSet<Integer> quotePositions = new HashSet<>();
		HashSet<Integer> finalQuotePositions = new HashSet<>();

		HashMap<Integer, String> quoteTags = new HashMap<>();

		int startIndex = 0;
		int indexToken;

		//Stanford has a problem with annotation of the quotes at the beginning of a sentence. It puts the quote at the end of the previous sentence.
		//What we do is when a quotation mark is at after a newline, we put that quote at the beginning of the next sentence.
		//At first we look for the position of these quotations and we keep their position in a HashSet.
		while ((indexToken = text.indexOf("\n\"", startIndex)) != -1) {
			finalQuotePositions.add(indexToken + 1);
			startIndex = indexToken + 1;
		}

		int window = 5;
		int indexWord = 1;
		CoreLabel keptToken = null;

		ArrayList<Integer> sentenceOffsets = new ArrayList<>();

		for (CoreMap sentence : sentences) {
			int tokenPosition = 0;

			int idLine = 1;

			List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);

			if (keptToken != null) {
				tokens.add(0, keptToken);
				keptToken = null;
			}

			for (CoreLabel token : tokens) {
				// Store start of sentence position
				if (tokenPosition == 0) {
					sentenceOffsets.add(token.beginPosition());
				}

				// this is the text of the token
				String word = token.get(TextAnnotation.class);

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();

				//If the quotation mark is in the HashSet and if it is not the first token of the sentence we switch the position of the quotation.
				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					assert tokenPosition == tokens.size() - 1 : "tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1) + "\n"
							+ "Values must be equal.";
					keptToken = token;
					continue;
				}

				if (word.equals("\"") || word.equals("»") || word.equals("«") || word.equals("“")) {
					quotePositions.add(tokenPosition);
				}

				tokenPosition++;

			}
			// We place a tag QM for each quotation mark found and +/- window for each word located at a +/- distance of the quotation mark
			for (int quotePosition : quotePositions) {
				for (int index = quotePosition - (2 * window); index < quotePosition - window; index++) {
					quoteTags.put(index, "QM-" + 2 * window);
				}

				for (int index = quotePosition + window; index < quotePosition + 2 * window; index++) {
					quoteTags.put(index, "QM+" + 2 * window);
				}
			}

			for (int quotePosition : quotePositions) {
				for (int index = quotePosition - window; index < quotePosition; index++) {
					quoteTags.put(index, "QM-" + window);
				}
				for (int index = quotePosition; index < quotePosition + window; index++) {
					quoteTags.put(index, "QM+" + window);
				}
			}

			for (int quotePosition : quotePositions) {
				for (int index = quotePosition; index <= quotePosition; index++) {
					quoteTags.put(index, "QM");
				}
			}

			StringBuilder sentenceResult = new StringBuilder();

			boolean triggerFound = false;

			List<MatchedExpression> matchedExpressions = tools.triggerExtractor.extractExpressions(sentence);
			HashSet<Integer> triggerPositions = new HashSet<>();

			//we extract the trigger words for primary quotes and we keep their position
			for (MatchedExpression expression : matchedExpressions) {
				for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
					triggerPositions.add(offset);
				}
				triggerFound = true;
			}

			//we extract the media words and we keep their position 
			HashSet<Integer> triggerPositionsMedia = new HashSet<>();
			if (searchSecondary) {
				List<MatchedExpression> matchedExpressionsMedia = tools.mediaExtractor.extractExpressions(sentence);

				for (MatchedExpression expression : matchedExpressionsMedia) {
					for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
						triggerPositionsMedia.add(offset);
					}
					triggerFound = true;
				}
			}
			//we extract the trigger words for secondary quotes and we keep their position
			List<MatchedExpression> matchedExpressionsTriggerSecondary = tools.triggerSecondaryExtractor.extractExpressions(sentence);
			HashSet<Integer> triggerPositionsSecondary = new HashSet<>();

			for (MatchedExpression expression : matchedExpressionsTriggerSecondary) {
				for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
					triggerPositionsSecondary.add(offset);
				}
				triggerFound = true;
			}

			tokenPosition = 0;
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token : tokens) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// replace for example the phone numbers 06 xx xx xx xx with 06_xx_xx_xx_xx in the text
				word = word.replaceAll(" ", "_");

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();
				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					assert tokenPosition == tokens.size() - 1 : "tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1)
							+ ", Ces valeurs doivent etre égales.";
					keptToken = token;
					continue;
				}

				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				// this is the lemma of the token
				String lemma = tools.lemmatizer.getLemma(word, pos);

				// some corrections to help the CRF
				if (pos.equals("N")) {
					pos = "NC";
				}
				if (resources.PERSON_ABBR.contains(word)) {
					pos = "NPP";
				}
				if (resources.TEMPORAL_EXPRESSIONS.contains(word.toLowerCase())) {
					pos = "NC-TEMP";
				}
				if (word.equals("dit")) {
					lemma = "dire";
				}
				if (word.equals("péri")) {
					pos = "VPP";
					lemma = "périr";
				}

				//				if (word.equals("Emmanuelle") && pos.equals("NC"))
				//					pos = "NPP";
				//
				//				if (word.equals("Manuel") && pos.equals("NC"))
				//					pos = "NPP";
				//
				//				if (word.equals("Valls") && pos.equals("NC"))
				//					pos = "NPP";
				//
				//				if (word.equals("Sénat"))
				//					pos = "NC";
				//
				//				if (word.equals("cheikh")) {
				//					pos = "NC";
				//				}
				//				if (lemma.equals("ben")) {
				//					pos = "NPP";
				//				}
				//				if (lemma.equals("bent")) {
				//					pos = "NPP";
				//				}

				String isTrigger = "NO";
				String isMedia = "NO_MEDIA";
				String mix_col = lemma;

				if (triggerPositions.contains(token.beginPosition())) {
					isTrigger = "TRIGGER";
				}

				if (triggerPositionsMedia.contains(token.beginPosition())) {
					if (!isGeographic(word, resources))
						isMedia = "IS_MEDIA";
				}

				// mix_col contains : 
				// either SOURCE_SEC_TRIGGER if the word is in the trigger words for secondary quotes
				// or either the pos tagging of the word if it is a "N" pos and not a trigger word for secondary quotes
				// otherwise it is the lemma
				if (triggerPositionsSecondary.contains(token.beginPosition())) {
					mix_col = "SOURCE_SEC_TRIGGER";
				} else if (resources.COARSE_GRAINED_POS_MAP.get(pos).equals("N")) {
					mix_col = pos;
				}

				String quoteMark = "NQM";

				if (quoteTags.containsKey(tokenPosition)) {

					quoteMark = quoteTags.get(tokenPosition);

					if (quoteMark.equals("QM"))
						triggerFound = true;
				}

				if (resources.CITATION_VERBS.contains(lemma)) {
					isTrigger = "TRIGGER";
					triggerFound = true;
				}

				String isProf = "ISNOTPROF";
				if (resources.PROFESSION_WORDS.contains(word)) {
					isProf = "PROF";
				}
				// Two token.beginPosition() + ":" + token.endPosition() to make easier the (B)IO conversion
				// Two sentenceResult result to make easier the coreferences.
				//				if (!onlyOffset) {
				sentenceResult.append(
						indexWord + "-" + idLine + "\t" + word + "\t" + pos + "\t" + lemma + "\t" + isTrigger + "\t" + isProf + "\t" + quoteMark + "\t" + isMedia + "\t"
								+ mix_col + "\t" + token.beginPosition() + ":" + token.endPosition() + "\t" + token.beginPosition() + ":" + token.endPosition() + "\n");
				//				}
				//				else {
				//					sentenceResult.append(word + "\t" + pos + "\t" + lemma + "\t" + isProf + "\t"
				//							+ token.beginPosition() + ":" + token.endPosition() + "\n");
				//				}

				idLine++;
				tokenPosition++;
			}
			sentenceResult.append("\n");

			if (triggerFound) {
				result.append(sentenceResult);
			}

			quotePositions.clear();
			quoteTags.clear();
			indexWord++;
		}
		String resultStr = result.toString();
		//		if (!onlyOffset)
		memory.parsedTexts.put(fileId, resultStr);

		memory.sentenceOffsetsByFile.put(fileId, sentenceOffsets);

		return resultStr;
	}

	public static String parseTextForMaltParser(String text, File inFile, File outFileMP, Tools tools, Resources resources) throws IOException {
		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		tools.coreNLPParser.annotate(document);
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		StringBuilder builder = new StringBuilder();
		StringBuilder result = new StringBuilder();

		int startIndex = 0;
		int indexToken;
		HashSet<Integer> finalQuotePositions = new HashSet<>();

		while ((indexToken = text.indexOf("\n\"", startIndex)) != -1) {
			finalQuotePositions.add(indexToken + 1);
			startIndex = indexToken + 1;
		}

		int idLine = 1;
		CoreLabel keptToken = null;

		for (CoreMap sentence : sentences) {
			int tokenPosition = 0;

			int index = 1;
			List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);

			if (keptToken != null) {
				tokens.add(0, keptToken);
				keptToken = null;
			}

			String malt = "";

			for (CoreLabel token : tokens) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				word = word.replaceAll(" ", "_");

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();

				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					if (tokenPosition != tokens.size() - 1) {
						System.out.println("tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1));
						System.out.println("Ces valeurs doivent etre égales.");
					}
					keptToken = token;
					continue;
				}

				String pos = token.get(PartOfSpeechAnnotation.class);
				String lemma = tools.lemmatizer.getLemma(word, pos);

				boolean isCL = pos.equals("CL");
				String supplementary = "_";
				if (pos.equals("PUNC"))
					pos = "PONCT";

				if (pos.equals("N")) {
					pos = "NC";
					supplementary = "NC";
				}

				if (resources.PERSON_ABBR.contains(word)) {
					pos = "NPP";
				}

				// some corrections to help malt parser and the CRF.
				if (pos.equals("C"))
					pos = "CS";

				if (word.equals("y") && isCL)
					pos = "CLO";

				if (word.equals("il") && isCL)
					pos = "CLS";

				if (word.equals("on") && isCL)
					pos = "CLS";

				if (word.equals("dit")) {
					lemma = "dire";
				}
				if (word.equals("péri")) {
					pos = "VPP";
					lemma = "périr";
				}
				//				if (word.equals("cheikh")) {
				//					pos = "NC";
				//				}
				//
				//				if (lemma.equals("ben")) {
				//					pos = "NPP";
				//				}

				//				if (word.equals("Sénat"))
				//					pos = "NC";
				//
				//				if (word.equals("Emmanuelle") && pos.equals("NC"))
				//					pos = "NPP";
				//
				//				if (word.equals("Manuel") && pos.equals("NC"))
				//					pos = "NPP";
				//
				//				if (word.equals("Valls") && pos.equals("NC"))
				//					pos = "NPP";

				String basicCategory = resources.COARSE_GRAINED_POS_MAP.get(pos);

				builder.append(idLine + "-" + index + "\t" + word + "\t" + lemma + "\t" + basicCategory + "\t" + pos + "\t" + supplementary + "\t\n");
				index++;
				tokenPosition++;
			}
			// we run malt parser on each sentence
			malt = runMaltParser(builder.toString(), inFile, outFileMP, tools);

			result.append(malt);
			builder.setLength(0);
			idLine++;
		}

		return result.toString();
	}

	/**
	 * Méthode qui permet de transformer un String en un fichier (B)IO.<br />
	 * <br />
	 * Pour cela nous récupérons le String tagAndOffset qui a été tag par la fonction parseText et on le split par
	 * rapport aux espaces trouvés.<br />
	 * exemple : marche NC marche NO NQM O --------> [marche, NC, marche, NO, NQM, O].<br />
	 * <br />
	 * Nous splittons de la même façon le fichier annoté ann qui provient du dossier input et qui porte le même nom que
	 * le fichier dont le tagAndOffset est issu.<br />
	 * Après avoir split, nous regardons pour chaque ligne si l'annotation est une SOURCE-PRIM ou SOURCE-SEC, si oui on
	 * sauvegarde l'offset de début dans le HashSet startOffsets et<br />
	 * les offsets suivant jusqu'à l'offset de fin de la SOURCE-PRIM ou SOURCE-SEC dans un HashSet inOffsets.<br />
	 * <br />
	 * Nous parcourons ensuite le String taggedText ligne par ligne et pour chaque offset on vérifie s'il est contenu
	 * dans startOffsets (si oui on inscrit B à la place de l'offset), ou s'il est contenu dans inOffsets (si oui on
	 * inscrit I à la place de l'offset) ou s'il n'est ni dans l'un ni dans l'autre (si oui on inscrit O à la place de
	 * l'offset). Si aucune source n'est présente dans le fichier annoté (ou si justement pas encore annoté), on met
	 * directement O à la place de tous les offsets.<br />
	 * <br />
	 * Finalement on écrit le résultat dans le fichier outFile qui porte le même nom que le fichier d'entrée mais avec
	 * l'extension .tag.<br />
	 * <br />
	 * 
	 * @param taggedText
	 *            String qui a été tag par la fonction tagFromMaltParserAndParseText.
	 * @param ann
	 *            Contenu d'un fichier annoté qui provient du dossier input portant le même nom que le fichier d'entrée.
	 * @param outFile
	 *            Fichier où on écrit le résultat de la conversion.
	 * @return le résultat de la conversion.
	 * @throws IOException
	 */
	public static String bioConversion(String taggedText, String ann, File outFile) throws IOException {
		String name = outFile.getName();
		int pos = name.lastIndexOf(".");
		if (pos > 0) {
			name = name.substring(0, pos);
		}

		List<String[]> listOfTextTag = new LinkedList<String[]>();
		String[] wordsTag = taggedText.split("\\r?\\n");

		for (int i = 0; i < wordsTag.length; i++) {
			listOfTextTag.add(wordsTag[i].split("\\s+"));
		}

		List<String[]> listOfBratAnnotation = new LinkedList<String[]>();
		String[] wordsAnn = ann.split("\\r?\\n");

		for (int i = 0; i < wordsAnn.length; i++) {
			listOfBratAnnotation.add(wordsAnn[i].split("\\s+"));
		}

		HashSet<Integer> startOffsetsPrim = new HashSet<>();
		HashSet<Integer> inOffsetsPrim = new HashSet<>();
		HashSet<Integer> startOffsetsSec = new HashSet<>();
		HashSet<Integer> inOffsetsSec = new HashSet<>();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = new HashMap<>();

		fillMapsWithOffsets(listOfBratAnnotation, name, startOffsetsPrim, inOffsetsPrim, startOffsetsSec, inOffsetsSec, sourceAndOffset);

		writeBIO(startOffsetsPrim, inOffsetsPrim, listOfTextTag, PRIM, name);

		writeBIO(startOffsetsSec, inOffsetsSec, listOfTextTag, SEC, name);

		StringBuilder builder = new StringBuilder();

		for (String[] w : listOfTextTag)
			builder.append((String.join(" ", w) + "\n"));

		IOUtils.write(builder.toString(), outFile);

		return builder.toString();
	}

	/**
	 * Méthode permettant de remplir 5 HashSets selon la source lue dans le fichier ann.<br>
	 * On note l'offset de début de chaque source primaire et secondaire dans leur HashSet respectif ainsi que tous les
	 * offsets jusqu'à la fin de la source.<br>
	 * 
	 * Pour l'annotation des coreferences, nous enregistrons aussi tous les offsets de chaque source par rapport à leur
	 * id.<br>
	 * sourceAndOffset sera de la forme : {T1 = [547,548,549,550,551,552,...]}<br>
	 * 
	 * @param listOfBratAnnotation
	 * @param name
	 */
	private static void fillMapsWithOffsets(List<String[]> listOfBratAnnotation, String name, HashSet<Integer> startOffsetsPrim, HashSet<Integer> inOffsetsPrim,
			HashSet<Integer> startOffsetsSec, HashSet<Integer> inOffsetsSec, HashMap<String, ArrayList<Integer>> sourceAndOffset) {

		//		startOffsetsPrim = new HashSet<>();
		//		inOffsetsPrim = new HashSet<>();
		//		startOffsetsSec = new HashSet<>();
		//		inOffsetsSec = new HashSet<>();
		//		sourceAndOffset = new HashMap<>();

		for (String[] bratAnnotation : listOfBratAnnotation) {
			ArrayList<Integer> offsets = new ArrayList<>();
			if (bratAnnotation.length != 1) {
				String entity = bratAnnotation[1];
				String id = bratAnnotation[0];

				if (bratAnnotation.length <= 1) {
					System.out.println("Fichier d'annotation corrompu : " + name + ".ann.\nVoir cette ligne : \t" + Arrays.toString(bratAnnotation));
					System.exit(1);
				}

				int leftOffset = 0;
				int rightOffset = 0;

				switch (entity) {

				case SOURCEPRIM:
					leftOffset = Integer.parseInt(bratAnnotation[2]);
					rightOffset = Integer.parseInt(bratAnnotation[3]);
					startOffsetsPrim.add(leftOffset);

					offsets.add(leftOffset);
					sourceAndOffset.put(id, offsets);

					for (int offset = leftOffset; offset < rightOffset; offset++) {
						inOffsetsPrim.add(offset);
						sourceAndOffset.get(id).add(offset);
					}

					break;

				case SOURCESEC:
					leftOffset = Integer.parseInt(bratAnnotation[2]);
					rightOffset = Integer.parseInt(bratAnnotation[3]);
					offsets.add(leftOffset);
					sourceAndOffset.put(id, offsets);

					startOffsetsSec.add(leftOffset);
					for (int offset = leftOffset; offset < rightOffset; offset++) {
						inOffsetsSec.add(offset);
						sourceAndOffset.get(id).add(offset);
					}

					break;

				default:
					break;
				}

			}
		}
	}

	/**
	 * Méthode permettant de remplacer les offsets de fin de chaque ligne de tag par un encoding (B)IO.
	 * 
	 * @param startOffsets
	 *            HashSet qui donne le début de chaque source
	 * @param inOffsets
	 *            HashSet qui donne l'intérieur de chaque source
	 * @param listOfTextTag
	 *            La liste de tag d'un fichier
	 * @param suffix
	 *            PRIM ou SEC selon si on veut convertir la source PRIM ou SEC
	 * @param name
	 *            Nom du fichier en cours
	 */
	public static void writeBIO(HashSet<Integer> startOffsets, HashSet<Integer> inOffsets, List<String[]> listOfTextTag, String suffix, String name) {

		// if there is a source to transform we look for the beginning of the source, otherwise we know that we can put "O" at the end of each line.
		if (startOffsets.size() > 0) {
			for (String[] fieldsTag : listOfTextTag) {

				if (fieldsTag.length != numberFieldsSec + 1 && fieldsTag.length != 1) {
					System.out.println("Les fichiers avec offsets doivent avoir 11 champs, " + name + ".txt est corrompu\n" + "Voir cette ligne : \t"
							+ Arrays.toString(fieldsTag) + "et " + fieldsTag.length);
					System.exit(1);
				}

				try {
					// If it is not a new line
					if (fieldsTag.length != 1) {
						String[] offsetsStr = null;
						// If PRIM : fieldsTag[numberFieldsPrim] contains the offset of the start and of the end of the current word separated by ":"
						// If SEC : same thing but with the next fieldsTag.
						if (suffix.equals(PRIM)) {
							offsetsStr = fieldsTag[numberFieldsPrim].split(":");
						} else if (suffix.equals(SEC)) {
							offsetsStr = fieldsTag[numberFieldsSec].split(":");
						}

						int offsetLeft = Integer.parseInt(offsetsStr[0]);
						int offsetRight = Integer.parseInt(offsetsStr[1]);
						String className = null;

						// we look at each offset between the start and the end of each word
						// and if the start of a source is equal to the current offset then we put className to B or I (according to the encoding wanted)
						for (int offset = offsetLeft; offset < offsetRight; offset++) {
							if (startOffsets.contains(offset)) {
								if (modelBio)
									className = encodingB + suffix;
								else
									className = encodingI + suffix;
								break;
							}
						}
						// Now we have already marked all starts of sources, we then need to mark the offsets included in sources.
						if (className == null) {
							// we look at each offset between the start and the end of each word that are not already marked
							for (int offset = offsetLeft; offset < offsetRight; offset++) {
								if (inOffsets.contains(offset)) {
									className = encodingI + suffix;
									break;
								}
							}
						}
						// Here we are sure that all sources have already been marked so we can transform everything else into "O"
						if (className == null) {
							className = OUT;
						}
						// We replace the values of the offsets with the className found.
						if (suffix.equals(PRIM))
							fieldsTag[numberFieldsPrim] = className;
						else if (suffix.equals(SEC))
							fieldsTag[numberFieldsSec] = className;
					}
				} catch (NumberFormatException e) {
					throw e;
				}
			}
		} else {
			for (String[] fieldsTag : listOfTextTag) {
				if (fieldsTag.length != 1) {
					if (suffix.equals(PRIM))
						fieldsTag[numberFieldsPrim] = OUT;
					else if (suffix.equals(SEC))
						fieldsTag[numberFieldsSec] = OUT;
				}
			}
		}
	}

	/**
	 * Méthode qui permet de convertir du (B)IO vers BRAT. (seulement les SOURCE).<br />
	 * <br />
	 * Nous enregistrons les offsets de tagAndOffset que nous utilisons dans l'affichage.<br />
	 * <br />
	 * Nous parcourons ligne par ligne le String annAuto et nous regardons chaque dernier champ. Si le dernier champ est
	 * un B alors Wapiti a prédit qu'une source commençait à ce mot. Nous enregistrons l'offset et nous regardons
	 * combien de I se trouvent après ce B. Nous affichons ensuite le résultat en écrivant la source complète issue du
	 * texte original grâce aux offsets sauvegardés. <br />
	 * 
	 * 
	 * @param tagAndOffset
	 *            parseText du fichier .txt original pour avoir les offsets des mots.
	 * @param resultWapiti
	 *            Le fichier de sortie de Wapiti après la phase de test (nous prenons la prédiction de Wapiti).
	 * @param fileId
	 *            Le nom du fichier en entrée pour pouvoir nommer le fichier en sortie.
	 * @param originalText
	 *            Le texte original pour pouvoir afficher la source grâce à un substring.
	 * @param dirAuto
	 *            Dossier où enregistrer le résultat de la conversion.
	 * @throws IOException
	 */
	public static void annConversion(String tagAndOffset, String resultWapiti, String fileId, String originalText, File dirAuto, String sourceExt) throws IOException {
		boolean isPrim = false;

		if (sourceExt.equals(PRIM))
			isPrim = true;

		boolean isSec = false;

		if (sourceExt.equals(SEC))
			isSec = true;

		// Align wapiti output with lines containing word offsets 
		List<String[]> listOfTextTag = new LinkedList<String[]>();
		List<String[]> listOfFieldsResultWapiti = new LinkedList<String[]>();

		if (resultWapiti.trim().length() > 0) {
			String[] linesWordsTag = tagAndOffset.split("\\r?\\n");
			String[] linesResultWapiti = resultWapiti.split("\\r?\\n");

			List<String[]> listOfSentenceTextTag = new LinkedList<String[]>();
			List<String[]> listOfSentenceResultWapiti = new LinkedList<String[]>();

			int linesWordsIndex = 0;
			boolean skipSentence = false;
			for (String lineWords : linesWordsTag) {
				if (lineWords.length() > 0) {
					if (!skipSentence) {
						if (linesWordsIndex > linesResultWapiti.length - 1) {
							skipSentence = true;
							break;
						}
						String lineResultWapiti = linesResultWapiti[linesWordsIndex];
						assert lineResultWapiti.length() > 0 : "Line " + linesWordsIndex + " should not be empty";
						String[] fieldsResultWapiti = lineResultWapiti.split("\\s+");
						String[] fieldsLineWords = lineWords.split("\\s+");
						// Must be same word, otherwise skip the sentence
						if (fieldsResultWapiti[0].equals(fieldsLineWords[1])) {
							listOfSentenceTextTag.add(fieldsLineWords);
							listOfSentenceResultWapiti.add(fieldsResultWapiti);
							linesWordsIndex++;
						} else {
							skipSentence = true;
						}
					}

				}
				// Empty line (EOS)
				else {
					if (!skipSentence) {
						listOfFieldsResultWapiti.addAll(listOfSentenceResultWapiti);
						listOfTextTag.addAll(listOfSentenceTextTag);
						listOfFieldsResultWapiti.add(new String[0]);
						listOfTextTag.add(new String[0]);
						++linesWordsIndex;
					} else {
						skipSentence = false;
					}
					listOfSentenceResultWapiti.clear();
					listOfSentenceTextTag.clear();
				}
			}

			// Last sentence
			if (!skipSentence) {
				listOfFieldsResultWapiti.addAll(listOfSentenceResultWapiti);
				listOfTextTag.addAll(listOfSentenceTextTag);
			}
		}

		assert listOfFieldsResultWapiti.size() == listOfTextTag.size() : "Wapiti output files and offset files must have the same length " + ".\nHere "
				+ listOfFieldsResultWapiti.size() + " != " + listOfTextTag.size();

		// if it is a new conversion, we must delete previous files in the directory
		if (isPrim) {
			DIRUtils.deleteFilesInDirIfExist(dirAuto, fileId, ".ann");
		}

		File bratOutputFile = new File(dirAuto, fileId + ".ann");

		int sourceNumberBrat = 0;
		for (int index = 0; index < listOfTextTag.size(); index++) {
			String[] fieldsResultWapiti = listOfFieldsResultWapiti.get(index);
			assert listOfTextTag.get(index).length == numberFieldsSec || listOfTextTag.get(index).length == 0 : "Offset files must have " + numberFieldsSec + " fields\n"
					+ " but we have: \t" + Arrays.toString(listOfTextTag.get(index)) + " (size " + listOfTextTag.get(index).length + ")";

			assert !(fieldsResultWapiti.length != numberFieldsPrim + 1 && fieldsResultWapiti.length != numberFieldsSec + 1
					&& listOfTextTag.get(index).length != 0) : "Wapiti output must have " + (numberFieldsPrim + 1) + " (PRIM) or " + (numberFieldsSec + 1)
							+ " (SEC) fields\n" + " but we have: \t" + Arrays.toString(listOfFieldsResultWapiti.get(index));

			// !=1 because we separate each sentence by a new line
			if (listOfTextTag.get(index).length != 0) {
				String classNameAuto = "";
				if (isPrim) {
					// wapiti annotes on the last column, we get this.
					classNameAuto = fieldsResultWapiti[fieldsResultWapiti.length - 1];
					assert classNameAuto.endsWith(PRIM) || classNameAuto.equals(OUT) || classNameAuto.endsWith(NULL) : classNameAuto;
				} else if (isSec) {
					// wapiti annotes on the last column, we get this.
					classNameAuto = fieldsResultWapiti[fieldsResultWapiti.length - 1];
					assert classNameAuto.endsWith(SEC) || classNameAuto.equals(OUT) || classNameAuto.endsWith(NULL) : classNameAuto;
				}

				// if we meet a B or a I we are sure that it is the first word of the source.
				if (classNameAuto.startsWith(encodingI) || classNameAuto.startsWith(encodingB)) {
					sourceNumberBrat++;
					writeToFiles(index, listOfFieldsResultWapiti, listOfTextTag, bratOutputFile, sourceNumberBrat, originalText, sourceExt);
				}

			}
		}
		// If no source at all, create an empty .ann file
		Files.append("", bratOutputFile, Charsets.UTF_8);
	}

	/**
	 * Méthode qui permet d'écrire les sources trouvées par wapiti.
	 * 
	 * @param index
	 *            L'index du début de source
	 * @param listOfFieldsResultWapiti
	 *            La liste des phrases avec comme dernière colonne la prédiction de wapiti
	 * @param listOfTextTag
	 *            La liste des phrases avec les offsets aux dernières colonnes
	 * @param outputBRAT
	 *            StringBuilder pour écrire le résultat
	 * @param sourceNumberBrat
	 *            Numéro de la source à afficher
	 * @param originalText
	 *            Le string du text original pour pouvoir afficher correctement la source
	 * @param sourceExt
	 *            L'extension de la source à écrire (-PRIM ou -SEC)
	 * @throws IOException
	 */
	public static void writeToFiles(int index, List<String[]> listOfFieldsResultWapiti, List<String[]> listOfTextTag, File outputBRAT, int sourceNumberBrat,
			String originalText, String sourceExt) throws IOException {

		String[] offsetsStr = listOfTextTag.get(index)[numberFieldsPrim].split(":");
		int offsetLeft = Integer.parseInt(offsetsStr[0]);
		int offsetRight = Integer.parseInt(offsetsStr[1]);

		String word = listOfFieldsResultWapiti.get(index)[0];

		int gapForEndOffset = 1;
		switch (sourceExt) {
		case PRIM:
			String[] fieldsResultWapiti = listOfFieldsResultWapiti.get(index + gapForEndOffset);
			// if this annotation is not the last one of the sentence
			if (fieldsResultWapiti.length != 1) {
				// if this annotation does not exceed the listOfFieldsResultWapiti size and is equal to a source
				assert fieldsResultWapiti[numberFieldsPrim].endsWith(sourceExt) || fieldsResultWapiti[numberFieldsPrim].equals(OUT);
				if (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapiti[numberFieldsPrim].equals(encodingI + sourceExt)) {

					while (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapiti.length > 1
							&& fieldsResultWapiti[numberFieldsPrim].equals(encodingI + sourceExt)) {

						// we replace all I-sourceExt found by null otherwise each I-sourceExt would be the start a one source.
						// we could have Le président Francois Hollande, président Francois Hollande, Francois Hollande, Hollande (4 sources) if we 
						// did not do that change
						listOfFieldsResultWapiti.get(index + gapForEndOffset)[numberFieldsPrim] = NULL;

						gapForEndOffset++;
						fieldsResultWapiti = listOfFieldsResultWapiti.get(index + gapForEndOffset);
					}

					// we take the right offset of the source
					String[] offsetsStrI = listOfTextTag.get(index + gapForEndOffset - 1)[numberFieldsPrim - 1].split(":");
					int offsetRightI = Integer.parseInt(offsetsStrI[1]);
					String sourceText = originalText.substring(offsetLeft, offsetRightI);

					if (sourceText.contains("\"")) {
						sourceText = sourceText.replace("\"", "\\\"");
					}

					Files.append("T" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + offsetLeft + " " + offsetRightI + "\t"
							+ originalText.substring(offsetLeft, offsetRightI) + "\t\n", outputBRAT, Charsets.UTF_8);
				} else {
					// If it is a pronoun which begins by a "-" like "-t-il" we write only "il" starting from the offset at last index of "-"
					if (word.startsWith("-")) {
						int newStartOffset = word.lastIndexOf("-") + 1;
						word = word.substring(newStartOffset);
						Files.append("T" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + (offsetLeft + newStartOffset) + " " + offsetRight + "\t" + word + "\t\n",
								outputBRAT, Charsets.UTF_8);
					}
					// Otherwise there is only one word in the source and we write it
					else {
						String sourceText = originalText.substring(offsetLeft, offsetRight);
						if (sourceText.contains("\""))
							sourceText.replace("\"", "\\\"");

						Files.append("T" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + offsetLeft + " " + offsetRight + "\t" + sourceText + "\t\n", outputBRAT,
								Charsets.UTF_8);
					}
				}
			}
			break;
		//same as case PRIM	
		case SEC:
			String[] fieldsResultWapitiSec = listOfFieldsResultWapiti.get(index + gapForEndOffset);
			if (listOfFieldsResultWapiti.get(index + gapForEndOffset).length != 1) {
				assert fieldsResultWapitiSec[numberFieldsSec].endsWith(sourceExt) || fieldsResultWapitiSec[numberFieldsSec].equals(OUT);
				if (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapitiSec[numberFieldsSec].equals(encodingI + sourceExt)
						&& listOfFieldsResultWapiti.get(index + gapForEndOffset).length != 1) {
					while (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapitiSec.length != 1
							&& fieldsResultWapitiSec[numberFieldsSec].equals(encodingI + sourceExt)) {

						listOfFieldsResultWapiti.get(index + gapForEndOffset)[numberFieldsSec] = NULL;
						gapForEndOffset++;
						fieldsResultWapitiSec = listOfFieldsResultWapiti.get(index + gapForEndOffset);
					}

					String[] offsetsStrI = listOfTextTag.get(index + gapForEndOffset - 1)[numberFieldsSec - 2].split(":");

					int offsetRightI = Integer.parseInt(offsetsStrI[1]);

					String sourceText = originalText.substring(offsetLeft, offsetRightI);

					if (sourceText.contains("\"")) {
						sourceText = sourceText.replace("\"", "\\\"");
					}

					Files.append("T" + "100" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + offsetLeft + " " + offsetRightI + "\t"
							+ originalText.substring(offsetLeft, offsetRightI) + "\t\n", outputBRAT, Charsets.UTF_8);
				}

				else {
					if (word.startsWith("-")) {
						int newStartOffset = word.lastIndexOf("-") + 1;
						word = word.substring(newStartOffset);
						Files.append("T" + "10" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + (offsetLeft + newStartOffset) + " " + offsetRight + "\t" + word
								+ "\t\n", outputBRAT, Charsets.UTF_8);
					} else
						Files.append("T" + "100" + sourceNumberBrat + "\t" + SOURCE + sourceExt + " " + offsetLeft + " " + offsetRight + "\t"
								+ originalText.substring(offsetLeft, offsetRight) + "\t\n", outputBRAT, Charsets.UTF_8);
				}
			}
			break;

		default:
			System.out.println(sourceExt + " = PRIM ou SEC");
			System.exit(1);
			break;
		}

	}

	/**
	 * Méthode qui permet de transformer un fichier BRAT avec SOURCE vers un fichier BRAT avec SOURCE-PRIM et SOURCE-SEC
	 * 
	 * @throws IOException
	 */
	private void transformOriginalBratToBratSourcePrimAndSec() throws IOException {

		for (File inFile : this.paths.DIR_INPUT_LABELED.listFiles()) {
			HashMap<String, String> id2Line = new HashMap<>();
			HashMap<String, String> id2Type = new HashMap<>();
			HashSet<String> sources = new HashSet<>();
			HashSet<String> secondarySources = new HashSet<>();

			String name = inFile.getName();

			if (name.endsWith(".ann")) {
				String ann = Files.toString(inFile, Charsets.UTF_8);
				String[] wordsAnn = ann.split("\\r?\\n");

				for (int i = 0; i < wordsAnn.length; i++) {
					Matcher matcher = BRAT_ENTITY_PATTERN.matcher(wordsAnn[i]);
					if (matcher.find()) {
						String id = matcher.group(1);
						String type = matcher.group(2);
						String restOfLine = matcher.group(3);
						id2Line.put(id, restOfLine);
						id2Type.put(id, type);
						if (type.equals(SOURCE)) {
							sources.add(id);
						}
						if (type.equals(SECONDARY)) {
							secondarySources.add(restOfLine.trim());
						}
					}
				}

				FileWriter fw = new FileWriter(this.paths.DIR_INPUT_LABELED_CONVERTED + "/" + inFile.getName(), false);

				BufferedWriter output = new BufferedWriter(fw);

				for (String sourceId : sources) {
					if (secondarySources.contains(sourceId)) {
						output.append(sourceId + "\t" + SOURCESEC + " " + id2Line.get(sourceId) + "\n");
					} else {
						output.append(sourceId + "\t" + SOURCEPRIM + " " + id2Line.get(sourceId) + "\n");
					}

				}
				output.close();
			} else if (name.endsWith(".txt")) {
				continue;
			} else {
				System.out.println("seulement dossier avec des fichiers .ann ou .txt");
				System.exit(1);
			}
		}
	}

	/**
	 * @param text
	 *            the normalized SOURCE, as labeled by Wapiti and resolved by coreference module
	 * @return the cleaned text to be indexed by later processes (search engines, etc...)
	 */
	private static String text2Index(String text) {
		String newText = text;
		Matcher m1 = PN_COMMA_PATTERN.matcher(newText);

		if (m1.find()) {
			newText = m1.group(1);
		}

		Matcher m2 = COMMA_NP_AND_MORE_PATTERN.matcher(newText);
		if (m2.find()) {
			newText = m2.group(1);
		}
		return newText;
	}

	public static void main(String[] args) throws IOException, MaltChainedException, InterruptedException {
		long startTime = System.currentTimeMillis();
		// Debug mode
		if (args.length == 0) {
			//			String argsStr = "-u -b -c /home/xtannier/Recherche/SourceExtractor/config.properties";
			String argsStr = "-j 2 -o /tmp/test/ -d /home/xtannier/tmp/temp/seul/ -c /home/xtannier/Recherche/SourceExtractor/config.properties";
			args = argsStr.split(" ");
		}

		// Default parameter values
		String configFileName = DEFAULT_CONFIG_FILE;

		CLIParameters cli = new CLIParameters(args);

		//		Property property = new Property();

		CommandLine cmd = cli.parse();
		//		if (DATA_DIR == null) {
		//			dataDirConfig();
		//
		//		}
		if (cmd.hasOption(CLIParameters.OPTION_CONFIG)) {
			configFileName = cmd.getOptionValue(CLIParameters.OPTION_CONFIG);
		}

		Properties properties = new Properties();
		System.err.println("Loading file " + configFileName);
		properties.load(new FileInputStream(new File(configFileName)));

		DATA_DIR = new File(properties.getProperty(DATA_DIR_PROPERTY));
		DIR_LIB = new File(properties.getProperty(LIB_DIR_PROPERTY));
		DIR_RESOURCES = new File(properties.getProperty(RESOURCES_DIR_PROPERTY));

		String lemmatizerResourceDir = DIR_RESOURCES + "/fr/AhmetAkerLemmatizer_ressources/";
		String gazetteersDir = DIR_RESOURCES + "/fr/gazetteers/";
		File dirModelMaltParser = new File(new File(DIR_LIB, "maltparser"), "model");

		SourceExtractor sourceExtractor = new SourceExtractor();

		sourceExtractor.paths.DATA_LABELED_DIR = new File(DATA_DIR, "labeled");
		sourceExtractor.paths.DATA_UNLABELED_DIR = new File(DATA_DIR, "unlabeled");

		sourceExtractor.paths.DIR_INPUT_LABELED = new File(sourceExtractor.paths.DATA_LABELED_DIR, "input_labeled");
		sourceExtractor.paths.DIR_INPUT_LABELED_CONVERTED = new File(sourceExtractor.paths.DATA_LABELED_DIR, "input_labeled_converted");

		sourceExtractor.paths.DIR_INPUT_LABELED_MP = new File(sourceExtractor.paths.DATA_LABELED_DIR, "input_labeled_mp");
		sourceExtractor.paths.DIR_OUTPUT_LABELED = new File(sourceExtractor.paths.DATA_LABELED_DIR, "output_labeled");

		sourceExtractor.paths.DIR_INPUT_UNLABELED = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "input_unlabeled");
		sourceExtractor.paths.DIR_OUTPUT_UNLABELED = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "output_unlabeled");
		sourceExtractor.paths.DIR_INPUT_UNLABELED_MP = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "input_labeled_mp");

		sourceExtractor.paths.DIR_TRAIN_FILES_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "train_prim");
		sourceExtractor.paths.DIR_TEST_FILES_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "test_prim");
		sourceExtractor.paths.DIR_DEV_FILES_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "dev_prim");

		sourceExtractor.paths.DIR_TRAIN_FILES_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "train_sec");
		sourceExtractor.paths.DIR_TEST_FILES_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "test_sec");
		sourceExtractor.paths.DIR_DEV_FILES_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_LABELED, "dev_sec");

		sourceExtractor.paths.DIR_TRAIN_FILES_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "train_prim");
		sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "test_prim");
		sourceExtractor.paths.DIR_DEV_FILES_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "dev_prim");

		sourceExtractor.paths.DIR_TRAIN_FILES_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "train_sec");
		sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "test_sec");
		sourceExtractor.paths.DIR_DEV_FILES_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "dev_sec");

		sourceExtractor.paths.DIR_RESULT_FILES_LABELED = new File(sourceExtractor.paths.DATA_LABELED_DIR, "result_labeled");
		sourceExtractor.paths.DIR_MERGED_FILES_LABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "merged_files");

		sourceExtractor.paths.TRAIN_PRIM_MERGED_LABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_LABELED, "train_prim.txt");
		sourceExtractor.paths.DEV_PRIM_MERGED_LABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_LABELED, "dev_prim.txt");

		sourceExtractor.paths.TRAIN_SEC_MERGED_LABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_LABELED, "train_sec.txt");
		sourceExtractor.paths.DEV_SEC_MERGED_LABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_LABELED, "dev_sec.txt");

		sourceExtractor.paths.DIR_MODELS_LABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "models");

		sourceExtractor.paths.PATTERN_PRIM = new File(new File(DATA_DIR, "wapiti_patterns"), "pattern_prim.txt");
		sourceExtractor.paths.PATTERN_SEC = new File(new File(DATA_DIR, "wapiti_patterns"), "pattern_sec.txt");

		sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "bioWapiti_prim");
		sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "bioWapiti_sec");

		sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "brat_autoWapiti");

		sourceExtractor.paths.DIR_BRAT_REFERENCE_LABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "brat_ref");

		sourceExtractor.paths.DIR_FINAL_RESULT_LABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_LABELED, "brat_ref_ajout_annotator");

		sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "result_unlabeled");

		sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_prim");
		sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_sec");

		sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "merged_files");

		sourceExtractor.paths.TRAIN_PRIM_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "train_prim.txt");
		sourceExtractor.paths.DEV_PRIM_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "dev_prim.txt");

		sourceExtractor.paths.TRAIN_SEC_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "train_sec.txt");
		sourceExtractor.paths.DEV_SEC_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "dev_sec.txt");

		sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "brat_autoWapiti");

		sourceExtractor.paths.DIR_FINAL_RESULT_UNLABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "final");

		sourceExtractor.paths.MODEL_FINAL_DIR = new File(DIR_LIB, "wapiti_models");

		if (cmd.hasOption(CLIParameters.OPTION_BRAT)) {
			outputBrat = true;
		}

		if (cmd.hasOption(CLIParameters.OPTION_IO)) {
			modelBio = false;
		}

		String modelSuffix = "";
		if (cmd.hasOption(CLIParameters.OPTION_PRIM_ONLY)) {
			searchSecondary = false;
			modelSuffix = "_nomedia";
		}

		int jubNumber = 1;
		if (cmd.hasOption(CLIParameters.OPTION_JOB_NUMBER)) {
			jubNumber = Integer.parseInt(cmd.getOptionValue(CLIParameters.OPTION_JOB_NUMBER));
		}

		sourceExtractor.loadTools(lemmatizerResourceDir, gazetteersDir, dirModelMaltParser);

		System.err.print("Loading Wapiti library ");

		// we load the wapiti library compiled for UNIX/POSIX
		if (org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
			System.err.print("for Linux...");
			System.load(DIR_LIB + "/wapiti_java/libwapiti.so");
		} else if (SystemUtils.IS_OS_WINDOWS) {
			// we load the wapiti library compiled for WINDOWS
			System.err.println("for Windows...");
			System.load(DIR_LIB + "/wapiti_java/libwapiti_swig.dll");
			System.load(DIR_LIB + "/wapiti_java/libwapiti.dll");
		} else if (SystemUtils.IS_OS_MAC) {
			System.err.print("for Max...");
			System.load(DIR_LIB + "/wapiti_java/libwapiti.dylib");

		} else {
			System.err.println("Cette version n'est pas prévue sur votre OS");
			System.exit(1);
		}
		System.err.println(" done");

		// if labeled
		if (cmd.hasOption(CLIParameters.OPTION_DATA_TYPE_LABELED)) {
			if (!org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
				System.err.println("Can only train the model on Linux version");
				System.exit(1);
			}
			LABELED_MODE = true;
			sourceExtractor.paths.DIR_INPUT_LABELED_CONVERTED.mkdirs();
			sourceExtractor.paths.DIR_MERGED_FILES_LABELED.mkdirs();
			sourceExtractor.paths.DIR_MODELS_LABELED.mkdirs();
			sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.mkdirs();
			sourceExtractor.paths.DIR_RESULT_FILES_LABELED.mkdirs();
			//			switch (cmd.getOptionValue(CLIParameters.OPTION_DATA_TYPE_LABELED)) {
			//
			//			case encodingIO:
			//
			//				BIO = false;
			//
			//				break;
			//
			//			case encodingBIO: 
			//
			//				BIO = true;
			//
			//				break;
			//			case addAnnotation:
			//				DIRUtils.copyFilesWithSameNameToCompare(DIR_TEST_FILES_PRIM, DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, DIR_FINAL_RESULT_LABELED);
			//				sourceExtractor.parseDirForLabeled(DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED);
			//				throw new RuntimeException();
			////				System.exit(1);
			////				break;
			//			default:
			//				System.out.println("arguments should be : io, bio or ann");
			//				System.exit(1);
			//				break;
			//			}

			File modelPrim;
			File modelSec;
			if (modelBio) {
				modelPrim = new File(sourceExtractor.paths.DIR_MODELS_LABELED, MODEL_PRIM + "_bio" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.DIR_MODELS_LABELED, MODEL_SEC + "_bio" + modelSuffix);
			} else {
				modelPrim = new File(sourceExtractor.paths.DIR_MODELS_LABELED, MODEL_PRIM + "_io" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.DIR_MODELS_LABELED, MODEL_SEC + "_io" + modelSuffix);
			}

			// we convert all input annotation files into SOURCE-PRIM and SOURCE-SEC
			sourceExtractor.transformOriginalBratToBratSourcePrimAndSec();
			System.out.println("Transformation en SOURCE-PRIM et SOURCE-SEC réussie.");

			// we start with tagging each files and launch maltparser on these same files. We eventually end with the (B)IO conversion.
			sourceExtractor.parseDirForLabeled(sourceExtractor.paths.DIR_INPUT_LABELED);
			System.out.println("Tous les fichiers ont été transformés en (B)IO sans problème.");

			// we get all tests files from the input annotation directory
			DIRUtils.copyFilesWithSameNameToCompare(sourceExtractor.paths.DIR_TEST_FILES_PRIM, sourceExtractor.paths.DIR_INPUT_LABELED_CONVERTED,
					sourceExtractor.paths.DIR_BRAT_REFERENCE_LABELED);
			System.out.println("Copie des fichiers d'annotations réussie.");

			// we merge all train, dev and test files to obtain a "big" training corpus for wapiti
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TRAIN_FILES_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TEST_FILES_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_DEV_FILES_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);

			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TRAIN_FILES_SEC, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TEST_FILES_SEC, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_DEV_FILES_SEC, sourceExtractor.paths.DIR_MERGED_FILES_LABELED);
			System.out.println("Copie des fichiers assemblés réussie.\n");

			// wapiti train -d DEV_PRIM_MERGED_LABELED -p PATTERN_PRIM_LABELED TRAIN_PRIM_MERGED_LABELED modelPrim
			System.out.println("\t\tDébut entrainement PRIM\t\t");
			WapitiModel.train(sourceExtractor.paths.PATTERN_PRIM, sourceExtractor.paths.TRAIN_PRIM_MERGED_LABELED, modelPrim,
					"-d " + sourceExtractor.paths.DEV_PRIM_MERGED_LABELED);
			System.out.println("\t\tModèle sauvegardé dans " + modelPrim.getAbsolutePath());
			// wapiti train -d DEV_SEC_MERGED_LABELED -p PATTERN_SEC_LABELED input=TRAIN_PRIM_MERGED_LABELED output=modelSec
			if (searchSecondary) {
				System.out.println("\t\tDébut entrainement SEC\t\t");
				WapitiModel.train(sourceExtractor.paths.PATTERN_SEC, sourceExtractor.paths.TRAIN_SEC_MERGED_LABELED, modelSec,
						"-d " + sourceExtractor.paths.DEV_SEC_MERGED_LABELED);
			}
			System.out.println("\t\tModèle sauvegardé dans " + modelSec.getAbsolutePath());
			System.out.println();
			System.out.println("\t\tDébut label PRIM\t\t");

			// wapiti label -m modelPrim input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM -p
			WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
			wapitiPrim.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_PRIM, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM, jubNumber);

			if (searchSecondary) {
				System.out.println();
				System.out.println("\t\tDébut label SEC\t\t");
				sourceExtractor.deleteSentenceWithoutPrimForSec(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM,
						sourceExtractor.paths.DIR_TEST_FILES_SEC, true);
				// wapiti label -m modelSec input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_Sec -p
				WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
				wapitiSec.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_SEC, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC, jubNumber);
			}

			System.out.println();
			System.out.println("\t\tDébut conversion de la sortie wapiti en Brat\t\t");

			// wapiti has predicted the labels, we now transform the (B)IO files into Brat format.
			sourceExtractor.parseDirForLabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM);
			if (searchSecondary) {
				sourceExtractor.parseDirForLabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC);
			}

			System.out.println();
			System.out.println("\t\tDébut d'ajout d'annotation à la sortie Brat\t\t");

			// we end up adding some annotations to the previous files (the ones that have been converted to brat)
			//			System.out.println("copyFilesWithSameNameToCompare(" + DIR_TEST_FILES_PRIM + ", " + DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED + ", " + DIR_BRAT_AJOUT_ANNOTATOR_LABELED + ")");
			DIRUtils.copyFilesWithSameNameToCompare(sourceExtractor.paths.DIR_TEST_FILES_PRIM, sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED,
					sourceExtractor.paths.DIR_FINAL_RESULT_LABELED);
			sourceExtractor.parseDirForLabeled(sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED);

			System.out.println();
			System.out.println("\t\tScript terminé sans erreur.\t\t");

		} else if (cmd.hasOption(CLIParameters.OPTION_DATA_TYPE_UNLABELED)) {
			if (!org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
				System.err.println("Option " + CLIParameters.OPTION_DATA_TYPE_UNLABELED + " is only available on Linux version");
				System.exit(1);
			}
			sourceExtractor.paths.DIR_INPUT_LABELED_CONVERTED.mkdirs();
			sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED.mkdirs();
			sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.mkdirs();
			sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED.mkdirs();

			File modelPrim;
			File modelSec;
			if (modelBio) {
				modelPrim = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_PRIM + "_bio" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_SEC + "_bio" + modelSuffix);
			} else {
				modelPrim = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_PRIM + "_io" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_SEC + "_io" + modelSuffix);
			}

			// we convert all input annotation files into SOURCE-PRIM and SOURCE-SEC
			sourceExtractor.transformOriginalBratToBratSourcePrimAndSec();
			System.out.println("Transformation en SOURCE-PRIM et SOURCE-SEC réussie.");

			// we start with tagging each files by launching maltparser on these same files. 
			// We eventually end with the (B)IO conversion.
			sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_INPUT_LABELED, false, jubNumber);
			System.out.println("Tous les fichiers ont été transformés en B(IO) sans problème.");

			// we merge all train, dev and test files in three BIO files (Wapiti training input)
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TRAIN_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);
			DIRUtils.mergeFile(sourceExtractor.paths.DIR_DEV_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);

			if (searchSecondary) {
				DIRUtils.mergeFile(sourceExtractor.paths.DIR_TRAIN_FILES_UNLABELED_SEC, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);
				DIRUtils.mergeFile(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);
				DIRUtils.mergeFile(sourceExtractor.paths.DIR_DEV_FILES_UNLABELED_SEC, sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED);
			}
			System.out.println("Copie des fichiers assemblés réussie.\n");

			System.out.println("\t\tDébut entrainement PRIM\t\t");

			// wapiti train -d DEV_PRIM_MERGED_UNLABELED -p PATTERN_PRIM_UNLABELED TRAIN_PRIM_MERGED_UNLABELED modelPrim
			WapitiModel.train(sourceExtractor.paths.PATTERN_PRIM, sourceExtractor.paths.TRAIN_PRIM_MERGED_UNLABELED, modelPrim,
					"-d " + sourceExtractor.paths.DEV_PRIM_MERGED_UNLABELED);

			if (searchSecondary) {
				System.out.println("\t\tDébut entrainement SEC\t\t");

				// wapiti train -d DEV_SEC_MERGED_UNLABELED -p PATTERN_SEC_UNLABELED TRAIN_SEC_MERGED_UNLABELED modelSec
				WapitiModel.train(sourceExtractor.paths.PATTERN_SEC, sourceExtractor.paths.TRAIN_SEC_MERGED_UNLABELED, modelSec,
						"-d " + sourceExtractor.paths.DEV_SEC_MERGED_UNLABELED);
			}

			System.out.println();
			System.out.println("\t\tDébut label PRIM\t\t");

			WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
			// wapiti label -m modelPrim input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM -p
			wapitiPrim.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM,
					jubNumber);

			if (searchSecondary) {
				System.out.println();
				System.out.println("\t\tDébut label SEC\t\t");
				sourceExtractor.deleteSentenceWithoutPrimForSec(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM,
						sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC, true);

				WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
				// wapiti label -m modelSec input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_Sec -p
				wapitiSec.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC,
						jubNumber);
			}

			System.out.println();
			System.out.println("\t\tDébut conversion de la sortie wapiti en Brat\t\t");

			// wapiti has predicted the labels, we now transform the (B)IO files into Brat format.
			sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, false, jubNumber);
			if (searchSecondary) {
				sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, false, jubNumber);
			}

			System.out.println();
			System.out.println("\t\tDébut d'ajout d'annotation à la sortie Brat\t\t");

			// we end up adding some annotations to the previous files (the ones that have been converted to brat)
			DIRUtils.copyFilesWithSameNameToCompare(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED,
					sourceExtractor.paths.DIR_FINAL_RESULT_UNLABELED);
			sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, false, jubNumber);

			System.out.println();
			System.out.println("\t\tScript terminé sans erreur.\t\t");
		} else if (cmd.hasOption(CLIParameters.OPTION_FINAL_FILE) || cmd.hasOption(CLIParameters.OPTION_FINAL_DIR)) {
			if (!cmd.hasOption(CLIParameters.OPTION_OUTPUT_DIR)) {
				System.err.println("An output directory is required in annotation mode (-" + CLIParameters.OPTION_OUTPUT_DIR + ")");
				cli.printHelp(true);
			}
			sourceExtractor.paths.DIR_OUTPUT_FINAL = new File(cmd.getOptionValue(CLIParameters.OPTION_OUTPUT_DIR));
			if (!sourceExtractor.paths.DIR_OUTPUT_FINAL.exists()) {
				sourceExtractor.paths.DIR_OUTPUT_FINAL.mkdirs();
			}

			boolean xml = false;
			if (cmd.hasOption(CLIParameters.OPTION_NEWSML)) {
				xml = true;
			}

			PRODUCTION_MODE = true;
			File input;
			if (cmd.hasOption(CLIParameters.OPTION_FINAL_FILE)) {
				input = new File(cmd.getOptionValue(CLIParameters.OPTION_FINAL_FILE));
			} else {
				input = new File(cmd.getOptionValue(CLIParameters.OPTION_FINAL_DIR));
			}
			File modelPrim = null;
			File modelSec = null;

			if (!modelBio) {
				modelPrim = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_PRIM + "_io" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_SEC + "_io" + modelSuffix);
			} else {
				modelPrim = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_PRIM + "_bio" + modelSuffix);
				modelSec = new File(sourceExtractor.paths.MODEL_FINAL_DIR, MODEL_SEC + "_bio" + modelSuffix);
			}

			if (input.isFile()) {
				sourceExtractor.paths.DIR_INPUT_FINAL = Files.createTempDir();
				sourceExtractor.paths.DIR_INPUT_FINAL.deleteOnExit();
				Files.copy(input, new File(sourceExtractor.paths.DIR_INPUT_FINAL, input.getName()));
				input = sourceExtractor.paths.DIR_INPUT_FINAL;
			}

			if (input.isDirectory()) {
				sourceExtractor.paths.DIR_INPUT_FINAL = input;

				File tempDir = Files.createTempDir();
				tempDir.deleteOnExit();

				sourceExtractor.paths.DIR_INPUT_UNLABELED = sourceExtractor.paths.DIR_INPUT_FINAL;

				sourceExtractor.paths.DATA_UNLABELED_DIR = new File(tempDir, "unlabeled");
				sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "result_unlabeled");
				sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_prim");
				sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "bioWapiti_sec");
				sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "merged_files");
				sourceExtractor.paths.TRAIN_PRIM_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "train_prim.txt");
				sourceExtractor.paths.DEV_PRIM_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "dev_prim.txt");
				sourceExtractor.paths.TRAIN_SEC_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "train_sec.txt");
				sourceExtractor.paths.DEV_SEC_MERGED_UNLABELED = new File(sourceExtractor.paths.DIR_MERGED_FILES_UNLABELED, "dev_sec.txt");
				sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = new File(sourceExtractor.paths.DIR_RESULT_FILES_UNLABELED, "brat_autoWapiti");

				sourceExtractor.paths.DIR_OUTPUT_UNLABELED = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "output_unlabeled");
				sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "test_prim");
				sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC = new File(sourceExtractor.paths.DIR_OUTPUT_UNLABELED, "test_sec");
				sourceExtractor.paths.DIR_INPUT_UNLABELED_MP = new File(sourceExtractor.paths.DATA_UNLABELED_DIR, "input_labeled_mp");

				sourceExtractor.paths.DIR_FINAL_RESULT_UNLABELED = sourceExtractor.paths.DIR_OUTPUT_FINAL;

				sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.mkdirs();

				// we start with tagging each files by launching maltparser on these same files. 
				// We eventually end with the (B)IO conversion.
				sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_INPUT_FINAL, xml, jubNumber);

				System.err.println("Labeling primary sources (Wapiti PRIM)");

				WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
				// wapiti label -m modelPrim input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM -p
				wapitiPrim.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM,
						jubNumber);

				if (searchSecondary) {
					System.err.println();
					System.err.println("Labeling secondary sources (Wapiti SEC)");
					sourceExtractor.deleteSentenceWithoutPrimForSec(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM,
							sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC, true);

					WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
					// wapiti label -m modelSec input=tests_files_one_by_one output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_Sec -p
					wapitiSec.wapitiTest(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_SEC, sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC,
							jubNumber);
				}

				System.err.println();
				System.err.println("Conversion Wapiti output -> Brat");

				// wapiti has predicted the labels, we now transform the (B)IO files into Brat format.
				sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, false, jubNumber);
				if (searchSecondary) {
					sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, false, jubNumber);
				}

				System.err.println();
				System.err.println("Adding coreference and indexing information");

				// we end up adding some annotations to the previous files (the ones that have been converted to brat)
				DIRUtils.copyFilesWithSameNameToCompare(sourceExtractor.paths.DIR_TEST_FILES_UNLABELED_PRIM,
						sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, sourceExtractor.paths.DIR_FINAL_RESULT_UNLABELED);
				sourceExtractor.parseDirForUnlabeled(sourceExtractor.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, xml, jubNumber);
				FileUtils.deleteDirectory(tempDir);

				System.err.println("Output written in " + sourceExtractor.paths.DIR_OUTPUT_FINAL.getAbsolutePath());
			} else {
				System.err.println("File " + input + " is neither a file or a directory");
			}
		} else {
			System.err.println("No parameter has been specified, did nothing");
		}
		long estimatedTime = System.currentTimeMillis() - startTime;
		System.err.println("Done in " + (estimatedTime / 1000.0 / 60.0) + " minutes");
	}
}