package fr.limsi.sourceExtractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class IOUtils {
//    private final static int KB8 = 8 * 1024;

	
//    protected static String read(File file) throws IOException {
//        BufferedInputStream bin = null;
//        FileInputStream fin = null;
//
//        // create FileInputStream object
//        fin = new FileInputStream(file);
//
//        // create object of BufferedInputStream
//        bin = new BufferedInputStream(fin);
//
//
//        //      BufferedInputStream f = new BufferedInputStream(Files.newInputStream(path));
//        byte[] barray = new byte[KB8];
//        //      long checkSum = 0L;
//        int nRead;
//        StringBuilder sb = new StringBuilder();
//
//        while ( (nRead = bin.read(barray, 0, KB8)) != -1 ) {
//            sb.append(new String(barray, 0, nRead));
//        }
//
//        bin.close();
//        
//        return sb.toString();
//    }
	
    
    public static void write(String text, File file) throws IOException {
		FileWriter fw = new FileWriter(file, false);
		BufferedWriter output = new BufferedWriter(fw);
		output.write(text);
		output.close();
	}
	
    public static Map<String, String> loadDictionary(String aFileName) throws IOException {
        Map<String, String> map = new HashMap<String, String>();
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(aFileName)), "UTF-8"));
        String str;
        while ((str = in.readLine()) != null) {
            if (!"".equals(str.trim())) {
                String values[] = str.split("===");
                if (values.length == 2) {
                    String vals[] = values[1].split(";");
                    for (int i = 0; i < vals.length; i++) {
                        String val = vals[i];
                        map.put(val.toLowerCase(), values[0].trim());

                    }
                }
            }
        }
        in.close();
        return map;
    }
    public static Set<String> getFileContentAsSet(InputStream aFileName) throws IOException {
        Set<String> map = new HashSet<String>();
        BufferedReader in = new BufferedReader(new InputStreamReader(aFileName));
        String str;
        while ((str = in.readLine()) != null) {
            map.add(str.trim());
        }
        in.close();
        return map;
    }
    
    public static Set<String> getFileContentAsSet(String aFileName) throws IOException {
        Set<String> map = new HashSet<String>();
        BufferedReader in = new BufferedReader(new FileReader(aFileName));
        String str;
        while ((str = in.readLine()) != null) {
            map.add(str.trim());
        }
        in.close();
        return map;
    }

    public static Map<String, String> getFileContentAsMap(String aFileName) throws IOException {
        Map<String, String> map = new HashMap<String, String>();
        BufferedReader in = new BufferedReader(new FileReader(aFileName));
        String str;
        while ((str = in.readLine()) != null) {
            map.put(str, str);
        }
        in.close();
        return map;
    }
    
    public static Map<String, String> getFileContentAsMap(String aFileName, String aDivider, boolean inLowerCased) throws IOException {
        Map<String, String> map = new HashMap<String, String>();
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(aFileName)), "UTF-8"));
        String str;
        while ((str = in.readLine()) != null) {
            if (!"".equals(str.trim())) {
                String values[] = str.split(aDivider);
                if (values.length == 2) {
                    if (inLowerCased) {
//                    	if(!map.containsValue(values[1].toLowerCase()))
                    		map.put(values[0].trim().toLowerCase(), values[1].trim().toLowerCase());
                        
                    } else {
//                    	if(!map.containsKey(values[1].toLowerCase()))
                    		map.put(values[0].trim(), values[1].trim());
                        
                    }
                }
            }
        }
        in.close();
        return map;
    }
    
    public static Map<String, String> getFileContentAsMap(InputStream aFileName, String aDivider, boolean inLowerCased) throws IOException {
        Map<String, String> map = new HashMap<String, String>();
        BufferedReader in = new BufferedReader(new InputStreamReader(aFileName));
        String str;
        while ((str = in.readLine()) != null) {
            if (!"".equals(str.trim())) {
                String values[] = str.split(aDivider);
                if (values.length == 2) {
                    if (inLowerCased) {
//                    	if(!map.containsValue(values[1].toLowerCase()))
                    		map.put(values[0].trim().toLowerCase(), values[1].trim().toLowerCase());
                        
                    } else {
//                    	if(!map.containsKey(values[1].toLowerCase()))
                    		map.put(values[0].trim(), values[1].trim());
                        
                    }
                }
            }
        }
        in.close();
        return map;
    }
    
    public static Set<String> getFileContentAsSetFromMap(String aFileName, String aDivider) throws IOException {
    	 Set<String> map = new HashSet<String>();
         BufferedReader in = new BufferedReader(new FileReader(aFileName));
         String str;
         while ((str = in.readLine()) != null) {
        	 String values[] = str.split(aDivider);
             map.add(values[1].trim().toLowerCase());
         }
         in.close();
         return map;
	}
    
    public static <V, K> Map<V, K> invert(Map<K, V> map) {

        Map<V, K> inv = new HashMap<V, K>();

        for (Entry<K, V> entry : map.entrySet())
            inv.put(entry.getValue(), entry.getKey());

        return inv;
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public static <K, V extends Comparable<? super V>> Map<K, V>  sortByValue( Map<K, V> map ){
	    Map<K, V> result = new LinkedHashMap<>();
	    Stream<Map.Entry<K, V>> st = map.entrySet().stream();
	
	    st.sorted( Map.Entry.comparingByValue() )
	        .forEachOrdered( e -> result.put(e.getKey(), e.getValue()) );
	
	    return result;
    }


}

