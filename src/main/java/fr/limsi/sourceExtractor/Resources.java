package fr.limsi.sourceExtractor;

import java.util.Map;
import java.util.Set;

public class Resources {
	public Set<String> CITATION_VERBS;
	public Set<String> PROFESSION_WORDS;
	public Set<String> COUNTRIES_AND_CONTINENTS;
	public Set<String> STOP_WORDS;
	public Set<String> PERSON;
	public Set<String> CAPITALS;
	public Set<String> NON_DET_ARTICLES;
	public Set<String> PERSON_ABBR;
	public Set<String> PRONOUNS;
	public Set<String> TEMPORAL_EXPRESSIONS;
	public Map<String, String> COARSE_GRAINED_POS_MAP;

	
	public Resources(Set<String> citationVerbs, Set<String> professionsWords, Set<String> countriesAndContinents,
			Set<String> stopWords, Set<String> person, Set<String> capitals, Set<String> nonDetArticles,
			Set<String> personAbbr, Set<String> pronouns, Set<String> temporalExpressions,
			Map<String, String> coarseGrainedPosMap) {
		super();
		CITATION_VERBS = citationVerbs;
		PROFESSION_WORDS = professionsWords;
		COUNTRIES_AND_CONTINENTS = countriesAndContinents;
		STOP_WORDS = stopWords;
		PERSON = person;
		CAPITALS = capitals;
		NON_DET_ARTICLES = nonDetArticles;
		PERSON_ABBR = personAbbr;
		PRONOUNS = pronouns;
		TEMPORAL_EXPRESSIONS = temporalExpressions;
		COARSE_GRAINED_POS_MAP = coarseGrainedPosMap;
	}

}
