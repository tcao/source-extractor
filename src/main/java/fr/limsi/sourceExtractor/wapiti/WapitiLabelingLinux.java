package fr.limsi.sourceExtractor.wapiti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.limsi.wapiti.Wapiti;

public class WapitiLabelingLinux extends WapitiLabeling {
	private Logger logger = LoggerFactory.getLogger(WapitiLabelingLinux.class);

	private File modelFile;
	
	public WapitiLabelingLinux(File modelFile) {
		this.modelFile = modelFile;
	}

	@Override
	public void label(File input, File outputDir, String filename)
			throws UnsupportedEncodingException, FileNotFoundException {
		String wapitiArgs = "label -m " + this.modelFile+" "+input+ " "+outputDir+"/"+filename+".wapiti -p";
		logger.debug(wapitiArgs);
		String wapitiArgsFormat = String.format(wapitiArgs);
		Wapiti.runWapiti(wapitiArgsFormat);
	}
}
