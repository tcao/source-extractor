package fr.limsi.sourceExtractor.wapiti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ForkJoinPool;

import org.apache.commons.lang3.SystemUtils;

import fr.limsi.sourceExtractor.ProcessingThreadFactory;
import fr.limsi.wapiti.Wapiti;

public abstract class WapitiLabeling {
	public abstract void label(File input, File outputDir, String filename)
			throws UnsupportedEncodingException, FileNotFoundException;

	/**
	 * Méthode permettant d'utiliser wapiti sur les fichiers de tests
	 * 
	 * @param input      Fichier ou dossier à tester
	 * @param modelsFile Le modèle wapiti
	 * @param outputDir  Le dossier de sortie des fichiers
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws InterruptedException
	 */
	public void wapitiTest(File inputDir, File outputDir, int jobNumber, 
			String modelFile, String dirLib)
			throws UnsupportedEncodingException, FileNotFoundException, InterruptedException {
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}

		try {
			String command = String.format("%s/wapiti"
					+ " label -m "
					+ "%s "
					+ "-i %s/ "
					+ "-o %s/ -p", dirLib, modelFile, inputDir.getAbsolutePath(), outputDir.getAbsolutePath());
			System.err.println(command);
			
			String line;
			Process p = Runtime.getRuntime().exec(command);
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null) {
				System.out.println(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null) {
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void wapitiTest(File input, File outputDir, int jobNumber)
			throws UnsupportedEncodingException, FileNotFoundException, InterruptedException {
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}
		if (input.isDirectory()) {
	        ProcessingThreadFactory threadFactory = new ProcessingThreadFactory();		
	        ForkJoinPool forkJoinPool = new ForkJoinPool(jobNumber, threadFactory, null, false);
			ArrayBlockingQueue<WapitiTask> forks = new ArrayBlockingQueue<>(1000000);

			for (File infile : input.listFiles()) {
				if (infile.isFile()) {
					WapitiTask task = new WapitiTask(infile, outputDir, this);
					forks.put(task);
					forkJoinPool.submit(task);
					//wapitiLabelFile(infile, outputDir, this);
				}			
			}
			while (forks.size() > 0) {
				WapitiTask task = forks.take();
				task.join();
			}

		}
		else {
			throw new RuntimeException();
		}
	}

	public void wapitiLabeled(File input, String fileId, String extension, File outputDir) {
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}
		File inFile = new File(input, fileId + "." + extension);
		WapitiTask task = new WapitiTask(inFile, outputDir, this);
		task.compute();
	}

	// Doublon par rapport a WapitiTask
	/*
	 * private static void wapitiLabelFile(File input, File outputDir,
	 * WapitiLabeling wapitiLabeler) throws UnsupportedEncodingException,
	 * FileNotFoundException { String filename = input.getName();
	 * 
	 * int pos = filename.lastIndexOf(".");
	 * 
	 * if (pos > 0) { filename = filename.substring(0, pos); }
	 * 
	 * wapitiLabeler.label(input, outputDir, filename); }
	 */

	public static WapitiLabeling getWapitiInstance(File modelFile)
			throws UnsupportedEncodingException, FileNotFoundException {
		if (org.apache.commons.lang3.SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
			return new WapitiLabelingLinux(modelFile);
		} else if (SystemUtils.IS_OS_WINDOWS) {
			return new WapitiLabelingWindows(modelFile);
		} else {
			throw new RuntimeException("Unknown OS");
		}
	}
}
