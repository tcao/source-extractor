package fr.limsi.sourceExtractor.training;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ForkJoinPool;

import org.grobid.core.jni.WapitiModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.limsi.sourceExtractor.CLIParameters;
import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.DPreprocessingTask;
import fr.limsi.sourceExtractor.ProcessingThreadFactory;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

public class TrainingUnLabel extends AProcess {
	private Logger logger = LoggerFactory.getLogger(TrainingUnLabel.class);

	public TrainingUnLabel(SourceExtractorConfig extractorConfig) {
		super(extractorConfig);
	}

	public void train(String modelSuffix, int jubNumber) throws IOException, InterruptedException {
		if (!org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
			System.err.println("Option " + CLIParameters.OPTION_DATA_TYPE_UNLABELED + " is only available on Linux version");
			System.exit(1);
		}
		this.paths.DIR_INPUT_LABELED_CONVERTED.mkdirs();
		this.paths.DIR_MERGED_FILES_UNLABELED.mkdirs();
		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.mkdirs();
		this.paths.DIR_RESULT_FILES_UNLABELED.mkdirs();

		File modelPrim;
		File modelSec;
		if (modelBio) {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_bio" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_bio" + modelSuffix);
		} else {
			modelPrim = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_PRIM + "_io" + modelSuffix);
			modelSec = new File(this.paths.MODEL_FINAL_DIR, SourceExtractorConstant.MODEL_SEC + "_io" + modelSuffix);
		}

		// we convert all input annotation files into SOURCE-PRIM and
		// SOURCE-SEC
		transformOriginalBratToBratSourcePrimAndSec();
		System.out.println("Transformation en SOURCE-PRIM et SOURCE-SEC réussie.");

		// we start with tagging each files by launching maltparser on these
		// same files.
		// We eventually end with the (B)IO conversion.
		parseDirForUnlabeled(this.paths.DIR_INPUT_LABELED, false, jubNumber);
		System.out.println("Tous les fichiers ont été transformés en B(IO) sans problème.");

		// we merge all train, dev and test files in three BIO files (Wapiti
		// training input)
		DIRUtils.mergeFile(this.paths.DIR_TRAIN_FILES_UNLABELED_PRIM, this.paths.DIR_MERGED_FILES_UNLABELED);
		DIRUtils.mergeFile(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, this.paths.DIR_MERGED_FILES_UNLABELED);
		DIRUtils.mergeFile(this.paths.DIR_DEV_FILES_UNLABELED_PRIM, this.paths.DIR_MERGED_FILES_UNLABELED);

		if (searchSecondary) {
			DIRUtils.mergeFile(this.paths.DIR_TRAIN_FILES_UNLABELED_SEC, this.paths.DIR_MERGED_FILES_UNLABELED);
			DIRUtils.mergeFile(this.paths.DIR_TEST_FILES_UNLABELED_SEC, this.paths.DIR_MERGED_FILES_UNLABELED);
			DIRUtils.mergeFile(this.paths.DIR_DEV_FILES_UNLABELED_SEC, this.paths.DIR_MERGED_FILES_UNLABELED);
		}
		System.out.println("Copie des fichiers assemblés réussie.\n");

		System.out.println("\t\tDébut entrainement PRIM\t\t");

		// wapiti train -d DEV_PRIM_MERGED_UNLABELED -p
		// PATTERN_PRIM_UNLABELED TRAIN_PRIM_MERGED_UNLABELED modelPrim
		WapitiModel.train(this.paths.PATTERN_PRIM, this.paths.TRAIN_PRIM_MERGED_UNLABELED, modelPrim, "-d " + this.paths.DEV_PRIM_MERGED_UNLABELED);

		if (searchSecondary) {
			System.out.println("\t\tDébut entrainement SEC\t\t");

			// wapiti train -d DEV_SEC_MERGED_UNLABELED -p
			// PATTERN_SEC_UNLABELED TRAIN_SEC_MERGED_UNLABELED modelSec
			WapitiModel.train(this.paths.PATTERN_SEC, this.paths.TRAIN_SEC_MERGED_UNLABELED, modelSec, "-d " + this.paths.DEV_SEC_MERGED_UNLABELED);
		}

		System.out.println();
		System.out.println("\t\tDébut label PRIM\t\t");

		WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
		// wapiti label -m modelPrim input=tests_files_one_by_one
		// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM -p
		wapitiPrim.wapitiTest(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, jubNumber);

		if (searchSecondary) {
			System.out.println();
			System.out.println("\t\tDébut label SEC\t\t");
			deleteSentenceWithoutPrimForSec(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, this.paths.DIR_TEST_FILES_UNLABELED_SEC, true);

			WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
			// wapiti label -m modelSec input=tests_files_one_by_one
			// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_Sec -p
			wapitiSec.wapitiTest(this.paths.DIR_TEST_FILES_UNLABELED_SEC, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, jubNumber);
		}

		System.out.println();
		System.out.println("\t\tDébut conversion de la sortie wapiti en Brat\t\t");

		// wapiti has predicted the labels, we now transform the (B)IO files
		// into Brat format.
		parseDirForUnlabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, false, jubNumber);
		if (searchSecondary) {
			parseDirForUnlabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC, false, jubNumber);
		}

		System.out.println();
		System.out.println("\t\tDébut d'ajout d'annotation à la sortie Brat\t\t");

		// we end up adding some annotations to the previous files (the ones
		// that have been converted to brat)
		DIRUtils.copyFilesWithSameNameToCompare(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED,
				this.paths.DIR_FINAL_RESULT_UNLABELED);
		parseDirForUnlabeled(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, false, jubNumber);

		System.out.println();
		System.out.println("\t\tScript terminé sans erreur.\t\t");
	}

	/**
	 * Méthode utilisée pour parcourir les dossiers quand on a des fichiers de test non annotés.<br />
	 * On l'utilise avec un dossier dir et le comportement sera différent selon le dossier entrée.<br />
	 * Fonctionne avec seulement 2 dossiers :<br />
	 * &nbsp;&nbsp; - Celui de tous les fichiers .txt DEJA ANNOTES POUR LES PHASES DE TRAIN ET DE DEV avec les fichiers
	 * .ann qui vont avec,<br />
	 * &nbsp;&nbsp; - Celui où se trouvent / trouveront les fichiers .tag annotés automatiquement avec Wapiti (voir
	 * script bio_wapiti_ann.sh).\n<br />
	 * <br />
	 * On parcourt les fichiers dans le dossier en entrée.<br />
	 * &nbsp;&nbsp;- si on est dans le dossier d'input, on appelle la fonction parseFileToTag sur chaque fichier .txt
	 * pour tag chaque fichier (on "continue" quand fichier .ann).<br />
	 * Ensuite on va lire tous les fichiers dans le dossier des fichiers non annotés pour les transformer en format BIO
	 * (toutes les lignes auront comme tag un O).<br />
	 * <br />
	 * &nbsp;&nbsp;- si on est dans le fichier de sortie de Wapiti, on prend tous les fichiers .tag et pour chaque
	 * fichier on appelle la fonction parseFileToAnn pour annoter automatiquement chaque fichier.<br />
	 * <br />
	 * 
	 * @param input
	 *            Le dossier en entrée (input_labeled ou bioWapiti)
	 * @throws IOException
	 * @throws InterruptedException
	 */
	protected void parseDirForUnlabeled(File input, boolean xmlInput, int jobNumber) throws IOException, InterruptedException {

		if (input.isDirectory()) {

			int nbFilesDir = input.list().length;
			int countTxt = 0;

			// for the training phase we need to take all LABELED input_files to
			// test on unlabeled files
			if (input.getName().equals(this.paths.DIR_INPUT_LABELED.getName()) || (input.equals(this.paths.DIR_INPUT_UNLABELED) && PRODUCTION_MODE)) {
				// If input = DIR_INPUT_FILE, we are in production mode, and we
				// apply
				// the same procedure as for the UNLABELED mode, except that
				// there is no training step.
				boolean skipTraining = false;
				if (PRODUCTION_MODE) {
					skipTraining = true;
				}
				int countUnlabeled = 0;
				int nbFileUnlabeled = this.paths.DIR_INPUT_UNLABELED.list().length;

				// this loop is for the training phase files
				if (!skipTraining) {
					for (File inFile : input.listFiles()) {
						if (inFile.isFile()) {
							String name = inFile.getName();
							String fileId = name.substring(0, name.lastIndexOf("."));

							if (name.endsWith(".txt")) {
								countTxt++;
								System.out.println("TRAIN & DEV LABELED FOR UNLABELED: Traitement du fichier : " + countTxt + "/" + (nbFilesDir / 2));
								parseFileToTag(fileId, inFile, nbFilesDir / 2);
							} else if (name.endsWith(".ann")) {
								continue;
							} else {
								System.out.println("Format de fichier inconnu : " + name + " -> seulement .txt ou .ann");
								System.exit(1);
							}

						}
					}
				}
				// this loop is for the labeling phase files
				ProcessingThreadFactory threadFactory = new ProcessingThreadFactory();
				ForkJoinPool forkJoinPool = new ForkJoinPool(jobNumber, threadFactory, null, false);
				ArrayBlockingQueue<DPreprocessingTask> forks = new ArrayBlockingQueue<>(1000000);

				for (File inFile : this.paths.DIR_INPUT_UNLABELED.listFiles()) {
					if (inFile.isFile() && !inFile.getName().endsWith(".ann")) {
						countUnlabeled++;
						DPreprocessingTask task = new DPreprocessingTask(inFile, xmlInput, countUnlabeled, nbFileUnlabeled, tools, resources, paths, memory, this);
						forks.put(task);
						forkJoinPool.submit(task);
						// parseUnlabeledFile(inFile, xmlInput, countUnlabeled,
						// nbFileUnlabeled,
						// this.tools, this.resources, this.paths, memory);
						// deleteSentenceWithoutPrimForSec (outFileSEC);

					}
				}
				while (forks.size() > 0) {
					DPreprocessingTask task = forks.take();
					task.join();
				}

			}
			// convert the wapiti SOURCE-PRIM output files into brat format
			// files
			else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM.getName())) {
				logger.debug("conversion Wapiti PRIM -> Brat");
				wapitiDirectoryToBratConvertor(input, SourceExtractorConstant.PRIM, nbFilesDir);
			}
			// convert the wapiti SOURCE-SEC output files into brat format files
			else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC.getName())) {
				logger.debug("conversion Wapiti SEC -> Brat");
				wapitiDirectoryToBratConvertor(input, SourceExtractorConstant.SEC, nbFilesDir);
			}
			// add annotation JSON and BRAT
			else if (input.getName().equals(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED.getName())) {
				annotateDirectoryJsonAndBrat(input, xmlInput, nbFilesDir);
			} else {
				System.out.println("Le dossier " + input.getName() + " n'est pas un dossier possible.");
			}
		} else if (input.isFile()) {
			throw new RuntimeException();
		}
	}

	/**
	 * convert the wapiti SOURCE-PRIM output files into brat format
	 * 
	 * @param input
	 * @param prim
	 * @param nbFilesDir
	 * @throws IOException
	 */
	private void wapitiDirectoryToBratConvertor(File input, String prim, int nbFilesDir) throws IOException {
		int countUnlabeled = 0;
		for (File inFile : input.listFiles()) {
			if (inFile.isFile()) {
				countUnlabeled++;
				String filename = inFile.getName();
				String fileId = filename.substring(0, filename.lastIndexOf("."));

				if (!filename.endsWith(".wapiti")) {
					logger.error("Format de fichier inconnu : " + filename + " -> seulement .wapiti");
					System.exit(1);
				}
				logger.debug("  conversion Wapiti -> Brat " + countUnlabeled + "/" + (nbFilesDir) + " " + filename);

				parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, prim, this.tools, this.paths, this.resources, this.memory);
			}
		}
	}

	public void wapitiToBratConvertor(String fileId, String wapitiFilePath, String prim) throws IOException {
		File wapitiFile = new File(wapitiFilePath);
		logger.debug("  conversion Wapiti -> Brat " + wapitiFile.getName());

		parseFileToAnn(fileId, wapitiFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED, prim, this.tools, this.paths, this.resources,
				this.memory);

	}

	/**
	 * annotation JSON and BRAT
	 * 
	 * @param input
	 * @param xmlInput
	 * @param nbFilesDir
	 * @throws IOException
	 */
	public void annotateDirectoryJsonAndBrat(File input, boolean xmlInput, int nbFilesDir) throws IOException {
		int countUnlabeled = 0;
		for (File inFile : input.listFiles()) {
			if (inFile.isFile()) {
				countUnlabeled++;
				String filename = inFile.getName();
				String fileId = filename.substring(0, filename.lastIndexOf("."));

				assert filename.endsWith(".ann") : "Unknown file format: " + filename + " -> should be .ann";

				logger.debug("ADD ANNOTATION : Traitement du fichier : " + countUnlabeled + "/" + (nbFilesDir));
				// System.out.println(inFile.getName());
				File outputFile = parseFileToAddAnnotationToBrat(fileId, inFile);

				logger.debug("1. Output written in " + outputFile.getAbsolutePath());

				// If XML input, then we can delete the TXT file in the
				// input directory
				if (xmlInput) {
					(new File(this.paths.DIR_INPUT_UNLABELED, filename.replaceAll("[.][^.]+$", ".txt"))).delete();
				}

			}
		}
	}
}
