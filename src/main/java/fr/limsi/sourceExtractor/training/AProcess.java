/**
 * Bertrand Goupil - AFP Medialab - Refactoring
 * Xavier Tannier - Limsi - Core coding
 */
package fr.limsi.sourceExtractor.training;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import edu.stanford.nlp.pipeline.Annotation;
import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.Memory;
import fr.limsi.sourceExtractor.Pair;
import fr.limsi.sourceExtractor.Paths;
import fr.limsi.sourceExtractor.Resources;
import fr.limsi.sourceExtractor.SourceAnnotation;
import fr.limsi.sourceExtractor.Tools;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorUtil;

public abstract class AProcess extends AProcessSupport {
	//private Logger logger = LoggerFactory.getLogger(AProcess.class);


	private int counterFiles = 0;
	protected Paths paths;
	protected Memory memory;
	protected Tools tools;
	protected Resources resources;
	protected boolean searchSecondary, modelBio, outputBrat;
	// If we're working on label data split into train/dev/test (evaluation purpose)
	protected boolean LABELED_MODE = false;
	// If we're working on production (already have a model, just apply it to unlabeled data)
	protected boolean PRODUCTION_MODE = false;

	public AProcess(SourceExtractorConfig extractorConfig) {
		super(extractorConfig);
		this.paths = extractorConfig.getPaths();
		this.memory = extractorConfig.getMemory();
		this.tools = extractorConfig.getTools();
		this.resources = extractorConfig.getResources();
		this.outputBrat = extractorConfig.isOutputBrat();
		this.searchSecondary = extractorConfig.isSearchSecondary();
		this.modelBio = extractorConfig.isModelBio();
	}

	protected void transformOriginalBratToBratSourcePrimAndSec() throws IOException {

		for (File inFile : this.paths.DIR_INPUT_LABELED.listFiles()) {
			HashMap<String, String> id2Line = new HashMap<>();
			HashMap<String, String> id2Type = new HashMap<>();
			HashSet<String> sources = new HashSet<>();
			HashSet<String> secondarySources = new HashSet<>();

			String name = inFile.getName();

			if (name.endsWith(".ann")) {
				String ann = Files.toString(inFile, Charsets.UTF_8);
				String[] wordsAnn = ann.split("\\r?\\n");

				for (int i = 0; i < wordsAnn.length; i++) {
					Matcher matcher = SourceExtractorConstant.BRAT_ENTITY_PATTERN.matcher(wordsAnn[i]);
					if (matcher.find()) {
						String id = matcher.group(1);
						String type = matcher.group(2);
						String restOfLine = matcher.group(3);
						id2Line.put(id, restOfLine);
						id2Type.put(id, type);
						if (type.equals(SourceExtractorConstant.SOURCE)) {
							sources.add(id);
						}
						if (type.equals(SourceExtractorConstant.SECONDARY)) {
							secondarySources.add(restOfLine.trim());
						}
					}
				}

				FileWriter fw = new FileWriter(this.paths.DIR_INPUT_LABELED_CONVERTED + "/" + inFile.getName(), false);

				BufferedWriter output = new BufferedWriter(fw);

				for (String sourceId : sources) {
					if (secondarySources.contains(sourceId)) {
						output.append(sourceId + "\t" + SourceExtractorConstant.SOURCESEC + " " + id2Line.get(sourceId) + "\n");
					} else {
						output.append(sourceId + "\t" + SourceExtractorConstant.SOURCEPRIM + " " + id2Line.get(sourceId) + "\n");
					}

				}
				output.close();
			} else if (name.endsWith(".txt")) {
				continue;
			} else {
				System.out.println("seulement dossier avec des fichiers .ann ou .txt");
				System.exit(1);
			}
		}
	}
	
	/**
	 * 
	 * @param fileId
	 * @param inFile
	 * @return
	 * @throws IOException
	 */
	private OffsetMaps communParseFileToAddAnnotationToBrat(String fileId, File inFile) throws IOException {
		String bratGeneratedWithWapitiResult = Files.toString(new File(inFile.getParentFile(), fileId + ".ann"), Charsets.UTF_8);
		return communParseFileToAddAnnotationToBrat(fileId, bratGeneratedWithWapitiResult);
	}

	/**
	 * Méthode utilisée pour parcourir les fichiers .ann dans le but d'y ajouter des annotations. </br>
	 * On commence par récupérer l'annotation BRAT automatiquement générée par la transformation du résultat de wapiti
	 * (bratAutoTransformed).</br>
	 * On prend ensuite le résultat de sortie de wapiti (wapitiResult).</br>
	 * On a besoin des offsets de chaque mots donc parse de nouveau le texte pour récupérer seulements les offsets
	 * (taggedTextOnlyOffset).</br>
	 * </br>
	 * Ensuite on appelle la fonction parseTaggedTextToKeepCoreferencesAndSubject pour trouver les sujets / correference
	 * et garder les meilleurs.</br>
	 * </br>
	 * On finit par ajouter les éléments trouvés au nouveau fichier BRAT avec addAnnotationCorreferenceToBrat.
	 * 
	 * @param inFile
	 * @throws IOException
	 */
	private OffsetMaps communParseFileToAddAnnotationToBrat(String fileId, String bratGeneratedWithWapitiResult) throws IOException {
		String wapitiResult = "";

		String taggedTextOnlyOffset = "";
		if (LABELED_MODE) {
			wapitiResult = Files.toString(new File(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM, fileId + ".wapiti"), Charsets.UTF_8);
			//inputTxtFile = new File(this.paths.DIR_INPUT_LABELED, fileId + ".txt");
		} else {
			wapitiResult = Files.toString(new File(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, fileId + ".wapiti"), Charsets.UTF_8);
			//inputTxtFile = new File(this.paths.DIR_INPUT_UNLABELED, fileId + ".txt");
		}
		//le fichier n'est jamais utilisé => remplacement par null (à tester possible effet de bord)
		String text = SourceExtractorUtil.getDocText(fileId, null, memory);
		Annotation annotation = new Annotation(text);
		taggedTextOnlyOffset = parseText(annotation, fileId, text, tools, resources, memory);

		HashMap<String, String> retrieveOffsetsOfSource = new HashMap<>();

		List<String[]> listOfBratAnnotation = new LinkedList<String[]>();
		String[] wordsBratAnnotation = bratGeneratedWithWapitiResult.split("\\r?\\n");

		for (int i = 0; i < wordsBratAnnotation.length; i++) {
			listOfBratAnnotation.add(wordsBratAnnotation[i].split("\\s+"));
		}

		// we keep only the SOURCE-PRIM, it is useful not to have the secondary
		// ones to find the subjects
		for (int i = 0; i < wordsBratAnnotation.length; i++) {
			Matcher matcher = SourceExtractorConstant.BRAT_ENTITY_PATTERN.matcher(wordsBratAnnotation[i]);
			if (matcher.find()) {
				String id = matcher.group(1);
				String source = matcher.group(2);
				String restOfLine = matcher.group(3);

				if (source.contains(SourceExtractorConstant.PRIM)) {
					retrieveOffsetsOfSource.put(id, restOfLine.trim());
				}
			}
		}

		// this will contain the left and right offset of each source
		TreeMap<Integer, Integer> sourceOffsets = new TreeMap<>();
		// this will contain the left and right offset of each subject found by
		// maltparser (column isSuj)
		TreeMap<Integer, Integer> subjectOffsets = new TreeMap<>();
		// this will contain the left and right offset of each professions word
		TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets = new TreeMap<>();
		// this will contain the left and right offset of each pronoun
		TreeMap<Integer, Integer> pronounOffsets = new TreeMap<>();
		HashMap<Integer, Integer> bestSubjectForTheSources = new HashMap<>();

		HashSet<Integer> startOffsetsPrim = new HashSet<>();
		HashSet<Integer> inOffsetsPrim = new HashSet<>();
		HashSet<Integer> startOffsetsSec = new HashSet<>();
		HashSet<Integer> inOffsetsSec = new HashSet<>();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = new HashMap<>();

		// we look for what to write as annotation and we keep the best ones
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = parseTaggedTextToKeepCoreferencesAndSubject(wapitiResult, taggedTextOnlyOffset,
				retrieveOffsetsOfSource, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, bestSubjectForTheSources);

		fillMapsWithOffsets(listOfBratAnnotation, fileId, startOffsetsPrim, inOffsetsPrim, startOffsetsSec, inOffsetsSec, sourceAndOffset);

		OffsetMaps offsetMaps = new OffsetMaps(sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, bestSubjectForTheSources,
				sourceAndOffset, wordsAndPosByOffset);
		return offsetMaps;

	}
	
	/**
	 * Sent results as an outputstream
	 * @param fileId
	 * @param inFile
	 * @param outputStream
	 * @throws IOException
	 */
	protected String parseFileToAddAnnotationToBrat(String fileId, String bratAnn) throws IOException {
		OffsetMaps offsetMaps = communParseFileToAddAnnotationToBrat(fileId, bratAnn);
		TreeMap<Integer, Integer> sourceOffsets = offsetMaps.getSourceOffsets();
		TreeMap<Integer, Integer> subjectOffsets = offsetMaps.getSubjectOffsets();
		TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets = offsetMaps.getProfAndSecondarySourceTriggerOffsets();
		TreeMap<Integer, Integer> pronounOffsets = offsetMaps.getPronounOffsets();
		HashMap<Integer, Integer> bestSubjectForTheSources = offsetMaps.getBestSubjectForTheSources();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = offsetMaps.getSourceAndOffset();
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = offsetMaps.getWordsAndPosByOffset();
		String jsonResult = addAnnotationCoreferenceToBrat(fileId, bratAnn, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, wordsAndPosByOffset,
				bestSubjectForTheSources, sourceAndOffset);
		return jsonResult;
	}

	protected File parseFileToAddAnnotationToBrat(String fileId, File inFile) throws IOException {
		OffsetMaps offsetMaps = communParseFileToAddAnnotationToBrat(fileId, inFile);
		TreeMap<Integer, Integer> sourceOffsets = offsetMaps.getSourceOffsets();
		TreeMap<Integer, Integer> subjectOffsets = offsetMaps.getSubjectOffsets();
		TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets = offsetMaps.getProfAndSecondarySourceTriggerOffsets();
		TreeMap<Integer, Integer> pronounOffsets = offsetMaps.getPronounOffsets();
		HashMap<Integer, Integer> bestSubjectForTheSources = offsetMaps.getBestSubjectForTheSources();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = offsetMaps.getSourceAndOffset();
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = offsetMaps.getWordsAndPosByOffset();
		// we write annotations that we found to be the best

		File outputFile = addAnnotationCoreferenceToBrat(fileId, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, wordsAndPosByOffset,
				bestSubjectForTheSources, sourceAndOffset);
		return outputFile;
	}

	/**
	 * inSec and outSEC can be the same
	 * 
	 * @param inSEC
	 * @param outSEC
	 * @param testMode
	 * @throws IOException
	 */
	protected void deleteSentenceWithoutPrimForSec(File inSEC, File outSEC, boolean testMode) throws IOException {
		if (inSEC.isDirectory()) {
			for (File file : inSEC.listFiles()) {
				deleteSentenceWithoutPrimForSec(file, new File(outSEC, file.getName()), testMode);
			}
		} else if (inSEC.isFile()) {
			Scanner scan = new Scanner(inSEC);
			StringBuilder result = new StringBuilder();

			StringBuilder sentenceBuffer = new StringBuilder();
			boolean foundPrim = false;

			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.length() > 1) {
					String[] fields = line.split("\\s+");
					String primClassName;
					if (testMode) {
						primClassName = fields[fields.length - 1];
					} else {
						primClassName = fields[SourceExtractorConstant.numberFieldsPrim];
					}

					assert primClassName.endsWith(SourceExtractorConstant.PRIM) || primClassName.equals(SourceExtractorConstant.OUT) : "You got the wrong column!";

					if (!primClassName.equals(SourceExtractorConstant.OUT)) {
						foundPrim = true;
					}
					sentenceBuffer.append(line + "\n");
				} else {
					if (foundPrim) {
						result.append(sentenceBuffer.toString());
						result.append("\n");
						foundPrim = false;
					}
					// else {
					// // add a newline so that we know that a sentence
					// // was skipped
					// output.write("\n");
					// }
					sentenceBuffer = new StringBuilder();
				}

			}
			scan.close();
			Files.write(result.toString(), outSEC, Charsets.UTF_8);
		} else {
			throw new RuntimeException();
		}
	}

	/**
	 * Méthode qui permet de tag le fichier inFile en entrée avec la fonction parseText et de trouver les sujets avec
	 * parseTextForMaltParser.<br />
	 * On définit 3 dossiers (train, dev, test) utiles pour le CRF. Ces dossiers changent selon si on est dans le cas de
	 * fichiers de test déjà annotés ou non.<br />
	 * <br />
	 * On va ensuite rassembler les résultats des deux parseText pour transformer le résultat final en un fichier
	 * automatiquement transformé en fichier .tag (écrit avec un encoding (B)IO) par la méthode bioConversion qui sera
	 * mis dans un des 3 dossiers.<br />
	 * <br />
	 * Si labeled : on met les 75% premiers fichiers dans le dossier train, puis les 10 % suivant dans un dossier dev et
	 * les 15% restant dans un dossier de test. (on crée (ou efface puis crée de nouveau) les dossiers)<br />
	 * <br />
	 * Si !labeled : on met les 90% premiers fichiers du dossier input_labeled dans un dossier train et les 10% suivant
	 * dans un dossier dev (on a besoin d'utiliser les fichiers déjà annotés pour les phases d'entrainement et de
	 * développement). Le dossier de test est créé automatiquement avec la fonction parseDirUnlabeled.
	 * 
	 * 
	 * @param inFile
	 *            Le fichier qu'il faut tag.
	 * @param nbFiles
	 *            Le nombre de fichiers dans le dossier pour pouvoir mettre les x% dans le dossier spécifique
	 * @return
	 * @throws IOException
	 */
	protected String parseFileToTag(String fileId, File inFile, int nbFiles) throws IOException {

		String text = SourceExtractorUtil.getDocText(fileId, inFile, memory);

		text = text.replaceAll("\\r", "");

		File trainDirPRIM = null;
		File testDirPRIM = null;
		File devDirPRIM = null;

		File trainDirSEC = null;
		File testDirSEC = null;
		File devDirSEC = null;

		File outFileSEC = null;
		File outFilePRIM = null;
		//File outFileMP = null;

		if (LABELED_MODE) {
			trainDirPRIM = this.paths.DIR_TRAIN_FILES_PRIM;
			testDirPRIM = this.paths.DIR_TEST_FILES_PRIM;
			devDirPRIM = this.paths.DIR_DEV_FILES_PRIM;
			trainDirSEC = this.paths.DIR_TRAIN_FILES_SEC;
			testDirSEC = this.paths.DIR_TEST_FILES_SEC;
			devDirSEC = this.paths.DIR_DEV_FILES_SEC;
		} else {
			trainDirPRIM = this.paths.DIR_TRAIN_FILES_UNLABELED_PRIM;
			testDirPRIM = this.paths.DIR_TEST_FILES_UNLABELED_PRIM;
			devDirPRIM = this.paths.DIR_DEV_FILES_UNLABELED_PRIM;
			trainDirSEC = this.paths.DIR_TRAIN_FILES_UNLABELED_SEC;
			testDirSEC = this.paths.DIR_TEST_FILES_UNLABELED_SEC;
			devDirSEC = this.paths.DIR_DEV_FILES_UNLABELED_SEC;
		}

		counterFiles += 1;

		boolean test = false;

		if (LABELED_MODE) {
			// Files distribution :
			// 75% train, 10% dev, 15% test.
			if (counterFiles < 0.75 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(trainDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(trainDirPRIM, fileId, ".tag");
			} else if (counterFiles > 0.75 * nbFiles && counterFiles < 0.85 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(devDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(devDirPRIM, fileId, ".tag");
			} else {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(testDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(testDirPRIM, fileId, ".tag");
				test = true;
			}
		} else {
			// Files distribution :
			// 90% train, 10% dev
			// all files unlabeled in test.
			if (counterFiles < 0.90 * nbFiles) {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(trainDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(trainDirPRIM, fileId, ".tag");
			} else {
				outFileSEC = DIRUtils.createDirAndFilesWithExt(devDirSEC, fileId, ".tag");
				outFilePRIM = DIRUtils.createDirAndFilesWithExt(devDirPRIM, fileId, ".tag");
			}
		}

		String ann = Files.toString(new File(this.paths.DIR_INPUT_LABELED_CONVERTED, fileId + ".ann"), Charsets.UTF_8);

		//not use ? maybe use for training.
		//outFileMP = DIRUtils.createDirAndFilesWithExt(this.paths.DIR_INPUT_LABELED_MP, fileId, ".conll");

		String parsedText = tagAndConvert(fileId, text, outFileSEC, ann, this.tools, this.resources, memory);

		deleteLastColumnForPrim(outFilePRIM, outFileSEC, test);
		if (test) {
			outFileSEC.delete();
		} else {
			SourceExtractorUtil.deleteSentenceWithoutPrimForSec(outFileSEC, outFileSEC, false);
		}

		return parsedText;
	}

	/**
	 * Méthode qui permet de démarrer la conversion d'un fichier BIO en un fichier BRAT.<br />
	 * <br />
	 * On commence par récupérer le .txt du fichier BIO à convertir puis on le tag avec la méthode parseText pour avoir
	 * les offsets des mots.<br />
	 * <br />
	 * Ensuite on sauvegarde le contenu du fichier BIO généré par wapiti en entrée dans une variable annAuto puis on
	 * appelle la fonction annConversion<br />
	 * qui va convertir le fichier.<br />
	 * <br />
	 * 
	 * @param inFile
	 *            Le fichier de prédiction écrit par la phase de test de Wapiti.
	 * @param dirAuto
	 *            Le dossier où enregistrer le résultat de la conversion.
	 * @throws IOException
	 */
	protected void parseFileToAnn(String fileId, File inFile, File dirAuto, String sourceExt, Tools tools, Paths paths, Resources resources, Memory memory)
			throws IOException {

		String text = "";

		// if (!PRODUCTION_MODE) {
		if (LABELED_MODE) {
			text = SourceExtractorUtil.getDocText(fileId, new File(paths.DIR_INPUT_LABELED, fileId + ".txt"), memory);
		} else {
			text = SourceExtractorUtil.getDocText(fileId, new File(paths.DIR_INPUT_UNLABELED, fileId + ".txt"), memory);
		}
		
		Annotation annotation = new Annotation(text);	
		String tagWithOffset = parseText(annotation, fileId, text, tools, resources, memory);

		String resultWapiti = "";
		String line;

		// Read Wapiti output file
		FileReader rd = new FileReader(inFile);
		BufferedReader br = new BufferedReader(rd);

		while ((line = br.readLine()) != null) {
			// If we are annotating secondary sources,
			// we want to make sure that they won't overlap
			// with previously annotated primary sources
			if (sourceExt.equals(SourceExtractorConstant.SEC)) {
				String[] fields = line.split("\\s+");
				String primInfo;

				if (fields.length == 1) {
					resultWapiti += "\n";
					continue;
				}

				// the last fields before class is the primary source BIO
				// information
				// If the data is labeled, this field is n-2, otherwise n-1
				// if (LABELED_MODE) {
				primInfo = fields[fields.length - 2];
				// }
				// else {
				// primInfo = fields[fields.length - 1];
				// }
				if (!(primInfo.endsWith(SourceExtractorConstant.PRIM) || primInfo.equals(SourceExtractorConstant.OUT))) {
					System.out.println();
				}
				assert primInfo.endsWith(SourceExtractorConstant.PRIM) || primInfo.equals(SourceExtractorConstant.OUT) : primInfo;
				// if the primary source information is not "O",
				// then the secondary source information (class) must be set to
				// "O"
				if (!primInfo.equals(SourceExtractorConstant.OUT)) {
					fields[fields.length - 1] = SourceExtractorConstant.OUT;
				}
				resultWapiti += String.join(" ", fields) + "\n";
			}
			// If we are annotating primary sources,
			// we just take the BIO output as it is
			else {
				resultWapiti += line + "\n";
			}
		}

		annConversion(tagWithOffset, resultWapiti, fileId, text, dirAuto, sourceExt);
		rd.close();
		br.close();
	}

	/**
	 * Méthode qui permet de convertir du (B)IO vers BRAT. (seulement les SOURCE).<br />
	 * <br />
	 * Nous enregistrons les offsets de tagAndOffset que nous utilisons dans l'affichage.<br />
	 * <br />
	 * Nous parcourons ligne par ligne le String annAuto et nous regardons chaque dernier champ. Si le dernier champ est
	 * un B alors Wapiti a prédit qu'une source commençait à ce mot. Nous enregistrons l'offset et nous regardons
	 * combien de I se trouvent après ce B. Nous affichons ensuite le résultat en écrivant la source complète issue du
	 * texte original grâce aux offsets sauvegardés. <br />
	 * 
	 * 
	 * @param tagAndOffset
	 *            parseText du fichier .txt original pour avoir les offsets des mots.
	 * @param resultWapiti
	 *            Le fichier de sortie de Wapiti après la phase de test (nous prenons la prédiction de Wapiti).
	 * @param fileId
	 *            Le nom du fichier en entrée pour pouvoir nommer le fichier en sortie.
	 * @param originalText
	 *            Le texte original pour pouvoir afficher la source grâce à un substring.
	 * @param dirAuto
	 *            Dossier où enregistrer le résultat de la conversion.
	 * @throws IOException
	 */
	private void annConversion(String tagAndOffset, String resultWapiti, String fileId, String originalText, File dirAuto, String sourceExt) throws IOException {
		boolean isPrim = false;

		if (sourceExt.equals(SourceExtractorConstant.PRIM))
			isPrim = true;

		boolean isSec = false;

		if (sourceExt.equals(SourceExtractorConstant.SEC))
			isSec = true;

		// Align wapiti output with lines containing word offsets
		List<String[]> listOfTextTag = new LinkedList<String[]>();
		List<String[]> listOfFieldsResultWapiti = new LinkedList<String[]>();

		if (resultWapiti.trim().length() > 0) {
			String[] linesWordsTag = tagAndOffset.split("\\r?\\n");
			String[] linesResultWapiti = resultWapiti.split("\\r?\\n");

			List<String[]> listOfSentenceTextTag = new LinkedList<String[]>();
			List<String[]> listOfSentenceResultWapiti = new LinkedList<String[]>();

			int linesWordsIndex = 0;
			boolean skipSentence = false;
			for (String lineWords : linesWordsTag) {
				if (lineWords.length() > 0) {
					if (!skipSentence) {
						if (linesWordsIndex > linesResultWapiti.length - 1) {
							skipSentence = true;
							break;
						}
						String lineResultWapiti = linesResultWapiti[linesWordsIndex];
						assert lineResultWapiti.length() > 0 : "Line " + linesWordsIndex + " should not be empty";
						String[] fieldsResultWapiti = lineResultWapiti.split("\\s+");
						String[] fieldsLineWords = lineWords.split("\\s+");
						// Must be same word, otherwise skip the sentence
						if (fieldsResultWapiti[0].equals(fieldsLineWords[1])) {
							listOfSentenceTextTag.add(fieldsLineWords);
							listOfSentenceResultWapiti.add(fieldsResultWapiti);
							linesWordsIndex++;
						} else {
							skipSentence = true;
						}
					}

				}
				// Empty line (EOS)
				else {
					if (!skipSentence) {
						listOfFieldsResultWapiti.addAll(listOfSentenceResultWapiti);
						listOfTextTag.addAll(listOfSentenceTextTag);
						listOfFieldsResultWapiti.add(new String[0]);
						listOfTextTag.add(new String[0]);
						++linesWordsIndex;
					} else {
						skipSentence = false;
					}
					listOfSentenceResultWapiti.clear();
					listOfSentenceTextTag.clear();
				}
			}

			// Last sentence
			if (!skipSentence) {
				listOfFieldsResultWapiti.addAll(listOfSentenceResultWapiti);
				listOfTextTag.addAll(listOfSentenceTextTag);
			}
		}

		assert listOfFieldsResultWapiti.size() == listOfTextTag.size() : "Wapiti output files and offset files must have the same length " + ".\nHere "
				+ listOfFieldsResultWapiti.size() + " != " + listOfTextTag.size();

		// if it is a new conversion, we must delete previous files in the
		// directory
		if (isPrim) {
			DIRUtils.deleteFilesInDirIfExist(dirAuto, fileId, ".ann");
		}

		File bratOutputFile = new File(dirAuto, fileId + ".ann");

		int sourceNumberBrat = 0;
		for (int index = 0; index < listOfTextTag.size(); index++) {
			String[] fieldsResultWapiti = listOfFieldsResultWapiti.get(index);
			assert listOfTextTag.get(index).length == SourceExtractorConstant.numberFieldsSec || listOfTextTag.get(index).length == 0 : "Offset files must have "
					+ SourceExtractorConstant.numberFieldsSec + " fields\n" + " but we have: \t" + Arrays.toString(listOfTextTag.get(index)) + " (size "
					+ listOfTextTag.get(index).length + ")";

			assert !(fieldsResultWapiti.length != SourceExtractorConstant.numberFieldsPrim + 1 && fieldsResultWapiti.length != SourceExtractorConstant.numberFieldsSec + 1
					&& listOfTextTag.get(index).length != 0) : "Wapiti output must have " + (SourceExtractorConstant.numberFieldsPrim + 1) + " (PRIM) or "
							+ (SourceExtractorConstant.numberFieldsSec + 1) + " (SEC) fields\n" + " but we have: \t"
							+ Arrays.toString(listOfFieldsResultWapiti.get(index));

			// !=1 because we separate each sentence by a new line
			if (listOfTextTag.get(index).length != 0) {
				String classNameAuto = "";
				if (isPrim) {
					// wapiti annotes on the last column, we get this.
					classNameAuto = fieldsResultWapiti[fieldsResultWapiti.length - 1];
					assert classNameAuto.endsWith(SourceExtractorConstant.PRIM) || classNameAuto.equals(SourceExtractorConstant.OUT)
							|| classNameAuto.endsWith(SourceExtractorConstant.NULL) : classNameAuto;
				} else if (isSec) {
					// wapiti annotes on the last column, we get this.
					classNameAuto = fieldsResultWapiti[fieldsResultWapiti.length - 1];
					assert classNameAuto.endsWith(SourceExtractorConstant.SEC) || classNameAuto.equals(SourceExtractorConstant.OUT)
							|| classNameAuto.endsWith(SourceExtractorConstant.NULL) : classNameAuto;
				}

				// if we meet a B or a I we are sure that it is the first word
				// of the source.
				if (classNameAuto.startsWith(SourceExtractorConstant.encodingI) || classNameAuto.startsWith(SourceExtractorConstant.encodingB)) {
					sourceNumberBrat++;
					writeToFiles(index, listOfFieldsResultWapiti, listOfTextTag, bratOutputFile, sourceNumberBrat, originalText, sourceExt);
				}

			}
		}
		// If no source at all, create an empty .ann file
		Files.append("", bratOutputFile, Charsets.UTF_8);
	}

	/**
	 * Méthode qui permet d'écrire les sources trouvées par wapiti.
	 * 
	 * @param index
	 *            L'index du début de source
	 * @param listOfFieldsResultWapiti
	 *            La liste des phrases avec comme dernière colonne la prédiction de wapiti
	 * @param listOfTextTag
	 *            La liste des phrases avec les offsets aux dernières colonnes
	 * @param outputBRAT
	 *            StringBuilder pour écrire le résultat
	 * @param sourceNumberBrat
	 *            Numéro de la source à afficher
	 * @param originalText
	 *            Le string du text original pour pouvoir afficher correctement la source
	 * @param sourceExt
	 *            L'extension de la source à écrire (-PRIM ou -SEC)
	 * @throws IOException
	 */
	private void writeToFiles(int index, List<String[]> listOfFieldsResultWapiti, List<String[]> listOfTextTag, File outputBRAT, int sourceNumberBrat,
			String originalText, String sourceExt) throws IOException {

		String[] offsetsStr = listOfTextTag.get(index)[SourceExtractorConstant.numberFieldsPrim].split(":");
		int offsetLeft = Integer.parseInt(offsetsStr[0]);
		int offsetRight = Integer.parseInt(offsetsStr[1]);

		String word = listOfFieldsResultWapiti.get(index)[0];

		int gapForEndOffset = 1;
		switch (sourceExt) {
		case SourceExtractorConstant.PRIM:
			String[] fieldsResultWapiti = listOfFieldsResultWapiti.get(index + gapForEndOffset);
			// if this annotation is not the last one of the sentence
			if (fieldsResultWapiti.length != 1) {
				// if this annotation does not exceed the
				// listOfFieldsResultWapiti size and is equal to a source
				assert fieldsResultWapiti[SourceExtractorConstant.numberFieldsPrim].endsWith(sourceExt)
						|| fieldsResultWapiti[SourceExtractorConstant.numberFieldsPrim].equals(SourceExtractorConstant.OUT);
				if (index + gapForEndOffset < listOfFieldsResultWapiti.size()
						&& SourceExtractorConstant.numberFieldsPrim < fieldsResultWapiti.length 
						&& fieldsResultWapiti[SourceExtractorConstant.numberFieldsPrim].equals(SourceExtractorConstant.encodingI + sourceExt)) {

					while (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapiti.length > 1
							&& fieldsResultWapiti[SourceExtractorConstant.numberFieldsPrim].equals(SourceExtractorConstant.encodingI + sourceExt)) {

						// we replace all I-sourceExt found by null otherwise
						// each I-sourceExt would be the start a one source.
						// we could have Le président Francois Hollande,
						// président Francois Hollande, Francois Hollande,
						// Hollande (4 sources) if we
						// did not do that change
						listOfFieldsResultWapiti.get(index + gapForEndOffset)[SourceExtractorConstant.numberFieldsPrim] = SourceExtractorConstant.NULL;

						gapForEndOffset++;
						fieldsResultWapiti = listOfFieldsResultWapiti.get(index + gapForEndOffset);
					}

					// we take the right offset of the source
					String[] offsetsStrI = listOfTextTag.get(index + gapForEndOffset - 1)[SourceExtractorConstant.numberFieldsPrim - 1].split(":");
					int offsetRightI = Integer.parseInt(offsetsStrI[1]);
					String sourceText = originalText.substring(offsetLeft, offsetRightI);

					if (sourceText.contains("\"")) {
						sourceText = sourceText.replace("\"", "\\\"");
					}

					Files.append("T" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + offsetLeft + " " + offsetRightI + "\t"
							+ originalText.substring(offsetLeft, offsetRightI) + "\t\n", outputBRAT, Charsets.UTF_8);
				} else {
					// If it is a pronoun which begins by a "-" like "-t-il" we
					// write only "il" starting from the offset at last index of
					// "-"
					if (word.startsWith("-")) {
						int newStartOffset = word.lastIndexOf("-") + 1;
						word = word.substring(newStartOffset);
						Files.append("T" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + (offsetLeft + newStartOffset) + " " + offsetRight
								+ "\t" + word + "\t\n", outputBRAT, Charsets.UTF_8);
					}
					// Otherwise there is only one word in the source and we
					// write it
					else {
						String sourceText = originalText.substring(offsetLeft, offsetRight);
						if (sourceText.contains("\""))
							sourceText.replace("\"", "\\\"");

						Files.append("T" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + offsetLeft + " " + offsetRight + "\t" + sourceText
								+ "\t\n", outputBRAT, Charsets.UTF_8);
					}
				}
			}
			break;
		// same as case PRIM
		case SourceExtractorConstant.SEC:
			String[] fieldsResultWapitiSec = listOfFieldsResultWapiti.get(index + gapForEndOffset);
			if (listOfFieldsResultWapiti.get(index + gapForEndOffset).length != 1) {
				assert fieldsResultWapitiSec[SourceExtractorConstant.numberFieldsSec].endsWith(sourceExt)
						|| fieldsResultWapitiSec[SourceExtractorConstant.numberFieldsSec].equals(SourceExtractorConstant.OUT);
				if (index + gapForEndOffset < listOfFieldsResultWapiti.size()
						&& SourceExtractorConstant.numberFieldsSec < fieldsResultWapitiSec.length
						&& fieldsResultWapitiSec[SourceExtractorConstant.numberFieldsSec].equals(SourceExtractorConstant.encodingI + sourceExt)
						&& listOfFieldsResultWapiti.get(index + gapForEndOffset).length != 1) {
					while (index + gapForEndOffset < listOfFieldsResultWapiti.size() && fieldsResultWapitiSec.length != 1
							&& fieldsResultWapitiSec[SourceExtractorConstant.numberFieldsSec].equals(SourceExtractorConstant.encodingI + sourceExt)) {

						listOfFieldsResultWapiti.get(index + gapForEndOffset)[SourceExtractorConstant.numberFieldsSec] = SourceExtractorConstant.NULL;
						gapForEndOffset++;
						fieldsResultWapitiSec = listOfFieldsResultWapiti.get(index + gapForEndOffset);
					}

					String[] offsetsStrI = listOfTextTag.get(index + gapForEndOffset - 1)[SourceExtractorConstant.numberFieldsSec - 2].split(":");

					int offsetRightI = Integer.parseInt(offsetsStrI[1]);

					String sourceText = originalText.substring(offsetLeft, offsetRightI);

					if (sourceText.contains("\"")) {
						sourceText = sourceText.replace("\"", "\\\"");
					}

					Files.append("T" + "100" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + offsetLeft + " " + offsetRightI + "\t"
							+ originalText.substring(offsetLeft, offsetRightI) + "\t\n", outputBRAT, Charsets.UTF_8);
				}

				else {
					if (word.startsWith("-")) {
						int newStartOffset = word.lastIndexOf("-") + 1;
						word = word.substring(newStartOffset);
						Files.append("T" + "10" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + (offsetLeft + newStartOffset) + " "
								+ offsetRight + "\t" + word + "\t\n", outputBRAT, Charsets.UTF_8);
					} else
						Files.append("T" + "100" + sourceNumberBrat + "\t" + SourceExtractorConstant.SOURCE + sourceExt + " " + offsetLeft + " " + offsetRight + "\t"
								+ originalText.substring(offsetLeft, offsetRight) + "\t\n", outputBRAT, Charsets.UTF_8);
				}
			}
			break;

		default:
			System.out.println(sourceExt + " = PRIM ou SEC");
			System.exit(1);
			break;
		}

	}

	private StringBuilder addAnnotationCoreferenceToBratReturnString(String fileId, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, HashMap<Integer, Integer> bestSubjectForTheSources,
			HashMap<String, ArrayList<Integer>> sourceAndOffset) throws IOException {
		// "left offset source" : "[subject, source]"
		HashMap<Integer, Pair<String, String>> coreference = new HashMap<>();

		// we loop through the best subject we found to be the best
		for (Entry<Integer, Integer> r : bestSubjectForTheSources.entrySet()) {
			StringBuilder source = new StringBuilder();

			int startSource = r.getKey();
			int startSubject = r.getValue();

			// we write the source in StringBuilder source
			SourceExtractorUtil.appendSource(sourceOffsets, wordsAndPosByOffset, source, startSource);

			StringBuilder subject = new StringBuilder();
			// if the subject is actually a subject
			if (subjectOffsets.get(startSubject) != null) {
				SourceExtractorUtil.appendSubject(subjectOffsets, sourceOffsets, wordsAndPosByOffset, coreference, source, startSource, startSubject, subject);
			}
			// if the subject is not a subject but a important word
			// in the text it is possible that "Le ministre E. Macron" exists
			// without being a subject
			else if (profAndSecondarySourceTriggerOffsets.get(startSubject) != null) {
				SourceExtractorUtil.appendSubject(profAndSecondarySourceTriggerOffsets, sourceOffsets, wordsAndPosByOffset, coreference, source, startSource,
						startSubject, subject);
			}
			// if anonymous source
			else if (startSubject == -1) {
				coreference.put(startSource, new Pair<>("anonymous", source.toString()));
			}
		}

		HashMap<String, Double> sourceHasAlreadyBeenAnnotated = new HashMap<String, Double>();

		int indexNotes = 0;

		HashMap<String, String> keepOnlyOneAnnotation = new HashMap<>();

		// JSONObject object= null;
		// JSONArray jsonArray = null;

		StringBuilder result = new StringBuilder();

		// sourceAndOffset : "T1" : [550, 551, 552, 553, 554, 555, ...]
		for (Entry<String, ArrayList<Integer>> entry : sourceAndOffset.entrySet()) {

			String idAnnotation = entry.getKey();
			ArrayList<Integer> eachOffsetsOfTheSource = entry.getValue();

			// if correference : "550" : ["le ministre", "E. Macron"] et
			// sourceAndOffset : "T3" : [550, 551, 552, 553, 554]
			// 550 match -> true
			if (CollectionUtils.containsAny(eachOffsetsOfTheSource, coreference.keySet())) {

				// we keep 550
				Collection<Integer> sameValues = CollectionUtils.intersection(eachOffsetsOfTheSource, coreference.keySet());
				String annotation = "";

				// we loop through... 550
				for (int val : sameValues) {
					// if T3 has not already been marked
					if (!sourceHasAlreadyBeenAnnotated.containsKey(idAnnotation)) {
						// we get the subject of the source
						String coref = coreference.get(val).getFirst();
						// some corrections because we assemble the subjects
						// from the word with a space between each word
						annotation = coref.replaceAll(" , ", ", ");
						annotation = annotation.replaceAll("l' ", "l'");

						if (annotation.endsWith(", ")) {
							annotation = annotation.substring(0, annotation.length() - 2);
						}

						indexNotes++;
						// we eventually write the coreference in Brat format
						keepOnlyOneAnnotation.put(idAnnotation, "#" + (indexNotes) + "\t" + "AnnotatorNotes " + idAnnotation + "\t" + annotation + "\t");

					}
				}
			}
		}

		// if (outputBrat) {
		for (String s : keepOnlyOneAnnotation.values()) {
			// System.out.println("APPEND " + s);
			result.append(s + "\n");
		}
		return result;
	}
	
	/**
	 * Add annotation to brat and convert into json ..
	 * @param fileId
	 * @param sourceOffsets
	 * @param subjectOffsets
	 * @param profAndSecondarySourceTriggerOffsets
	 * @param pronounOffsets
	 * @param wordsAndPosByOffset
	 * @param bestSubjectForTheSources
	 * @param sourceAndOffset
	 * @param outputStream
	 * @throws IOException
	 */
	private String addAnnotationCoreferenceToBrat(String fileId, String bratAnn, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, HashMap<Integer, Integer> bestSubjectForTheSources,
			HashMap<String, ArrayList<Integer>> sourceAndOffset) throws IOException {
		StringBuilder result = addAnnotationCoreferenceToBratReturnString(fileId, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets,
				wordsAndPosByOffset, bestSubjectForTheSources, sourceAndOffset);
		StringBuilder ann = new StringBuilder(bratAnn);
		ann.append(result.toString());
		String jsonResult = brat2JSON(fileId, ann);
		return jsonResult;
	}

	private File addAnnotationCoreferenceToBrat(String fileId, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset, HashMap<Integer, Integer> bestSubjectForTheSources,
			HashMap<String, ArrayList<Integer>> sourceAndOffset) throws IOException {

		StringBuilder result = addAnnotationCoreferenceToBratReturnString(fileId, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets,
				wordsAndPosByOffset, bestSubjectForTheSources, sourceAndOffset);

		File annFile = null;
		File jsonFile = null;

		if (LABELED_MODE) {
			annFile = new File(this.paths.DIR_FINAL_RESULT_LABELED, fileId + ".ann");
			// JSON
			if (!outputBrat) {
				jsonFile = new File(this.paths.DIR_FINAL_RESULT_LABELED, fileId + ".json");

			}
		} else {
			annFile = new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".ann");

			if (!outputBrat) {
				jsonFile = new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".json");
			}
		}

		Files.append(result, annFile, Charsets.UTF_8);
		// }
		// Convert to JSON
		if (outputBrat) {
			if (!LABELED_MODE) {
				// Also add the .txt file for visualization in Brat
				Files.copy(new File(this.paths.DIR_INPUT_UNLABELED, fileId + ".txt"), new File(this.paths.DIR_FINAL_RESULT_UNLABELED, fileId + ".txt"));
			}

			return annFile;
		} else {
			brat2JSON(fileId, annFile, jsonFile);
			// Files.write("", outputFile, Charsets.UTF_8);
			// try {
			// Files.write(object.toString(2), outputFile, Charsets.UTF_8);
			// } catch (JSONException e) {
			// e.printStackTrace();
			// }
			annFile.delete();
			return jsonFile;
		}
		// if (jsonTempFile.exists())
		// jsonTempFile.delete();
		// return outputFile;
	}

	private StringBuilder brat2JSON(String fileId, HashMap<String, SourceAnnotation> annotations) {
		String docText = this.memory.docTexts.get(fileId);
		String revisionDateStr = this.memory.revisionDate.get(fileId);
		ArrayList<Integer> sentenceOffsets = this.memory.sentenceOffsetsByFile.get(fileId);
		TreeSet<SourceAnnotation> orderedAnnotations = new TreeSet<>();
		orderedAnnotations.addAll(annotations.values());
		StringBuilder jsonResult = new StringBuilder();
		jsonResult.append("{\"identifier\":\""+fileId+"\",\n");
		jsonResult.append("\"revisionDate\":\""+revisionDateStr+"\",\n");
		jsonResult.append("\"source_sentences\":[\n");
		int annIndex = 0;

		ArrayList<SourceAnnotation> sentenceAnnotations = new ArrayList<>();
		int currentSentenceIndex = 0;
		int currentSentenceOffset = sentenceOffsets.get(currentSentenceIndex);
		int nextSentenceOffset;
		if (sentenceOffsets.size() > 1) {
			nextSentenceOffset = sentenceOffsets.get(currentSentenceIndex + 1);
		} else {
			nextSentenceOffset = docText.length();
		}

		for (SourceAnnotation annotation : orderedAnnotations) {
			int leftOffset = annotation.getLeftOffset();

			// Still same sentence
			if (leftOffset < nextSentenceOffset) {
				sentenceAnnotations.add(annotation);
			}
			// New sentence
			else {
				if (!sentenceAnnotations.isEmpty()) {
					annIndex = SourceExtractorUtil.appendToJSON(docText, jsonResult, annIndex, sentenceAnnotations, currentSentenceIndex, currentSentenceOffset,
							nextSentenceOffset);
					sentenceAnnotations.clear();

				}

				// Go to sentence containing the annotation
				do {
					currentSentenceIndex++;
					currentSentenceOffset = sentenceOffsets.get(currentSentenceIndex);
					if (currentSentenceIndex < sentenceOffsets.size() - 1) {
						nextSentenceOffset = sentenceOffsets.get(currentSentenceIndex + 1);
					} else {
						nextSentenceOffset = docText.length();
					}
				} while (leftOffset > nextSentenceOffset);
				sentenceAnnotations.add(annotation);

			}
		}
		// System.out.println("--" + currentSentenceOffset + " " +
		// nextSentenceOffset);
		// Last annotations
		if (!sentenceAnnotations.isEmpty()) {
			annIndex = SourceExtractorUtil.appendToJSON(docText, jsonResult, annIndex, sentenceAnnotations, currentSentenceIndex, currentSentenceOffset,
					nextSentenceOffset);
		}

		jsonResult.append("]}\n");
		return jsonResult;
	}

	private String brat2JSON(String fileId, StringBuilder ann) {
		HashMap<String, SourceAnnotation> annotations = SourceExtractorUtil.brat2Annotation(ann.toString());
		StringBuilder jsonResult = brat2JSON(fileId, annotations);
		return jsonResult.toString();
	}

	private void brat2JSON(String fileId, File annFile, File jsonFile) throws IOException {
		// System.out.println(sentenceOffsets);
		HashMap<String, SourceAnnotation> annotations = SourceExtractorUtil.brat2Annotation(annFile);
		StringBuilder jsonResult = brat2JSON(fileId, annotations);
		Files.write(jsonResult.toString(), jsonFile, Charsets.UTF_8);
	}

	/**
	 * Cette méthode sert à garder les sujets importants et les correferences trouvés dans le texte. </br>
	 * On appelle d'abord la méthode findSubjects qui va récupérer tous les sujets annotés dans wapitiResult et ensuite
	 * on va trouver les meilleurs sujets</br>
	 * correspondant à chaque source.
	 * 
	 * @param wapitiResult
	 *            Le fichier en sortie de wapiti
	 * @param textWithOffset
	 *            On parse de nouveau le texte pour simplement récupérer les offsets des mots
	 * @param retrieveOffsetsOfSource
	 *            HashMap qui contient ID_SOURCE=OFFSETL OFFSETR TEXT
	 * @param sourceOffsets
	 *            TreeMap qui contient OFFSETL=OFFSETR
	 * @param subjectOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les sujets de la forme A=B
	 * @param profAndSecondarySourceTriggerOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les professions de la forme A=B
	 * @param pronounOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les pronoms de la forme A=B
	 * @return
	 * @return La liste de tous les sujets
	 * @throws IOException
	 */
	private HashMap<Integer, Pair<String, String>> parseTaggedTextToKeepCoreferencesAndSubject(String wapitiResult, String textWithOffset,
			HashMap<String, String> retrieveOffsetsOfSource, TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets,
			TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets, HashMap<Integer, Integer> bestSubjectForTheSources)
			throws IOException {

		ArrayList<Integer> sourceOffsetLeft = new ArrayList<Integer>();

		// retrieveOffsetsOfSource contains OFFSET OFFSET TEXT, we want to keep
		// only the first two element (offset left and right)
		for (String s : retrieveOffsetsOfSource.values()) {

			String[] fields = s.split("\\s+");
			String offsetLeft = fields[0];
			String offsetRight = fields[1];

			sourceOffsetLeft.add(Integer.parseInt(offsetLeft));
			sourceOffsets.put(Integer.parseInt(offsetLeft), Integer.parseInt(offsetRight));
		}

		// we start by gathering the subjects together
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = findSubjects(wapitiResult, textWithOffset, sourceOffsets, subjectOffsets,
				profAndSecondarySourceTriggerOffsets, pronounOffsets, resources, memory);

		Collections.sort(sourceOffsetLeft);

		// we continue by looking for the best annotation among the ones we have
		// found
		findBestSubjectsForSources(bestSubjectForTheSources, sourceOffsets, subjectOffsets, profAndSecondarySourceTriggerOffsets, pronounOffsets, wordsAndPosByOffset);

		return wordsAndPosByOffset;

	}

	/**
	 * Cette méthode permet de lancer la méthode bestAnnotation sur les 3 listes que nous avons (sujets, professions,
	 * pronoms).
	 * 
	 * @param sourceOffsets
	 * @param subjectOffsets
	 * @param profAndSecondarySourceTriggerOffsets
	 * @param pronounOffsets
	 * @param wordsAndPosByOffset
	 */
	private HashMap<Integer, Integer> findBestSubjectsForSources(HashMap<Integer, Integer> bestSubjectForTheSources, TreeMap<Integer, Integer> sourceOffsets,
			TreeMap<Integer, Integer> subjectOffsets, TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset) {

		HashSet<Integer> alreadyAnnotated = new HashSet<>();
		HashMap<ArrayList<String>, Integer> profCorefAlreadyAnnoted = new HashMap<>();
		int lastIndexSujet = -1;

		// we loop through all source offset and we look for the left offset of
		// each one.
		for (int sourceLeftOffset : sourceOffsets.keySet()) {

			lastIndexSujet = bestAnnotation(sourceOffsets, subjectOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources,
					sourceLeftOffset, lastIndexSujet, "subject");

			bestAnnotation(sourceOffsets, profAndSecondarySourceTriggerOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources,
					sourceLeftOffset, lastIndexSujet, "prof");

			bestAnnotation(sourceOffsets, pronounOffsets, alreadyAnnotated, profCorefAlreadyAnnoted, wordsAndPosByOffset, bestSubjectForTheSources, sourceLeftOffset,
					lastIndexSujet, "pronoun");

		}
		return bestSubjectForTheSources;
	}

	/**
	 * Méthode qui permet de garder le meilleur sujet pertinent selon la treemap que l'on parcourt.<br>
	 * 
	 * ALGO : <br>
	 * <br>
	 * Parcourir la liste des offsetsLeft de la treeMap voulue (item)<br>
	 * &nbsp;Si offsetLeftItem < sourceRightOffset (si le sujet apparaît avant la fin d'une source)<br>
	 * &nbsp;&nbsp;Si cette source n<br>
	 * &nbsp;&nbsp;&nbsp;Pour chaque tokenSOURCE != stopWords<br>
	 * 
	 * 
	 * 
	 * @param sourceOffsets
	 * @param mapOffsets
	 * @param wordsAndPosByOffset
	 * @param scores
	 * @param bestSujIndex
	 * @param alreadyAnnoted
	 * @param profCorefAlreadyAnnoted
	 * @param sourceLeftOffset
	 * @param lastIndexSujet
	 * @return
	 */
	private int bestAnnotation(TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> mapOffsets, HashSet<Integer> alreadyAnnoted,
			HashMap<ArrayList<String>, Integer> profCorefAlreadyAnnoted, HashMap<Integer, Pair<String, String>> wordsAndPosByOffset,
			HashMap<Integer, Integer> bestSubjectForTheSources, int sourceLeftOffset, int lastIndexSujet, String subjectMap) {

		Integer sourceRightOffset = sourceOffsets.get(sourceLeftOffset);
		ArrayList<String> sourceWords = new ArrayList<>();
		ArrayList<String> sourcePOSs = new ArrayList<>();

		// we rebuild the source from the offsets
		SourceExtractorUtil.putWordsAndPos(wordsAndPosByOffset, sourceLeftOffset, sourceRightOffset, sourceWords, sourcePOSs);

		// based on the fact a term-profession which designs someone is used
		// only for that person
		// so if a term-profession is a source and it has already been marked by
		// a particular subject
		// so if we meet again this term-profession-source we can use the same
		// subject used for the last same term-source
		//
		// let's make a example :
		// if we encounter these subjects in this order "Le réalisateur Antoine
		// Viktine", "le réalisateur", "Le président de la République", "le
		// réalisateur"
		// our way to annotate is always "look at the previous subject" and the
		// last "le réalisateur" could have been annotated into "Le président de
		// la République"
		// but with profCorefAlreadyAnnoted we can directly use the same mark
		// which we have used on previous same term-profession-source.
		//
		// "Le réalisateur Antoine Viktine" -> Antoine Viktine
		// "le réalisateur" -> Antoine Viktine
		// "Le président de la République"
		// "le réalisateur" -> Antoine Viktine

		if (profCorefAlreadyAnnoted.containsKey(sourceWords)) {
			Integer sameProfWordIndex = profCorefAlreadyAnnoted.get(sourceWords);
			if (bestSubjectForTheSources.containsKey(sameProfWordIndex)) {
				Integer transitive = bestSubjectForTheSources.get(sameProfWordIndex);
				bestSubjectForTheSources.put(sourceLeftOffset, transitive);
			}
		} else {
			// we are looking for anonymous sources and they start with a non
			// determinate article ("des","une","un")
			String firstWord;
			try {
				firstWord = wordsAndPosByOffset.get(sourceLeftOffset).getFirst();
			} catch (NullPointerException e) {
				TreeMap<Integer, Pair<String, String>> ordered = new TreeMap<>();
				ordered.putAll(wordsAndPosByOffset);
				System.out.println(ordered);
				System.out.println(sourceLeftOffset);
				System.out.println(wordsAndPosByOffset.get(sourceLeftOffset));
				System.out.println(sourceWords);
				System.out.println(sourcePOSs);
				throw e;
			}

			// if not anonymous
			if (!this.resources.NON_DET_ARTICLES.contains(firstWord)) {

				// this variable is THE variable to follow, it gives the best
				// subject index ;)
				int bestSujIndex = -1;

				for (int offsetLeftItem : mapOffsets.keySet()) {

					// the best subject for a source appears ALWAYS before the
					// source itself
					if (offsetLeftItem < sourceRightOffset) {

						Integer itemRightOffset = mapOffsets.get(offsetLeftItem);

						ArrayList<String> subjectWords = new ArrayList<>();
						ArrayList<String> subjectPOSs = new ArrayList<>();

						// we rebuild the subject from its offsets
						SourceExtractorUtil.putWordsAndPos(wordsAndPosByOffset, offsetLeftItem, itemRightOffset, subjectWords, subjectPOSs);

						// we will mark the source if we are sure that the
						// source is contained into the subject
						// SOURCE : M. Dupont
						// SUBJET : La ferme de Paul Dupont => the source is
						// contained into the source (we stop the PERSON_ABBR)
						// but
						// SOURCE : Paul Dupont
						// SUBJECT : Henri Dupont => only Dupont is contained
						// into the source therefore contain = false
						// thus we will not have a annotation that "belongs to"
						// someone to ... his brother !

						int countNumberOfSharedWords = 0;
						Boolean contain = true;
						for (String s : sourceWords) {
							if (!this.resources.STOP_WORDS.contains(s.toLowerCase()) && !this.resources.PERSON_ABBR.contains(s))
								if (!subjectWords.contains(s)) {
									contain = false;
								} else {
									countNumberOfSharedWords++;
								}
						}
						// we loop through all words in the subject
						for (int subjectTokenIndex = 0; subjectTokenIndex < subjectWords.size(); subjectTokenIndex++) {
							int lastIndexProf = 0;
							String tokenSuj = subjectWords.get(subjectTokenIndex);

							if (!this.resources.STOP_WORDS.contains(tokenSuj.toLowerCase())) {

								// we loop through all words in the source
								for (int wordIndex = 0; wordIndex < sourceWords.size(); wordIndex++) {

									String tokenSource = sourceWords.get(wordIndex);
									String posTokenSource = sourcePOSs.get(wordIndex);

									// we saves execution time
									if (this.resources.PERSON_ABBR.contains(tokenSource)) {
										continue;
									}

									if (!this.resources.STOP_WORDS.contains(tokenSource.toLowerCase())) {
										// we verify if a token from the source
										// is equal to a token from the subject
										// "Le réalisateur Antoine Viktine" ->
										// "le réalisateur" => réalisateur
										if (tokenSuj.equals(tokenSource) && !this.resources.PERSON_ABBR.contains(tokenSuj)) {

											// we get the token in common
											Pair<String, String> wordAndPosByOffset = wordsAndPosByOffset.get(offsetLeftItem);
											String wordByOffset = wordAndPosByOffset.getFirst();

											// if the token is a NPP (or ET
											// because there are some bad
											// annotations in Stanford)
											// and if the subject contains a NPP
											// (we do not want "le réalisateur"
											// to be marked as "le réalisateur")
											if ((posTokenSource.equals("NPP") || posTokenSource.equals("ET"))
													&& (subjectPOSs.contains("NPP") || subjectPOSs.contains("ET"))) {
												// contain because of the
												// "brother's thing"
												if (!this.resources.PERSON_ABBR.contains(wordByOffset) && contain)
													bestSujIndex = offsetLeftItem;

											}
											// this token is not a NPP
											// if it is a prof word
											else if (this.resources.PROFESSION_WORDS.contains(tokenSource.toLowerCase())) {
												// example : SOURCE : "le roi
												// d'Arabie saoudite"
												// SUBJECT : "le roi Salmane
												// d'Arabie saoudite"
												if (contain && countNumberOfSharedWords > 1) {
													// if last subject is not
													// the subject itself
													// because we do not
													// annotate a subject
													// without NPP with itself
													if (lastIndexSujet != offsetLeftItem) {
														bestSujIndex = offsetLeftItem;
													}
													// we verify if the source
													// is also a subject and if
													// the source contains a NPP
													// we annotate on this
													// source the text of this
													// source.
													else if (sourceOffsets.containsKey(offsetLeftItem) && (sourcePOSs.contains("NPP") || sourcePOSs.contains("ET"))) {
														bestSujIndex = offsetLeftItem;
													}
													// otherwise if the source
													// does not contain a NPP,
													// we take the last subject
													// before the source.
													else {
														Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
														if (lastSubject != null)
															bestSujIndex = lastSubject;
													}
												}
												// if there is only one common
												// word (and not NPP word)
												else if (contain && countNumberOfSharedWords == 1) {
													if (lastIndexSujet != -1) {
														// if we are looking for
														// profession (not
														// subject)
														if (subjectMap.equals("prof")) {
															// we put the last
															// profession found
															// before this
															// source
															if (mapOffsets.lowerKey(sourceLeftOffset) != null) {
																lastIndexProf = mapOffsets.lowerKey(sourceLeftOffset);
																bestSujIndex = lastIndexProf;
															}
															// if it is the
															// first profession
															// of the text, we
															// mark it as
															// annotation
															else {
																lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
																bestSujIndex = lastIndexProf;
															}

														}
														// if it is a subject
														else {
															// we always want to
															// put the last
															// subject before
															// the source as
															// annotation
															if (lastIndexSujet != sourceLeftOffset)
																// if subject is
																// not the
																// source itself
																bestSujIndex = lastIndexSujet;
															else {
																// otherwise we
																// look for the
																// last subject
																// before the
																// source
																Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
																if (lastSubject != null) {
																	bestSujIndex = lastSubject;
																	// if we
																	// encounter
																	// a word
																	// that is
																	// subject,
																	// we keep
																	// it to
																	// maybe use
																	// it again
																	profCorefAlreadyAnnoted.put(sourceWords, lastSubject);
																} else
																	bestSujIndex = lastIndexSujet;
															}
														}
													}
												}
											}
											// if the source is a pronoun (not a
											// NPP word or a profession)
											else if (this.resources.PRONOUNS.contains(tokenSource.toLowerCase())) {
												String lastWord = "";

												if (lastIndexSujet != -1) {
													String wordAtThisIndex = wordsAndPosByOffset.get(lastIndexSujet).getFirst();
													lastWord = wordAtThisIndex;
												}
												// if the last subject is a prof
												// word
												if (lastIndexSujet != -1 && this.resources.PROFESSION_WORDS.contains(lastWord)) {
													bestSujIndex = lastIndexSujet;
												}
												// other we look for the last
												// prof word
												else {
													lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
													if (lastIndexProf != offsetLeftItem) {
														bestSujIndex = lastIndexProf;
													} else {
														bestSujIndex = lastIndexSujet;
													}
												}

											}
											// if not a NPP, profword or
											// pronoun, we look for a person
											// word (femme, homme, jeune,
											// oncle...)
											else if (this.resources.PERSON.contains(tokenSource.toLowerCase())) {
												if (lastIndexSujet != -1) {
													if (lastIndexSujet != sourceLeftOffset)
														bestSujIndex = lastIndexSujet;
													else {
														Integer lastSubject = mapOffsets.lowerKey(sourceLeftOffset);
														if (lastSubject != null)
															bestSujIndex = lastSubject;
														else
															bestSujIndex = lastIndexSujet;
													}
												}
											}
										}
										// if there is not any token in subject
										// equal to a token in the source
										// il -> le ministre
										else if (this.resources.PRONOUNS.contains(tokenSource.toLowerCase())) {
											// if there is a last subject
											if (lastIndexSujet != -1) {
												bestSujIndex = lastIndexSujet;
											}
											// otherwise we look for a
											// profession word
											else if (subjectMap.equals("prof")) {
												lastIndexProf = mapOffsets.lowerKey(sourceRightOffset);
												if (lastIndexProf != offsetLeftItem)
													bestSujIndex = lastIndexProf;
											}

										}
									}
								}
							}
							if (subjectMap.equals("subject"))
								lastIndexSujet = mapOffsets.lowerKey(sourceRightOffset);
						}

					}
					// if there is subject found
					if (bestSujIndex != -1) {
						// if this subject has already been marked for another
						// source, we do a transitive relation
						if (bestSubjectForTheSources.containsKey(bestSujIndex)) {
							Integer transitive = bestSubjectForTheSources.get(bestSujIndex);
							bestSubjectForTheSources.put(sourceLeftOffset, transitive);
						}
						// otherwise we do mark the new subject for the source
						else {
							bestSubjectForTheSources.put(sourceLeftOffset, bestSujIndex);
						}
					}
				}

			}
			// if anonymous source
			else if (this.resources.NON_DET_ARTICLES.contains(firstWord)) {
				bestSubjectForTheSources.put(sourceLeftOffset, -1);
			}
		}

		return lastIndexSujet;

	}

	/**
	 * Cette méthode permet de garder 4 TreeMap : <br>
	 * - Une pour les sujets et leurs offsets, <br>
	 * - Une pour les noms des professions et leurs offsets (ministre Bernard Cazeneuve, l'imprimeur Michel
	 * Catalano...), <br>
	 * - Une pour les pronoms et leurs offsets. <br>
	 * - Une pour les sources et leurs offsets. <br>
	 * 
	 * @param wapitiResult
	 *            Le fichier en sortie de wapiti
	 * @param textWithOffset
	 *            On parse de nouveau le texte pour simplement récupérer les offsets des mots
	 * @param sourceOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les sources de la forme A=B
	 * @param subjectOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les sujets de la forme A=B
	 * @param profAndSecondarySourceTriggerOffsets
	 *            TreeMap qui contient l'offset gauche et droite de toutes les professions de la forme A=B
	 * @param pronounOffsets
	 *            TreeMap qui contient l'offset gauche et droite de tous les pronoms de la forme A=B
	 * 
	 */
	private HashMap<Integer, Pair<String, String>> findSubjects(String wapitiResult, String textWithOffset, TreeMap<Integer, Integer> sourceOffsets,
			TreeMap<Integer, Integer> subjectOffsets, TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets, TreeMap<Integer, Integer> pronounOffsets,
			Resources resources, Memory memory) {

		Scanner parseWapitiResultScanner = new Scanner(wapitiResult);
		Scanner parseTextOffsetScanner = new Scanner(textWithOffset);

		String wapitiResultLine;
		String textOffsetLine;

		// this HashMap will contain all leftOffset of each word and for each
		// offset it will contain a Pair of the word and the PoS at this offset
		HashMap<Integer, Pair<String, String>> wordsAndPosByOffset = new HashMap<>();

		// this counter prevents to keep a subject in a sentence if one source
		// is also in this same sentence
		// sources have priority over the subjects
		int numberOfSourcesInThatSentence = 0;

		// we need to loop though because parseWapitiResultScanner contains the
		// tags (SUJ)
		// and parseTextOffsetScanner contains the offsets
		while (parseWapitiResultScanner.hasNextLine() && parseTextOffsetScanner.hasNextLine()) {

			wapitiResultLine = parseWapitiResultScanner.nextLine();
			textOffsetLine = parseTextOffsetScanner.nextLine();

			// we reset the counter when it is a new line (new line symbolizes
			// beginning and end of sentence)
			if (wapitiResultLine.length() == 1 && textOffsetLine.length() == 1) {
				numberOfSourcesInThatSentence = 0;
				continue;
			}

			if (wapitiResultLine.length() > 1 && textOffsetLine.length() > 1) {
				String[] wapitiResultToken = wapitiResultLine.split("\\s+");

				String currentWord = wapitiResultToken[0];
				String currentPosTag = wapitiResultToken[1];
				String currentLemma = wapitiResultToken[2];
				String sourceSecTrigger = wapitiResultToken[7];
				String currentSubject = wapitiResultToken[8];

				String[] offsetTextToken = textOffsetLine.split("\\s+");
				String offset = "";
				// if (onlyOffset)
				// offset = offsetTextToken[4];
				// else
				offset = offsetTextToken[SourceExtractorConstant.numberFieldsPrim];

				String[] offsetsStr = offset.split(":");
				assert offsetsStr.length == 2 : "offsetsStr.length" + "must be equal to 2";

				int offsetLeft = Integer.parseInt(offsetsStr[0]);
				int lastOffsetRight = Integer.parseInt(offsetsStr[1]);
				int tempLastOffset = lastOffsetRight;

				boolean isSubject = currentSubject.equals("SUJ");
				boolean isProf = resources.PROFESSION_WORDS.contains(currentWord.toLowerCase());
				boolean isPronoun = resources.PRONOUNS.contains(currentLemma);
				boolean isSourceSecTrigger = false;

				if (sourceSecTrigger.equals("SOURCE_SEC_TRIGGER")) {
					isSourceSecTrigger = true;
				}
				// Stanford has marked as word "-t-il" or "-il", we need to fix
				// it and keep only "il"
				// we keep all words in wordsAndPosByOffset
				// if (isPronoun) {
				// System.out.println();
				// }
				if (isPronoun && currentWord.startsWith("-t-")) {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(3), currentPosTag));
					offsetLeft += 3;
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(3), currentPosTag));
				} else if (isPronoun && currentWord.startsWith("-")) {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(1), currentPosTag));
					offsetLeft += 1;
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord.substring(1), currentPosTag));
				} else {
					wordsAndPosByOffset.put(offsetLeft, new Pair<String, String>(currentWord, currentPosTag));
				}

				boolean isSource = sourceOffsets.containsKey(offsetLeft);

				// we keep only the subjects and the names of profession. We
				// ignore subjects that could be a country or a capital
				// we also keep all primary sources and the trigger which
				// triggers a secondary source
				if ((isSubject || isProf || isSource || isSourceSecTrigger) && !isGeographic(currentWord, resources)) {
					String newPosTag = currentPosTag;

					// we ignore some lemma to keep only important subject
					// (c'est -> subject = c')
					if (!currentLemma.equals("c'") && !currentLemma.equals("cela") && !currentLemma.equals("nous") && !currentLemma.equals("tu")
							&& !currentLemma.equals("où")) {

						boolean keepIt = false;
						boolean foundNPP = false;

						// person contains word like "femme","homme"
						if (resources.PERSON.contains(currentLemma))
							keepIt = true;

						if (isSourceSecTrigger)
							keepIt = true;

						if (isSource) {
							numberOfSourcesInThatSentence++;
							keepIt = true;
						}

						if (isSubject && isPronoun)
							keepIt = true;

						if (SourceExtractorUtil.isNPP(currentPosTag)) {
							keepIt = true;
							foundNPP = true;
						} else if (resources.PROFESSION_WORDS.contains(currentLemma)) {
							keepIt = true;
						}

						// we read the next word because a subject can start
						// with "le" and we need to know "le what"
						String newWapitiResultLine = parseWapitiResultScanner.nextLine();
						String newTextOffsetLine = parseTextOffsetScanner.nextLine();

						String newLemma = "";
						String newWord = "";
						String newSourceSecTrigger = "";

						if (newWapitiResultLine.length() > 1 && newTextOffsetLine.length() > 1) {

							String[] newWapitiResultToken = newWapitiResultLine.split("\\s+");

							String[] newOffsetTextToken = newTextOffsetLine.split("\\s+");
							String newOffset = "";
							// if (onlyOffset)
							// newOffset = newOffsetTextToken[4];
							// else
							newOffset = newOffsetTextToken[SourceExtractorConstant.numberFieldsPrim];

							String[] newOffsetsStr = newOffset.split(":");
							assert offsetsStr.length == 2 : "offsetsStr.length" + " must be equal to 2 (" + offsetsStr.length + ")";

							// get left offset of the new word
							int newOffsetLeft = Integer.parseInt(newOffsetsStr[0]);

							newWord = newWapitiResultToken[0];
							newPosTag = newWapitiResultToken[1];
							newLemma = newWapitiResultToken[2];
							newSourceSecTrigger = newWapitiResultToken[7];

							if (resources.PERSON.contains(newLemma))
								keepIt = true;

							if (SourceExtractorUtil.isNPP(newPosTag)) {

								keepIt = true;
								foundNPP = true;

							} else if (resources.PROFESSION_WORDS.contains(newLemma)) {
								keepIt = true;
							}

							if (newSourceSecTrigger.equals("SOURCE_SEC_TRIGGER"))
								keepIt = true;

							// we do not forget to add this new word into
							// wordsAndPosByOffset
							wordsAndPosByOffset.put(newOffsetLeft, new Pair<String, String>(newWord, newPosTag));

							// basicCategory is used with malt parser, it
							// contains simplified PoS tagging
							String cgPos = resources.COARSE_GRAINED_POS_MAP.get(newPosTag);

							// we continue if we have a "N", "A", "D" or "P"
							// word as well as PERSON_ABBR word (la ministre de
							// l'Environnement Mme Royal)
							if (SourceExtractorUtil.isSuperSensePos(cgPos) || resources.PERSON_ABBR.contains(newWord) || cgPos.equals("P")) {
								// we loop through the next words and we are
								// more selective about which word we keep
								while (((newPosTag.equals("ET") || newWord.equals(",") || SourceExtractorUtil.isSuperSensePos(cgPos) || (cgPos.equals("P") && !foundNPP)))
										&& !resources.TEMPORAL_EXPRESSIONS.contains(newLemma) && !newWord.equals("à") && !resources.PERSON_ABBR.contains(newWord)
										&& parseWapitiResultScanner.hasNextLine() && parseTextOffsetScanner.hasNextLine()) {

									if (SourceExtractorUtil.isNPP(newPosTag)) {
										keepIt = true;
										foundNPP = true;
									} else if (resources.PROFESSION_WORDS.contains(newLemma)) {
										keepIt = true;
									} else if (resources.PERSON.contains(newLemma)) {
										keepIt = true;
									}

									if (newSourceSecTrigger.equals("SOURCE_SEC_TRIGGER"))
										keepIt = true;

									// next word
									newWapitiResultLine = parseWapitiResultScanner.nextLine();
									newTextOffsetLine = parseTextOffsetScanner.nextLine();

									if (newWapitiResultLine.length() > 1 && newTextOffsetLine.length() > 1) {
										newWapitiResultToken = newWapitiResultLine.split("\\s+");
										newWord = newWapitiResultToken[0];
										newPosTag = newWapitiResultToken[1];
										newLemma = newWapitiResultToken[2];
										newSourceSecTrigger = newWapitiResultToken[7];

										// Get left offset of the new word
										newOffsetTextToken = newTextOffsetLine.split("\\s+");
										// if (onlyOffset)
										// newOffset = newOffsetTextToken[4];
										// else
										newOffset = newOffsetTextToken[SourceExtractorConstant.numberFieldsPrim];

										newOffsetsStr = newOffset.split(":");
										if (offsetsStr.length != 2) {
											System.out.println(offsetsStr.length + "must be equal to 2");
											System.exit(1);
										}

										lastOffsetRight = Integer.parseInt(newOffsetsStr[1]);
										newOffsetLeft = Integer.parseInt(newOffsetsStr[0]);

										tempLastOffset = newOffsetLeft;

										wordsAndPosByOffset.put(newOffsetLeft, new Pair<String, String>(newWord, newPosTag));
										cgPos = resources.COARSE_GRAINED_POS_MAP.get(newPosTag);
									}
								}
							}

						}

						// if it is a important word we go through this
						if (keepIt) {
							// we do not keep sources which are pronoun and we
							// add each source in the subject list
							if (isSource && !isPronoun) {
								subjectOffsets.put(offsetLeft, sourceOffsets.get(offsetLeft));
							}
							// we do not keep subjects which are pronoun and the
							// most importantly thing is that we do not keep
							// subject if we have kept a source in this sentence
							else if (isSubject && !isPronoun && numberOfSourcesInThatSentence == 0) {
								subjectOffsets.put(offsetLeft, tempLastOffset);
							}
							// we keep in another Map all profession word like
							// "le ministre des sports", "le policier
							// Ferdinand"...
							else if (isProf) {
								profAndSecondarySourceTriggerOffsets.put(offsetLeft, tempLastOffset);
							}
							// we keep all pronoun in another list
							else if (isPronoun) {
								pronounOffsets.put(offsetLeft, tempLastOffset);
							}
							// we also keep all word in relation with the
							// secondary sources
							else if (isSourceSecTrigger) {
								profAndSecondarySourceTriggerOffsets.put(offsetLeft, tempLastOffset);
							}

						}

					}
				}
			}

		}

		parseWapitiResultScanner.close();
		parseTextOffsetScanner.close();
		return wordsAndPosByOffset;
	}
}
