package fr.limsi.sourceExtractor.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import fr.limsi.sourceExtractor.Pair;

public class OffsetMaps {

	// this will contain the left and right offset of each source
	private TreeMap<Integer, Integer> sourceOffsets;
	private TreeMap<Integer, Integer> subjectOffsets;
	private TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets;
	private TreeMap<Integer, Integer> pronounOffsets;
	private HashMap<Integer, Integer> bestSubjectForTheSources;
	private HashMap<String, ArrayList<Integer>> sourceAndOffset;
	private HashMap<Integer, Pair<String, String>> wordsAndPosByOffset;

	public OffsetMaps(TreeMap<Integer, Integer> sourceOffsets, TreeMap<Integer, Integer> subjectOffsets, TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets,
			TreeMap<Integer, Integer> pronounOffsets, HashMap<Integer, Integer> bestSubjectForTheSources, HashMap<String, ArrayList<Integer>> sourceAndOffset,
			HashMap<Integer, Pair<String, String>> wordsAndPosByOffset) {
		super();
		this.sourceOffsets = sourceOffsets;
		this.subjectOffsets = subjectOffsets;
		this.profAndSecondarySourceTriggerOffsets = profAndSecondarySourceTriggerOffsets;
		this.pronounOffsets = pronounOffsets;
		this.bestSubjectForTheSources = bestSubjectForTheSources;
		this.sourceAndOffset = sourceAndOffset;
		this.wordsAndPosByOffset = wordsAndPosByOffset;
	}

	public TreeMap<Integer, Integer> getSourceOffsets() {
		return sourceOffsets;
	}

	public void setSourceOffsets(TreeMap<Integer, Integer> sourceOffsets) {
		this.sourceOffsets = sourceOffsets;
	}

	public TreeMap<Integer, Integer> getSubjectOffsets() {
		return subjectOffsets;
	}

	public void setSubjectOffsets(TreeMap<Integer, Integer> subjectOffsets) {
		this.subjectOffsets = subjectOffsets;
	}

	public TreeMap<Integer, Integer> getProfAndSecondarySourceTriggerOffsets() {
		return profAndSecondarySourceTriggerOffsets;
	}

	public void setProfAndSecondarySourceTriggerOffsets(TreeMap<Integer, Integer> profAndSecondarySourceTriggerOffsets) {
		this.profAndSecondarySourceTriggerOffsets = profAndSecondarySourceTriggerOffsets;
	}

	public TreeMap<Integer, Integer> getPronounOffsets() {
		return pronounOffsets;
	}

	public void setPronounOffsets(TreeMap<Integer, Integer> pronounOffsets) {
		this.pronounOffsets = pronounOffsets;
	}

	public HashMap<Integer, Integer> getBestSubjectForTheSources() {
		return bestSubjectForTheSources;
	}

	public void setBestSubjectForTheSources(HashMap<Integer, Integer> bestSubjectForTheSources) {
		this.bestSubjectForTheSources = bestSubjectForTheSources;
	}

	public HashMap<String, ArrayList<Integer>> getSourceAndOffset() {
		return sourceAndOffset;
	}

	public void setSourceAndOffset(HashMap<String, ArrayList<Integer>> sourceAndOffset) {
		this.sourceAndOffset = sourceAndOffset;
	}

	public HashMap<Integer, Pair<String, String>> getWordsAndPosByOffset() {
		return wordsAndPosByOffset;
	}

	public void setWordsAndPosByOffset(HashMap<Integer, Pair<String, String>> wordsAndPosByOffset) {
		this.wordsAndPosByOffset = wordsAndPosByOffset;
	}

}
