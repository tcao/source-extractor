package fr.limsi.sourceExtractor.training;

import java.io.File;
import java.io.IOException;

import org.grobid.core.jni.WapitiModel;

import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

public class TrainingLabel extends AProcess {

	public TrainingLabel(SourceExtractorConfig extractorConfig) {
		super(extractorConfig);
	}
	
	public void splitTrainDevTest() throws IOException, InterruptedException {
		LABELED_MODE = true;
		this.paths.DIR_INPUT_LABELED_CONVERTED.mkdirs();
		this.paths.DIR_MERGED_FILES_LABELED.mkdirs();
		this.paths.DIR_MODELS_LABELED.mkdirs();
		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.mkdirs();
		this.paths.DIR_RESULT_FILES_LABELED.mkdirs();

		// we convert all input annotation files into SOURCE-PRIM and
		// SOURCE-SEC
		transformOriginalBratToBratSourcePrimAndSec();
		System.out.println("Transformation en SOURCE-PRIM et SOURCE-SEC réussie.");

		// we start with tagging each files and launch maltparser on these
		// same files. We eventually end with the (B)IO conversion.
		parseDirForLabeled(this.paths.DIR_INPUT_LABELED);
		System.out.println("Tous les fichiers ont été transformés en (B)IO sans problème.");
	}

	public void train(String modelSuffix, int jubNumber) throws IOException, InterruptedException {
		if (!org.apache.commons.lang3.SystemUtils.IS_OS_LINUX) {
			System.err.println("Can only train the model on Linux version");
			System.exit(1);
		}
		LABELED_MODE = true;
		this.paths.DIR_INPUT_LABELED_CONVERTED.mkdirs();
		this.paths.DIR_MERGED_FILES_LABELED.mkdirs();
		this.paths.DIR_MODELS_LABELED.mkdirs();
		this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.mkdirs();
		this.paths.DIR_RESULT_FILES_LABELED.mkdirs();

		File modelPrim;
		File modelSec;
		if (modelBio) {
			modelPrim = new File(this.paths.DIR_MODELS_LABELED, SourceExtractorConstant.MODEL_PRIM + "_bio" + modelSuffix);
			modelSec = new File(this.paths.DIR_MODELS_LABELED, SourceExtractorConstant.MODEL_SEC + "_bio" + modelSuffix);
		} else {
			modelPrim = new File(this.paths.DIR_MODELS_LABELED, SourceExtractorConstant.MODEL_PRIM + "_io" + modelSuffix);
			modelSec = new File(this.paths.DIR_MODELS_LABELED, SourceExtractorConstant.MODEL_SEC + "_io" + modelSuffix);
		}

		// we convert all input annotation files into SOURCE-PRIM and
		// SOURCE-SEC
		transformOriginalBratToBratSourcePrimAndSec();
		System.out.println("Transformation en SOURCE-PRIM et SOURCE-SEC réussie.");

		// we start with tagging each files and launch maltparser on these
		// same files. We eventually end with the (B)IO conversion.
		parseDirForLabeled(this.paths.DIR_INPUT_LABELED);
		System.out.println("Tous les fichiers ont été transformés en (B)IO sans problème.");

		// we get all tests files from the input annotation directory
		DIRUtils.copyFilesWithSameNameToCompare(this.paths.DIR_TEST_FILES_PRIM, this.paths.DIR_INPUT_LABELED_CONVERTED, this.paths.DIR_BRAT_REFERENCE_LABELED);
		System.out.println("Copie des fichiers d'annotations réussie.");

		// we merge all train, dev and test files to obtain a "big" training
		// corpus for wapiti
		DIRUtils.mergeFile(this.paths.DIR_TRAIN_FILES_PRIM, this.paths.DIR_MERGED_FILES_LABELED);
		DIRUtils.mergeFile(this.paths.DIR_TEST_FILES_PRIM, this.paths.DIR_MERGED_FILES_LABELED);
		DIRUtils.mergeFile(this.paths.DIR_DEV_FILES_PRIM, this.paths.DIR_MERGED_FILES_LABELED);

		DIRUtils.mergeFile(this.paths.DIR_TRAIN_FILES_SEC, this.paths.DIR_MERGED_FILES_LABELED);
		DIRUtils.mergeFile(this.paths.DIR_TEST_FILES_SEC, this.paths.DIR_MERGED_FILES_LABELED);
		DIRUtils.mergeFile(this.paths.DIR_DEV_FILES_SEC, this.paths.DIR_MERGED_FILES_LABELED);
		System.out.println("Copie des fichiers assemblés réussie.\n");

		// wapiti train -d DEV_PRIM_MERGED_LABELED -p PATTERN_PRIM_LABELED
		// TRAIN_PRIM_MERGED_LABELED modelPrim
		System.out.println("\t\tDébut entrainement PRIM\t\t");
		WapitiModel.train(this.paths.PATTERN_PRIM, this.paths.TRAIN_PRIM_MERGED_LABELED, modelPrim, "-d " + this.paths.DEV_PRIM_MERGED_LABELED);
		System.out.println("\t\tModèle sauvegardé dans " + modelPrim.getAbsolutePath());
		// wapiti train -d DEV_SEC_MERGED_LABELED -p PATTERN_SEC_LABELED
		// input=TRAIN_PRIM_MERGED_LABELED output=modelSec
		if (searchSecondary) {
			System.out.println("\t\tDébut entrainement SEC\t\t");
			WapitiModel.train(this.paths.PATTERN_SEC, this.paths.TRAIN_SEC_MERGED_LABELED, modelSec, "-d " + this.paths.DEV_SEC_MERGED_LABELED);
		}
		System.out.println("\t\tModèle sauvegardé dans " + modelSec.getAbsolutePath());
		System.out.println();
		System.out.println("\t\tDébut label PRIM\t\t");

		// wapiti label -m modelPrim input=tests_files_one_by_one
		// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM -p
		WapitiLabeling wapitiPrim = WapitiLabeling.getWapitiInstance(modelPrim);
		wapitiPrim.wapitiTest(this.paths.DIR_TEST_FILES_PRIM, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM, jubNumber);

		if (searchSecondary) {
			System.out.println();
			System.out.println("\t\tDébut label SEC\t\t");
			deleteSentenceWithoutPrimForSec(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM, this.paths.DIR_TEST_FILES_SEC, true);
			// wapiti label -m modelSec input=tests_files_one_by_one
			// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_Sec -p
			WapitiLabeling wapitiSec = WapitiLabeling.getWapitiInstance(modelSec);
			wapitiSec.wapitiTest(this.paths.DIR_TEST_FILES_SEC, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC, jubNumber);
		}

		System.out.println();
		System.out.println("\t\tDébut conversion de la sortie wapiti en Brat\t\t");

		// wapiti has predicted the labels, we now transform the (B)IO files
		// into Brat format.
		parseDirForLabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM);
		if (searchSecondary) {
			parseDirForLabeled(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC);
		}

		System.out.println();
		System.out.println("\t\tDébut d'ajout d'annotation à la sortie Brat\t\t");

		// we end up adding some annotations to the previous files (the ones
		// that have been converted to brat)
		// System.out.println("copyFilesWithSameNameToCompare(" +
		// DIR_TEST_FILES_PRIM + ", " +
		// DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED + ", " +
		// DIR_BRAT_AJOUT_ANNOTATOR_LABELED + ")");
		DIRUtils.copyFilesWithSameNameToCompare(this.paths.DIR_TEST_FILES_PRIM, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, this.paths.DIR_FINAL_RESULT_LABELED);
		parseDirForLabeled(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED);

		System.out.println();
		System.out.println("\t\tScript terminé sans erreur.\t\t");
	}

	/**
	 * Méthode utilisée pour parcourir les dossiers quand on a des fichiers de test déjà annotés.<br />
	 * On l'utilise avec un dossier dir et le comportement sera différent selon le dossier entrée.<br />
	 * Fonctionne avec seulement 2 dossiers :<br />
	 * &nbsp;&nbsp;- Celui de tous les fichiers .txt avec les fichiers .ann qui vont avec,<br />
	 * &nbsp;&nbsp;- Celui où se trouvent / trouveront les fichiers .tag annotés automatiquement avec Wapiti (voir
	 * script bio_wapiti_ann.sh).<br />
	 * <br />
	 * On parcourt les fichiers dans le dossier en entrée, si on est dans le dossier d'input, on appelle la fonction
	 * parseFileToTag sur chaque fichier .txt pour tag chaque <br />
	 * fichier (on "continue" quand fichier .ann), et si on est dans le fichier de sortie de Wapiti, on prend tous les
	 * fichiers .tag et pour chaque fichier on appelle <br />
	 * la fonction parseFileToAnn pour annoter automatiquement chaque fichier.<br />
	 * <br />
	 *
	 * @param input
	 *            Le dossier en entrée (input_labeled ou bioWapiti)
	 * @throws IOException
	 */
	private void parseDirForLabeled(File input) throws IOException {

		if (input.isDirectory()) {
			int nbFilesDir = input.list().length;
			int countFiles = 0;

			for (File inFile : input.listFiles()) {
				if (inFile.isFile()) {
					String name = inFile.getName();
					String fileId = name.substring(0, name.lastIndexOf("."));

					// first step : pos tagging + malt parser + (B)IO conversion
					if (name.endsWith(".txt") && input.getName().equals(this.paths.DIR_INPUT_LABELED.getName())) {

						countFiles++;
						System.out.println("PoS Tagging + malt parser + (B)IO conversion : Traitement du fichier : " + countFiles + "/" + (nbFilesDir / 2));
						System.out.println(inFile.getName());

						parseFileToTag(fileId, inFile, nbFilesDir / 2);
					}
					// second step : convert the wapiti output files to brat
					// format
					else if (name.endsWith(".wapiti")) {
						countFiles++;
						// PRIM
						if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM.getName())) {
							System.out.println("BIOWAPITI LABELED PRIM: Traitement du fichier : " + countFiles + "/" + (nbFilesDir));

							parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, SourceExtractorConstant.PRIM, tools, paths, resources,
									memory);
						}
						// SEC
						else if (input.getName().equals(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC.getName())) {

							System.out.println("BIOWAPITI LABELED SEC : Traitement du fichier : " + countFiles + "/" + (nbFilesDir));
							parseFileToAnn(fileId, inFile, this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED, SourceExtractorConstant.SEC, tools, paths, resources,
									memory);

						} else {
							System.out.println("Erreur dossier bioWapiti");
							System.exit(0);
						}

					}
					// third step : add annotation to existing sources
					// (coreference, named entity recognition..)
					else if (name.endsWith(".ann")) {
						if (input.getName().equals(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED.getName())) {
							countFiles++;

							System.out.println("ADD ANNOTATION : Traitement du fichier : " + countFiles + "/" + (nbFilesDir));
							// System.out.println(inFile.getAbsolutePath());
							File outputFile = parseFileToAddAnnotationToBrat(fileId, inFile);
							System.out.println("2. Output written in " + outputFile.getAbsolutePath());

						} else if (input.getName().equals(this.paths.DIR_INPUT_LABELED.getName())) {
							// throw new RuntimeException(input.getName());
							continue;
						}
					}

					else {
						System.out.println("Format de fichier inconnu : " + name + " -> seulement .txt, .ann, .tag");
						System.exit(1);
					}
				}

			}
		} else if (input.isFile()) {
			String name = input.getName();
			// first step : pos tagging + malt parser + (B)IO conversion
			if (name.endsWith(".txt")) {
				String fileId = name.substring(0, name.lastIndexOf("."));
				System.out.println("PoS Tagging + malt parser + (B)IO conversion : Traitement du fichier " + name);
				parseFileToTag(fileId, input, 1);
			}
		}
	}

}
