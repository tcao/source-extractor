package fr.limsi.sourceExtractor;

public class FeatureSet {
	
	private String word;
	private String pos;
	private String lemma;
	private String isTrigger;
	private String isProf;
	private String QM;
	private String isSuj;
	private String isSujForTrigger;
	private String offsetPrim;
	private String isMedia;
	private String mix_col;
	
	public FeatureSet(String word, String pos, String lemma) {
		this.word = word;
		this.pos = pos;
		this.lemma = lemma;
		this.isTrigger = "NO";
		this.isProf = "ISNOTPROF";
		this.QM = "NQM";
		this.isSuj = "NOT_SUJ";
		this.isSujForTrigger = "NOT_SUJ_TRIGGER";
	}
	
	public FeatureSet(String word, String pos, String lemma, String isTrigger, String isProf, String QM, String isMedia, String mix_col, String offsets, String offsetSec) {
		this.word = word;
		this.pos = pos;
		this.lemma = lemma;
		this.isTrigger = isTrigger;
		this.isProf = isProf;
		this.isMedia = isMedia;
		this.mix_col = mix_col;
		this.QM = QM;
		this.isSuj = "NOT_SUJ";
		this.isSujForTrigger = "NOT_SUJ_TRIGGER";
		this.offsetPrim = offsets;
	}

	public String getIsMedia() {
		return isMedia;
	}

	public void setIsMedia(String isMedia) {
		this.isMedia = isMedia;
	}

	public String getIsTrigger() {
		return isTrigger;
	}

	public void setIsTrigger() {
		this.isTrigger = "TRIGGER";
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * @return the pos
	 */
	public String getPos() {
		return pos;
	}

	/**
	 * @param pos the pos to set
	 */
	public void setPos(String pos) {
		this.pos = pos;
	}

	/**
	 * @return the lemma
	 */
	public String getLemma() {
		return lemma;
	}

	/**
	 * @param lemma the lemma to set
	 */
	public void setLemma(String lemma) {
		this.lemma = lemma;
	}
	
	/**
	 * @return the isProf
	 */
	public String getIsProf() {
		return isProf;
	}

	/**
	 * @param isProf the isProf to set
	 */
	public void setIsProf() {
		this.isProf = "PROF";
	}

	/**
	 * @return the qM
	 */
	public String getQM() {
		return QM;
	}

	/**
	 * @param qM the qM to set
	 */
	public void setQM(String qM) {
		QM = qM;
	}

	/**
	 * @return the isSujForTrigger
	 */
	public String getIsSujForTrigger() {
		return isSujForTrigger;
	}

	/**
	 * @param isSujForTrigger the isSujForTrigger to set
	 */
	public void setIsSujForTrigger() {
		this.isSujForTrigger = "SUJ_TRIGGER";
	}

	/**
	 * @return the isSuj
	 */
	public String getIsSuj() {
		return isSuj;
	}

	/**
	 * @param isSuj the isSuj to set
	 */
	public void setIsSuj() {
		this.isSuj = "SUJ";
	}
	
	/**
	 * @return the encode
	 */
	public String getOffset() {
		return offsetPrim;
	}

	/**
	 * @param encode the encode to set
	 */
	public void setOffset(String offset) {
		this.offsetPrim = offset;
	}
	
	@Override
	public String toString() {
		return this.word + "\t" + this.pos + "\t" + this.lemma + "\t" + this.isTrigger + "\t" + this.isProf + "\t" 
	          + this.QM + "\t" + this.isMedia + "\t" + this.mix_col + "\t" + this.isSuj + "\t" + isSujForTrigger + "\t" + this.offsetPrim + "\t" + this.offsetPrim;
	}
}
