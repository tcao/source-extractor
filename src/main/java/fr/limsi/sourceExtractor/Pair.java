package fr.limsi.sourceExtractor;

public class Pair<U, V> {
	private U first;
	private V second;
	
	public Pair(U first, V second) {
		this.first = first;
		this.second = second;
	}
	
	/**
	 * @return the first
	 */
	public U getFirst() {
		return first;
	}
	/**
	 * @param first the first to set
	 */
	public void setFirst(U first) {
		this.first = first;
	}
	/**
	 * @return the second
	 */
	public V getSecond() {
		return second;
	}
	/**
	 * @param second the second to set
	 */
	public void setSecond(V second) {
		this.second = second;
	}
	
	@Override
	public boolean equals(Object o) {
	    if (o instanceof Pair) {
	        Pair<?, ?> p = (Pair<?, ?>)o;
	        return p.getFirst().equals(this.getFirst()) && p.getSecond().equals(this.getSecond());
	    } else {
	        return false;
	    }
	}
	
	@Override
	public int hashCode() {
	    return this.first.hashCode() + this.second.hashCode();
	}
	
	@Override
	public String toString() {
		return "(" + (this.getFirst() == null ? "null" : this.getFirst().toString()) + "," + (this.getSecond() == null ? "null" : this.getSecond().toString()) + ")";
	}
	
}
