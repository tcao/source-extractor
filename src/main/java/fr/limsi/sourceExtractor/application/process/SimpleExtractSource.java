package fr.limsi.sourceExtractor.application.process;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.Paths;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.training.TrainingUnLabel;
import fr.limsi.sourceExtractor.wapiti.WapitiLabeling;

public class SimpleExtractSource extends TrainingUnLabel {
	private Logger logger = LoggerFactory.getLogger(SimpleExtractSource.class);

	private Paths paths;

	public SimpleExtractSource(SourceExtractorConfig extractorConfig) {
		super(extractorConfig);
		this.paths = extractorConfig.getPaths();
	}

	//return result string
	public String extractSourceFromText(InputStream contentStream) throws IOException, InterruptedException {

		SimplePreprocessing simplePreprocessing = new SimplePreprocessing(this);
		//extract text from XML and set to Memory object
		String fileId = simplePreprocessing.tagXMLText(contentStream, extractorConfig);

		WapitiLabeling wapitiPrim = extractorConfig.getWapitiPrim();
		wapitiPrim.wapitiLabeled(this.paths.DIR_TEST_FILES_UNLABELED_PRIM, fileId, "tag", this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM);
		if (searchSecondary) {
			logger.debug("Labeling secondary sources (Wapiti SEC)");
			deleteSentenceWithoutPrimForSec(this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM, this.paths.DIR_TEST_FILES_UNLABELED_SEC, true);

			WapitiLabeling wapitiSec = extractorConfig.getWapitiSec();
			// wapiti label -m modelSec input=tests_files_one_by_one
			// output=DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_Sec
			// -p
			wapitiSec.wapitiLabeled(this.paths.DIR_TEST_FILES_UNLABELED_SEC, fileId, "wapiti", this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC);
		}

		logger.debug("Adding coreference and indexing information");

		// we end up adding some annotations to the previous files (the
		// ones that have been converted to brat)
		wapitiToBratConvertor(fileId, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM + "/" + fileId + ".wapiti", SourceExtractorConstant.PRIM);
		if (searchSecondary) {
			wapitiToBratConvertor(fileId, this.paths.DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC + "/" + fileId + ".wapiti", SourceExtractorConstant.SEC);
		}

		logger.debug("Adding coreference and indexing information");
		// we end up adding some annotations to the previous files (the
		// ones that have been converted to brat)
		ByteArrayOutputStream annFormatbyteArrayOutputStream = new ByteArrayOutputStream();
		DIRUtils.copyAnnotationToStream(this.paths.DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED + "/" + fileId + ".ann", annFormatbyteArrayOutputStream);
		String annContent = annFormatbyteArrayOutputStream.toString();
		logger.debug("brat: {}", annContent);
		//convertion de ann -> json
		String response = parseFileToAddAnnotationToBrat(fileId, annContent);
		logger.debug("output: {}", response);
		cleanDirContent(this.paths.DATA_UNLABELED_DIR, fileId);
		//Remote Memory entries
		extractorConfig.getMemory().cleanEntry(fileId);
		return response;
	}

	/**
	 * Remove only files that starts with fileId name. This is to avoid to remove ongoing process file for concurrent
	 * processing.
	 * 
	 * @param directory
	 * @param fileId
	 */
	private void cleanDirContent(File directory, String fileId) {
		if (directory.isDirectory()) {
			File[] files = directory.listFiles();
			for (File file : files) {
				if (file.isDirectory())
					cleanDirContent(file, fileId);
				else
					try {
						String name = file.getName();
						if (name.startsWith(fileId))
							FileUtils.forceDelete(file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}
}
