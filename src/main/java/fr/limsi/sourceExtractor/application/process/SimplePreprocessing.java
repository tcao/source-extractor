package fr.limsi.sourceExtractor.application.process;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Charsets;

import fr.limsi.sourceExtractor.DIRUtils;
import fr.limsi.sourceExtractor.Memory;
import fr.limsi.sourceExtractor.Paths;
import fr.limsi.sourceExtractor.Resources;
import fr.limsi.sourceExtractor.Tools;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.training.AProcessSupport;

public class SimplePreprocessing {

	private Pattern pattern, patternIdentifier, patternCreationDate;
	private AProcessSupport processSupport;
	private SimpleDateFormat utcFormat, format2;

	public SimplePreprocessing(AProcessSupport processSupport) {
		this.pattern = Pattern.compile("<p>([^<]+)</p>");
		this.patternIdentifier = Pattern.compile("<PublicIdentifier>([^<]+)</PublicIdentifier>");
		this.patternCreationDate = Pattern.compile("<ThisRevisionCreated>([^<]+)</ThisRevisionCreated>");
		this.processSupport = processSupport;
		this.utcFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
		this.format2 = new SimpleDateFormat("yyyyMMdd'T'HHmmssXX");

	}

	public String tagXMLText(InputStream contentStream, SourceExtractorConfig config) throws IOException {

		Memory memory = config.getMemory();
		Paths paths = config.getPaths();
		Tools tools = config.getTools();
		Resources resources = config.getResources();
		String xmlText = toString(contentStream);
		//String fileId = UUID.randomUUID().toString();
		String fileId = extractPublicIdentifier(xmlText);
		String text = extractTextFromXML(fileId, xmlText, memory);
		extractRevisionDate(fileId, xmlText, memory);
		// we create the files in which we will write the results
		File outFilePRIM = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_PRIM, fileId, ".tag");
		File outFileSEC = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_SEC, fileId, ".tag");
		//File outFileMP = DIRUtils.createDirAndFilesWithExt(paths.DIR_INPUT_UNLABELED_MP, fileId, ".conll");
		// parseText + maltParser + (b)io conversion
		processSupport.tagAndConvert(fileId, text, outFileSEC, "", tools, resources, memory);

		// we delete the last column of (B)io files to find the SOURCE-PRIM
		processSupport.deleteLastColumnForPrim(outFilePRIM, outFileSEC, true);
		outFileSEC.delete();
		return fileId;
	}

	private String toString(InputStream contentStream) throws IOException {
		StringWriter writer = new StringWriter();
		IOUtils.copy(contentStream, writer, Charsets.UTF_8);
		String xmlText = writer.toString();
		return xmlText;
	}

	private String extractPublicIdentifier(String xmlText) {
		Matcher matcher = this.patternIdentifier.matcher(xmlText);
		String fileId = "";
		while (matcher.find()) {
			fileId = matcher.group(1);
		}
		fileId.replaceAll("\\r", "");
		return fileId;
	}

	private void extractRevisionDate(String fileId, String xmlText, Memory memory) {
		Matcher matcher = this.patternCreationDate.matcher(xmlText);
		String revisionDateStr = "";
		while (matcher.find()) {
			revisionDateStr = matcher.group(1);
		}
		revisionDateStr.replaceAll("\\r", "");
		String strDate = "";
		try {
			if (revisionDateStr.contains("Z")) {
				strDate = revisionDateStr;
			} else {
				Date date = format2.parse(revisionDateStr);
				strDate = utcFormat.format(date);
			}
		} catch (ParseException e) {

		}
		memory.revisionDate.put(fileId, strDate);
	}

	private String extractTextFromXML(String fileId, String xmlText, Memory memory) {

		Matcher matcher = this.pattern.matcher(xmlText);
		StringBuilder result = new StringBuilder();
		while (matcher.find()) {
			result.append(matcher.group(1) + " ");
		}
		String resultStr = result.toString();
		resultStr.replaceAll("\\r", "");
		memory.docTexts.put(fileId, resultStr);
		return resultStr;
	}
}
