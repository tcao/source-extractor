package fr.limsi.sourceExtractor.application.service;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;
import fr.limsi.sourceExtractor.application.process.SimpleExtractSource;

@RestController
public class SourceExtractorService {
	private Logger logger = LoggerFactory.getLogger(SourceExtractorService.class);

	@Autowired
	private SourceExtractorConfig extractorConfig;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping(value = "/extractNewsML", method = RequestMethod.POST, consumes = "text/xml; charset=utf-8", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String> extractSource(RequestEntity<Resource> content) throws IOException {
		Resource body = content.getBody();
		InputStream bodyStream = body.getInputStream();
		logger.debug(extractorConfig.getResources().toString());
		extractorConfig.setXml(true);
		//doit extraire le contenu text entre <p></p>
		SimpleExtractSource simplePreprocessing = new SimpleExtractSource(extractorConfig);
		//recheche prim et secondary source
		try {
			String result = simplePreprocessing.extractSourceFromText(bodyStream);
			ResponseEntity<String> response = new ResponseEntity<String>(result, HttpStatus.OK);
			return response;
			//process le contenu txt
			// récupère la réponse dans un stream.
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;

	}

}
