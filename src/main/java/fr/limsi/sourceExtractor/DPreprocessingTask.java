package fr.limsi.sourceExtractor;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import fr.limsi.sourceExtractor.application.configuration.SourceExtractorUtil;
import fr.limsi.sourceExtractor.training.AProcessSupport;

public class DPreprocessingTask extends RecursiveTask<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private File inFile;
	private boolean xmlInput;
	private int countUnlabeled;
	private int nbFileUnlabeled;
	private Tools tools;
	private Resources resources;
	private Paths paths;
	private Memory memory;
	private AProcessSupport processSupport;

	public DPreprocessingTask(File inFile, boolean xmlInput, int countUnlabeled, int nbFileUnlabeled, Tools tools, Resources resources, Paths paths, Memory memory,
			AProcessSupport processSupport) {
		super();
		this.inFile = inFile;
		this.xmlInput = xmlInput;
		this.countUnlabeled = countUnlabeled;
		this.nbFileUnlabeled = nbFileUnlabeled;
		this.tools = tools;
		this.resources = resources;
		this.paths = paths;
		this.memory = memory;
		this.processSupport = processSupport;
	}

	private Integer preprocessUnlabeledFile(File inFile, boolean xmlInput, int countUnlabeled, int nbFileUnlabeled, Tools tools, Resources resources, Paths paths,
			Memory memory) throws IOException {
		String filename = inFile.getName();
		String fileId = inFile.getName().substring(0, filename.lastIndexOf("."));

		System.err.println("Start labeling file : " + countUnlabeled + "/" + (nbFileUnlabeled) + " " + fileId);

		File outFilePRIM = null;
		File outFileSEC = null;
		String text;
		File txtFile = new File(paths.DIR_INPUT_UNLABELED, fileId + ".txt");
		if (xmlInput) {
			text = extractTextFromXML(fileId, new File(paths.DIR_INPUT_UNLABELED, filename), txtFile, memory);
		} else {
			text = SourceExtractorUtil.getDocText(fileId, txtFile, memory);
		}

		text = text.replaceAll("\\r", "");

		// we create the files in which we will write the results
		outFilePRIM = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_PRIM, fileId, ".tag");
		outFileSEC = DIRUtils.createDirAndFilesWithExt(paths.DIR_TEST_FILES_UNLABELED_SEC, fileId, ".tag");
		//File outFileMP = DIRUtils.createDirAndFilesWithExt(paths.DIR_INPUT_UNLABELED_MP, fileId, ".conll");

		// parseText + maltParser + (b)io conversion
		//processSupport.tagAndConvert(fileId, text, outFileSEC, outFileMP, "", tools, resources, memory);
		processSupport.tagAndConvert(fileId, text, outFileSEC, "", tools, resources, memory);

		// we delete the last column of (B)io files to find the SOURCE-PRIM
		processSupport.deleteLastColumnForPrim(outFilePRIM, outFileSEC, true);
		outFileSEC.delete();
		return 1;
	}

	/**
	 * Extracts content within
	 * <p>
	 * element
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private String extractTextFromXML(String fileId, File inFile, File outFile, Memory memory) throws IOException {
		// Let's do it simply, so that even ill-formed XML will make it
		Pattern pattern = Pattern.compile("<p>([^<]+)</p>");
		String xmlText = Files.toString(inFile, Charsets.UTF_8);
		Matcher matcher = pattern.matcher(xmlText);
		StringBuilder result = new StringBuilder();
		while (matcher.find()) {
			result.append(matcher.group(1) + " ");
		}
		String resultStr = result.toString();
		if (outFile.exists()) {
			outFile.delete();
		}
		resultStr.replaceAll("\\r", "");
		Files.append(resultStr, outFile, Charsets.UTF_8);
		memory.docTexts.put(fileId, resultStr);
		return resultStr;
	}

	@Override
	protected Integer compute() {
		try {
			return preprocessUnlabeledFile(inFile, xmlInput, countUnlabeled, nbFileUnlabeled, tools, resources, paths, memory);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
